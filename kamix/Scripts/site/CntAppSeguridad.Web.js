﻿var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var LanguageForm = /** @class */ (function (_super) {
            __extends(LanguageForm, _super);
            function LanguageForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            LanguageForm.formKey = 'Administration.Language';
            return LanguageForm;
        }(Serenity.PrefixedContext));
        Administration.LanguageForm = LanguageForm;
        [['LanguageId', function () { return Serenity.StringEditor; }], ['LanguageName', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(LanguageForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var LanguageRow;
        (function (LanguageRow) {
            LanguageRow.idProperty = 'Id';
            LanguageRow.nameProperty = 'LanguageName';
            LanguageRow.localTextPrefix = 'Administration.Language';
            LanguageRow.lookupKey = 'Administration.Language';
            function getLookup() {
                return Q.getLookup('Administration.Language');
            }
            LanguageRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = LanguageRow.Fields || (LanguageRow.Fields = {}));
            [
                'Id',
                'LanguageId',
                'LanguageName'
            ].forEach(function (x) { return Fields[x] = x; });
        })(LanguageRow = Administration.LanguageRow || (Administration.LanguageRow = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var LanguageService;
        (function (LanguageService) {
            LanguageService.baseUrl = 'Administration/Language';
            var Methods;
            (function (Methods) {
            })(Methods = LanguageService.Methods || (LanguageService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                LanguageService[x] = function (r, s, o) {
                    return Q.serviceRequest(LanguageService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = LanguageService.baseUrl + '/' + x;
            });
        })(LanguageService = Administration.LanguageService || (Administration.LanguageService = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var RoleForm = /** @class */ (function (_super) {
            __extends(RoleForm, _super);
            function RoleForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            RoleForm.formKey = 'Administration.Role';
            return RoleForm;
        }(Serenity.PrefixedContext));
        Administration.RoleForm = RoleForm;
        [['RoleName', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(RoleForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var RolePermissionRow;
        (function (RolePermissionRow) {
            RolePermissionRow.idProperty = 'RolePermissionId';
            RolePermissionRow.nameProperty = 'PermissionKey';
            RolePermissionRow.localTextPrefix = 'Administration.RolePermission';
            var Fields;
            (function (Fields) {
            })(Fields = RolePermissionRow.Fields || (RolePermissionRow.Fields = {}));
            [
                'RolePermissionId',
                'RoleId',
                'PermissionKey',
                'RoleRoleName'
            ].forEach(function (x) { return Fields[x] = x; });
        })(RolePermissionRow = Administration.RolePermissionRow || (Administration.RolePermissionRow = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var RolePermissionService;
        (function (RolePermissionService) {
            RolePermissionService.baseUrl = 'Administration/RolePermission';
            var Methods;
            (function (Methods) {
            })(Methods = RolePermissionService.Methods || (RolePermissionService.Methods = {}));
            [
                'Update',
                'List'
            ].forEach(function (x) {
                RolePermissionService[x] = function (r, s, o) {
                    return Q.serviceRequest(RolePermissionService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = RolePermissionService.baseUrl + '/' + x;
            });
        })(RolePermissionService = Administration.RolePermissionService || (Administration.RolePermissionService = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var RoleRow;
        (function (RoleRow) {
            RoleRow.idProperty = 'RoleId';
            RoleRow.nameProperty = 'RoleName';
            RoleRow.localTextPrefix = 'Administration.Role';
            RoleRow.lookupKey = 'Administration.Role';
            function getLookup() {
                return Q.getLookup('Administration.Role');
            }
            RoleRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = RoleRow.Fields || (RoleRow.Fields = {}));
            [
                'RoleId',
                'RoleName'
            ].forEach(function (x) { return Fields[x] = x; });
        })(RoleRow = Administration.RoleRow || (Administration.RoleRow = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var RoleService;
        (function (RoleService) {
            RoleService.baseUrl = 'Administration/Role';
            var Methods;
            (function (Methods) {
            })(Methods = RoleService.Methods || (RoleService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                RoleService[x] = function (r, s, o) {
                    return Q.serviceRequest(RoleService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = RoleService.baseUrl + '/' + x;
            });
        })(RoleService = Administration.RoleService || (Administration.RoleService = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var TranslationService;
        (function (TranslationService) {
            TranslationService.baseUrl = 'Administration/Translation';
            var Methods;
            (function (Methods) {
            })(Methods = TranslationService.Methods || (TranslationService.Methods = {}));
            [
                'List',
                'Update'
            ].forEach(function (x) {
                TranslationService[x] = function (r, s, o) {
                    return Q.serviceRequest(TranslationService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TranslationService.baseUrl + '/' + x;
            });
        })(TranslationService = Administration.TranslationService || (Administration.TranslationService = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var UserForm = /** @class */ (function (_super) {
            __extends(UserForm, _super);
            function UserForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            UserForm.formKey = 'Administration.User';
            return UserForm;
        }(Serenity.PrefixedContext));
        Administration.UserForm = UserForm;
        [['Username', function () { return Serenity.StringEditor; }], ['DisplayName', function () { return Serenity.StringEditor; }], ['Email', function () { return Serenity.EmailEditor; }], ['UserImage', function () { return Serenity.ImageUploadEditor; }], ['Password', function () { return Serenity.PasswordEditor; }], ['PasswordConfirm', function () { return Serenity.PasswordEditor; }], ['Source', function () { return Serenity.StringEditor; }]].forEach(function (x) { return Object.defineProperty(UserForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var UserPermissionRow;
        (function (UserPermissionRow) {
            UserPermissionRow.idProperty = 'UserPermissionId';
            UserPermissionRow.nameProperty = 'PermissionKey';
            UserPermissionRow.localTextPrefix = 'Administration.UserPermission';
            var Fields;
            (function (Fields) {
            })(Fields = UserPermissionRow.Fields || (UserPermissionRow.Fields = {}));
            [
                'UserPermissionId',
                'UserId',
                'PermissionKey',
                'Granted',
                'Username',
                'User'
            ].forEach(function (x) { return Fields[x] = x; });
        })(UserPermissionRow = Administration.UserPermissionRow || (Administration.UserPermissionRow = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var UserPermissionService;
        (function (UserPermissionService) {
            UserPermissionService.baseUrl = 'Administration/UserPermission';
            var Methods;
            (function (Methods) {
            })(Methods = UserPermissionService.Methods || (UserPermissionService.Methods = {}));
            [
                'Update',
                'List',
                'ListRolePermissions',
                'ListPermissionKeys'
            ].forEach(function (x) {
                UserPermissionService[x] = function (r, s, o) {
                    return Q.serviceRequest(UserPermissionService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = UserPermissionService.baseUrl + '/' + x;
            });
        })(UserPermissionService = Administration.UserPermissionService || (Administration.UserPermissionService = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var UserRoleRow;
        (function (UserRoleRow) {
            UserRoleRow.idProperty = 'UserRoleId';
            UserRoleRow.localTextPrefix = 'Administration.UserRole';
            var Fields;
            (function (Fields) {
            })(Fields = UserRoleRow.Fields || (UserRoleRow.Fields = {}));
            [
                'UserRoleId',
                'UserId',
                'RoleId',
                'Username',
                'User'
            ].forEach(function (x) { return Fields[x] = x; });
        })(UserRoleRow = Administration.UserRoleRow || (Administration.UserRoleRow = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var UserRoleService;
        (function (UserRoleService) {
            UserRoleService.baseUrl = 'Administration/UserRole';
            var Methods;
            (function (Methods) {
            })(Methods = UserRoleService.Methods || (UserRoleService.Methods = {}));
            [
                'Update',
                'List'
            ].forEach(function (x) {
                UserRoleService[x] = function (r, s, o) {
                    return Q.serviceRequest(UserRoleService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = UserRoleService.baseUrl + '/' + x;
            });
        })(UserRoleService = Administration.UserRoleService || (Administration.UserRoleService = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var UserRow;
        (function (UserRow) {
            UserRow.idProperty = 'UserId';
            UserRow.isActiveProperty = 'IsActive';
            UserRow.nameProperty = 'Username';
            UserRow.localTextPrefix = 'Administration.User';
            UserRow.lookupKey = 'Administration.User';
            function getLookup() {
                return Q.getLookup('Administration.User');
            }
            UserRow.getLookup = getLookup;
            var Fields;
            (function (Fields) {
            })(Fields = UserRow.Fields || (UserRow.Fields = {}));
            [
                'UserId',
                'Username',
                'Source',
                'PasswordHash',
                'PasswordSalt',
                'DisplayName',
                'Email',
                'UserImage',
                'LastDirectoryUpdate',
                'IsActive',
                'Password',
                'PasswordConfirm',
                'InsertUserId',
                'InsertDate',
                'UpdateUserId',
                'UpdateDate'
            ].forEach(function (x) { return Fields[x] = x; });
        })(UserRow = Administration.UserRow || (Administration.UserRow = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var UserService;
        (function (UserService) {
            UserService.baseUrl = 'Administration/User';
            var Methods;
            (function (Methods) {
            })(Methods = UserService.Methods || (UserService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Undelete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                UserService[x] = function (r, s, o) {
                    return Q.serviceRequest(UserService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = UserService.baseUrl + '/' + x;
            });
        })(UserService = Administration.UserService || (Administration.UserService = {}));
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Auditorias;
    (function (Auditorias) {
        var Auditoria2016Form = /** @class */ (function (_super) {
            __extends(Auditoria2016Form, _super);
            function Auditoria2016Form() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            Auditoria2016Form.formKey = 'Auditorias.Auditoria2016';
            return Auditoria2016Form;
        }(Serenity.PrefixedContext));
        Auditorias.Auditoria2016Form = Auditoria2016Form;
        [,
            ['Codigo', function () { return Serenity.StringEditor; }],
            ['Grado', function () { return Serenity.IntegerEditor; }],
            ['TipoAu', function () { return Serenity.IntegerEditor; }],
            ['FuenteAu', function () { return Serenity.IntegerEditor; }],
            ['Responsable', function () { return Serenity.IntegerEditor; }],
            ['Gerente', function () { return Serenity.StringEditor; }],
            ['Unidad', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['PlanAccion', function () { return Serenity.StringEditor; }],
            ['Aceptacion', function () { return Serenity.IntegerEditor; }],
            ['Hallazgo', function () { return Serenity.StringEditor; }],
            ['Avance', function () { return Serenity.IntegerEditor; }]
        ].forEach(function (x) { return Object.defineProperty(Auditoria2016Form.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Auditorias = CntAppSeguridad.Auditorias || (CntAppSeguridad.Auditorias = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Auditorias;
    (function (Auditorias) {
        var Auditoria2016Row;
        (function (Auditoria2016Row) {
            Auditoria2016Row.idProperty = 'Id';
            Auditoria2016Row.nameProperty = 'Codigo';
            Auditoria2016Row.localTextPrefix = 'Auditorias.Auditoria2016';
            var Fields;
            (function (Fields) {
            })(Fields = Auditoria2016Row.Fields || (Auditoria2016Row.Fields = {}));
            [
                'Id',
                'Codigo',
                'Grado',
                'TipoAu',
                'FuenteAu',
                'Responsable',
                'Gerente',
                'Unidad',
                'Descripcion',
                'PlanAccion',
                'Aceptacion',
                'Hallazgo',
                'Avance',
                'GradoDescriopcion',
                'TipoAuDescripcion',
                'FuenteAuDescripcion',
                'ResponsableNombre',
                'UnidadNombre',
                'AceptacionDescripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(Auditoria2016Row = Auditorias.Auditoria2016Row || (Auditorias.Auditoria2016Row = {}));
    })(Auditorias = CntAppSeguridad.Auditorias || (CntAppSeguridad.Auditorias = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Auditorias;
    (function (Auditorias) {
        var Auditoria2016Service;
        (function (Auditoria2016Service) {
            Auditoria2016Service.baseUrl = 'Auditorias/Auditoria2016';
            var Methods;
            (function (Methods) {
            })(Methods = Auditoria2016Service.Methods || (Auditoria2016Service.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                Auditoria2016Service[x] = function (r, s, o) {
                    return Q.serviceRequest(Auditoria2016Service.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = Auditoria2016Service.baseUrl + '/' + x;
            });
        })(Auditoria2016Service = Auditorias.Auditoria2016Service || (Auditorias.Auditoria2016Service = {}));
    })(Auditorias = CntAppSeguridad.Auditorias || (CntAppSeguridad.Auditorias = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Auditorias;
    (function (Auditorias) {
        var Auditoria2017Form = /** @class */ (function (_super) {
            __extends(Auditoria2017Form, _super);
            function Auditoria2017Form() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            Auditoria2017Form.formKey = 'Auditorias.Auditoria2017';
            return Auditoria2017Form;
        }(Serenity.PrefixedContext));
        Auditorias.Auditoria2017Form = Auditoria2017Form;
        [,
            ['Codigo', function () { return Serenity.StringEditor; }],
            ['Grado', function () { return Serenity.IntegerEditor; }],
            ['TipoAu', function () { return Serenity.IntegerEditor; }],
            ['FuenteAu', function () { return Serenity.IntegerEditor; }],
            ['Responsable', function () { return Serenity.IntegerEditor; }],
            ['Gerente', function () { return Serenity.StringEditor; }],
            ['Unidad', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['PlanAccion', function () { return Serenity.StringEditor; }],
            ['Aceptacion', function () { return Serenity.IntegerEditor; }],
            ['Hallazgo', function () { return Serenity.StringEditor; }],
            ['Avance', function () { return Serenity.IntegerEditor; }]
        ].forEach(function (x) { return Object.defineProperty(Auditoria2017Form.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Auditorias = CntAppSeguridad.Auditorias || (CntAppSeguridad.Auditorias = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Auditorias;
    (function (Auditorias) {
        var Auditoria2017Row;
        (function (Auditoria2017Row) {
            Auditoria2017Row.idProperty = 'Id';
            Auditoria2017Row.nameProperty = 'Codigo';
            Auditoria2017Row.localTextPrefix = 'Auditorias.Auditoria2017';
            var Fields;
            (function (Fields) {
            })(Fields = Auditoria2017Row.Fields || (Auditoria2017Row.Fields = {}));
            [
                'Id',
                'Codigo',
                'Grado',
                'TipoAu',
                'FuenteAu',
                'Responsable',
                'Gerente',
                'Unidad',
                'Descripcion',
                'PlanAccion',
                'Aceptacion',
                'Hallazgo',
                'Avance',
                'GradoDescriopcion',
                'TipoAuDescripcion',
                'FuenteAuDescripcion',
                'ResponsableNombre',
                'UnidadNombre',
                'AceptacionDescripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(Auditoria2017Row = Auditorias.Auditoria2017Row || (Auditorias.Auditoria2017Row = {}));
    })(Auditorias = CntAppSeguridad.Auditorias || (CntAppSeguridad.Auditorias = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Auditorias;
    (function (Auditorias) {
        var Auditoria2017Service;
        (function (Auditoria2017Service) {
            Auditoria2017Service.baseUrl = 'Auditorias/Auditoria2017';
            var Methods;
            (function (Methods) {
            })(Methods = Auditoria2017Service.Methods || (Auditoria2017Service.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                Auditoria2017Service[x] = function (r, s, o) {
                    return Q.serviceRequest(Auditoria2017Service.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = Auditoria2017Service.baseUrl + '/' + x;
            });
        })(Auditoria2017Service = Auditorias.Auditoria2017Service || (Auditorias.Auditoria2017Service = {}));
    })(Auditorias = CntAppSeguridad.Auditorias || (CntAppSeguridad.Auditorias = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var UserPreferenceRow;
        (function (UserPreferenceRow) {
            UserPreferenceRow.idProperty = 'UserPreferenceId';
            UserPreferenceRow.nameProperty = 'Name';
            UserPreferenceRow.localTextPrefix = 'Common.UserPreference';
            var Fields;
            (function (Fields) {
            })(Fields = UserPreferenceRow.Fields || (UserPreferenceRow.Fields = {}));
            [
                'UserPreferenceId',
                'UserId',
                'PreferenceType',
                'Name',
                'Value'
            ].forEach(function (x) { return Fields[x] = x; });
        })(UserPreferenceRow = Common.UserPreferenceRow || (Common.UserPreferenceRow = {}));
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var UserPreferenceService;
        (function (UserPreferenceService) {
            UserPreferenceService.baseUrl = 'Common/UserPreference';
            var Methods;
            (function (Methods) {
            })(Methods = UserPreferenceService.Methods || (UserPreferenceService.Methods = {}));
            [
                'Update',
                'Retrieve'
            ].forEach(function (x) {
                UserPreferenceService[x] = function (r, s, o) {
                    return Q.serviceRequest(UserPreferenceService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = UserPreferenceService.baseUrl + '/' + x;
            });
        })(UserPreferenceService = Common.UserPreferenceService || (Common.UserPreferenceService = {}));
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var AceptacionAuForm = /** @class */ (function (_super) {
            __extends(AceptacionAuForm, _super);
            function AceptacionAuForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AceptacionAuForm.formKey = 'Configuracion.AceptacionAu';
            return AceptacionAuForm;
        }(Serenity.PrefixedContext));
        Configuracion.AceptacionAuForm = AceptacionAuForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(AceptacionAuForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var AceptacionAuRow;
        (function (AceptacionAuRow) {
            AceptacionAuRow.idProperty = 'Id';
            AceptacionAuRow.nameProperty = 'Descripcion';
            AceptacionAuRow.localTextPrefix = 'Configuracion.AceptacionAu';
            var Fields;
            (function (Fields) {
            })(Fields = AceptacionAuRow.Fields || (AceptacionAuRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(AceptacionAuRow = Configuracion.AceptacionAuRow || (Configuracion.AceptacionAuRow = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var AceptacionAuService;
        (function (AceptacionAuService) {
            AceptacionAuService.baseUrl = 'Configuracion/AceptacionAu';
            var Methods;
            (function (Methods) {
            })(Methods = AceptacionAuService.Methods || (AceptacionAuService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                AceptacionAuService[x] = function (r, s, o) {
                    return Q.serviceRequest(AceptacionAuService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = AceptacionAuService.baseUrl + '/' + x;
            });
        })(AceptacionAuService = Configuracion.AceptacionAuService || (Configuracion.AceptacionAuService = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var AnalistaForm = /** @class */ (function (_super) {
            __extends(AnalistaForm, _super);
            function AnalistaForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AnalistaForm.formKey = 'Configuracion.Analista';
            return AnalistaForm;
        }(Serenity.PrefixedContext));
        Configuracion.AnalistaForm = AnalistaForm;
        [,
            ['Nombre', function () { return Serenity.StringEditor; }],
            ['Estado', function () { return Serenity.IntegerEditor; }]
        ].forEach(function (x) { return Object.defineProperty(AnalistaForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var AnalistaRow;
        (function (AnalistaRow) {
            AnalistaRow.idProperty = 'Id';
            AnalistaRow.nameProperty = 'Nombre';
            AnalistaRow.localTextPrefix = 'Configuracion.Analista';
            var Fields;
            (function (Fields) {
            })(Fields = AnalistaRow.Fields || (AnalistaRow.Fields = {}));
            [
                'Id',
                'Nombre',
                'Estado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(AnalistaRow = Configuracion.AnalistaRow || (Configuracion.AnalistaRow = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var AnalistaService;
        (function (AnalistaService) {
            AnalistaService.baseUrl = 'Configuracion/Analista';
            var Methods;
            (function (Methods) {
            })(Methods = AnalistaService.Methods || (AnalistaService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                AnalistaService[x] = function (r, s, o) {
                    return Q.serviceRequest(AnalistaService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = AnalistaService.baseUrl + '/' + x;
            });
        })(AnalistaService = Configuracion.AnalistaService || (Configuracion.AnalistaService = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var FuenteAuForm = /** @class */ (function (_super) {
            __extends(FuenteAuForm, _super);
            function FuenteAuForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            FuenteAuForm.formKey = 'Configuracion.FuenteAu';
            return FuenteAuForm;
        }(Serenity.PrefixedContext));
        Configuracion.FuenteAuForm = FuenteAuForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(FuenteAuForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var FuenteAuRow;
        (function (FuenteAuRow) {
            FuenteAuRow.idProperty = 'Id';
            FuenteAuRow.nameProperty = 'Descripcion';
            FuenteAuRow.localTextPrefix = 'Configuracion.FuenteAu';
            var Fields;
            (function (Fields) {
            })(Fields = FuenteAuRow.Fields || (FuenteAuRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(FuenteAuRow = Configuracion.FuenteAuRow || (Configuracion.FuenteAuRow = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var FuenteAuService;
        (function (FuenteAuService) {
            FuenteAuService.baseUrl = 'Configuracion/FuenteAu';
            var Methods;
            (function (Methods) {
            })(Methods = FuenteAuService.Methods || (FuenteAuService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                FuenteAuService[x] = function (r, s, o) {
                    return Q.serviceRequest(FuenteAuService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = FuenteAuService.baseUrl + '/' + x;
            });
        })(FuenteAuService = Configuracion.FuenteAuService || (Configuracion.FuenteAuService = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var GradoAuForm = /** @class */ (function (_super) {
            __extends(GradoAuForm, _super);
            function GradoAuForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GradoAuForm.formKey = 'Configuracion.GradoAu';
            return GradoAuForm;
        }(Serenity.PrefixedContext));
        Configuracion.GradoAuForm = GradoAuForm;
        [,
            ['Descriopcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(GradoAuForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var GradoAuRow;
        (function (GradoAuRow) {
            GradoAuRow.idProperty = 'Id';
            GradoAuRow.nameProperty = 'Descriopcion';
            GradoAuRow.localTextPrefix = 'Configuracion.GradoAu';
            var Fields;
            (function (Fields) {
            })(Fields = GradoAuRow.Fields || (GradoAuRow.Fields = {}));
            [
                'Id',
                'Descriopcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(GradoAuRow = Configuracion.GradoAuRow || (Configuracion.GradoAuRow = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var GradoAuService;
        (function (GradoAuService) {
            GradoAuService.baseUrl = 'Configuracion/GradoAu';
            var Methods;
            (function (Methods) {
            })(Methods = GradoAuService.Methods || (GradoAuService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                GradoAuService[x] = function (r, s, o) {
                    return Q.serviceRequest(GradoAuService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = GradoAuService.baseUrl + '/' + x;
            });
        })(GradoAuService = Configuracion.GradoAuService || (Configuracion.GradoAuService = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var TipoAuForm = /** @class */ (function (_super) {
            __extends(TipoAuForm, _super);
            function TipoAuForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TipoAuForm.formKey = 'Configuracion.TipoAu';
            return TipoAuForm;
        }(Serenity.PrefixedContext));
        Configuracion.TipoAuForm = TipoAuForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TipoAuForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var TipoAuRow;
        (function (TipoAuRow) {
            TipoAuRow.idProperty = 'Id';
            TipoAuRow.nameProperty = 'Descripcion';
            TipoAuRow.localTextPrefix = 'Configuracion.TipoAu';
            var Fields;
            (function (Fields) {
            })(Fields = TipoAuRow.Fields || (TipoAuRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TipoAuRow = Configuracion.TipoAuRow || (Configuracion.TipoAuRow = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var TipoAuService;
        (function (TipoAuService) {
            TipoAuService.baseUrl = 'Configuracion/TipoAu';
            var Methods;
            (function (Methods) {
            })(Methods = TipoAuService.Methods || (TipoAuService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TipoAuService[x] = function (r, s, o) {
                    return Q.serviceRequest(TipoAuService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TipoAuService.baseUrl + '/' + x;
            });
        })(TipoAuService = Configuracion.TipoAuService || (Configuracion.TipoAuService = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var TipoOficioForm = /** @class */ (function (_super) {
            __extends(TipoOficioForm, _super);
            function TipoOficioForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TipoOficioForm.formKey = 'Configuracion.TipoOficio';
            return TipoOficioForm;
        }(Serenity.PrefixedContext));
        Configuracion.TipoOficioForm = TipoOficioForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TipoOficioForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var TipoOficioRow;
        (function (TipoOficioRow) {
            TipoOficioRow.idProperty = 'Id';
            TipoOficioRow.nameProperty = 'Descripcion';
            TipoOficioRow.localTextPrefix = 'Configuracion.TipoOficio';
            var Fields;
            (function (Fields) {
            })(Fields = TipoOficioRow.Fields || (TipoOficioRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TipoOficioRow = Configuracion.TipoOficioRow || (Configuracion.TipoOficioRow = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var TipoOficioService;
        (function (TipoOficioService) {
            TipoOficioService.baseUrl = 'Configuracion/TipoOficio';
            var Methods;
            (function (Methods) {
            })(Methods = TipoOficioService.Methods || (TipoOficioService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TipoOficioService[x] = function (r, s, o) {
                    return Q.serviceRequest(TipoOficioService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TipoOficioService.baseUrl + '/' + x;
            });
        })(TipoOficioService = Configuracion.TipoOficioService || (Configuracion.TipoOficioService = {}));
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var ActaBienForm = /** @class */ (function (_super) {
            __extends(ActaBienForm, _super);
            function ActaBienForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ActaBienForm.formKey = 'Documentos.ActaBien';
            return ActaBienForm;
        }(Serenity.PrefixedContext));
        Documentos.ActaBienForm = ActaBienForm;
        [,
            ['Numero', function () { return Serenity.StringEditor; }],
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Observacion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(ActaBienForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var ActaBienRow;
        (function (ActaBienRow) {
            ActaBienRow.idProperty = 'Id';
            ActaBienRow.nameProperty = 'Numero';
            ActaBienRow.localTextPrefix = 'Documentos.ActaBien';
            var Fields;
            (function (Fields) {
            })(Fields = ActaBienRow.Fields || (ActaBienRow.Fields = {}));
            [
                'Id',
                'Numero',
                'Analista',
                'Observacion',
                'AnalistaNombre',
                'AnalistaEstado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(ActaBienRow = Documentos.ActaBienRow || (Documentos.ActaBienRow = {}));
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var ActaBienService;
        (function (ActaBienService) {
            ActaBienService.baseUrl = 'Documentos/ActaBien';
            var Methods;
            (function (Methods) {
            })(Methods = ActaBienService.Methods || (ActaBienService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ActaBienService[x] = function (r, s, o) {
                    return Q.serviceRequest(ActaBienService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = ActaBienService.baseUrl + '/' + x;
            });
        })(ActaBienService = Documentos.ActaBienService || (Documentos.ActaBienService = {}));
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var ActaReunionForm = /** @class */ (function (_super) {
            __extends(ActaReunionForm, _super);
            function ActaReunionForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ActaReunionForm.formKey = 'Documentos.ActaReunion';
            return ActaReunionForm;
        }(Serenity.PrefixedContext));
        Documentos.ActaReunionForm = ActaReunionForm;
        [,
            ['Numero', function () { return Serenity.StringEditor; }],
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Observacion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(ActaReunionForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var ActaReunionRow;
        (function (ActaReunionRow) {
            ActaReunionRow.idProperty = 'Id';
            ActaReunionRow.nameProperty = 'Numero';
            ActaReunionRow.localTextPrefix = 'Documentos.ActaReunion';
            var Fields;
            (function (Fields) {
            })(Fields = ActaReunionRow.Fields || (ActaReunionRow.Fields = {}));
            [
                'Id',
                'Numero',
                'Analista',
                'Descripcion',
                'Observacion',
                'AnalistaNombre',
                'AnalistaEstado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(ActaReunionRow = Documentos.ActaReunionRow || (Documentos.ActaReunionRow = {}));
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var ActaReunionService;
        (function (ActaReunionService) {
            ActaReunionService.baseUrl = 'Documentos/ActaReunion';
            var Methods;
            (function (Methods) {
            })(Methods = ActaReunionService.Methods || (ActaReunionService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ActaReunionService[x] = function (r, s, o) {
                    return Q.serviceRequest(ActaReunionService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = ActaReunionService.baseUrl + '/' + x;
            });
        })(ActaReunionService = Documentos.ActaReunionService || (Documentos.ActaReunionService = {}));
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeMensualForm = /** @class */ (function (_super) {
            __extends(InformeMensualForm, _super);
            function InformeMensualForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            InformeMensualForm.formKey = 'Documentos.InformeMensual';
            return InformeMensualForm;
        }(Serenity.PrefixedContext));
        Documentos.InformeMensualForm = InformeMensualForm;
        [,
            ['Numero', function () { return Serenity.StringEditor; }],
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Fecha', function () { return Serenity.DateEditor; }],
            ['Observacion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(InformeMensualForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeMensualRow;
        (function (InformeMensualRow) {
            InformeMensualRow.idProperty = 'Id';
            InformeMensualRow.nameProperty = 'Numero';
            InformeMensualRow.localTextPrefix = 'Documentos.InformeMensual';
            var Fields;
            (function (Fields) {
            })(Fields = InformeMensualRow.Fields || (InformeMensualRow.Fields = {}));
            [
                'Id',
                'Numero',
                'Analista',
                'Descripcion',
                'Fecha',
                'Observacion',
                'AnalistaNombre',
                'AnalistaEstado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(InformeMensualRow = Documentos.InformeMensualRow || (Documentos.InformeMensualRow = {}));
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeMensualService;
        (function (InformeMensualService) {
            InformeMensualService.baseUrl = 'Documentos/InformeMensual';
            var Methods;
            (function (Methods) {
            })(Methods = InformeMensualService.Methods || (InformeMensualService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                InformeMensualService[x] = function (r, s, o) {
                    return Q.serviceRequest(InformeMensualService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = InformeMensualService.baseUrl + '/' + x;
            });
        })(InformeMensualService = Documentos.InformeMensualService || (Documentos.InformeMensualService = {}));
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeTecnicoForm = /** @class */ (function (_super) {
            __extends(InformeTecnicoForm, _super);
            function InformeTecnicoForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            InformeTecnicoForm.formKey = 'Documentos.InformeTecnico';
            return InformeTecnicoForm;
        }(Serenity.PrefixedContext));
        Documentos.InformeTecnicoForm = InformeTecnicoForm;
        [,
            ['Numero', function () { return Serenity.StringEditor; }],
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['FechaInforme', function () { return Serenity.DateEditor; }],
            ['Observacion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(InformeTecnicoForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeTecnicoRow;
        (function (InformeTecnicoRow) {
            InformeTecnicoRow.idProperty = 'Id';
            InformeTecnicoRow.nameProperty = 'Numero';
            InformeTecnicoRow.localTextPrefix = 'Documentos.InformeTecnico';
            var Fields;
            (function (Fields) {
            })(Fields = InformeTecnicoRow.Fields || (InformeTecnicoRow.Fields = {}));
            [
                'Id',
                'Numero',
                'Analista',
                'Descripcion',
                'FechaInforme',
                'Observacion',
                'AnalistaNombre',
                'AnalistaEstado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(InformeTecnicoRow = Documentos.InformeTecnicoRow || (Documentos.InformeTecnicoRow = {}));
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeTecnicoService;
        (function (InformeTecnicoService) {
            InformeTecnicoService.baseUrl = 'Documentos/InformeTecnico';
            var Methods;
            (function (Methods) {
            })(Methods = InformeTecnicoService.Methods || (InformeTecnicoService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                InformeTecnicoService[x] = function (r, s, o) {
                    return Q.serviceRequest(InformeTecnicoService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = InformeTecnicoService.baseUrl + '/' + x;
            });
        })(InformeTecnicoService = Documentos.InformeTecnicoService || (Documentos.InformeTecnicoService = {}));
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeTrimestralForm = /** @class */ (function (_super) {
            __extends(InformeTrimestralForm, _super);
            function InformeTrimestralForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            InformeTrimestralForm.formKey = 'Documentos.InformeTrimestral';
            return InformeTrimestralForm;
        }(Serenity.PrefixedContext));
        Documentos.InformeTrimestralForm = InformeTrimestralForm;
        [,
            ['Numero', function () { return Serenity.StringEditor; }],
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Destinatario', function () { return Serenity.StringEditor; }],
            ['Fecha', function () { return Serenity.DateEditor; }],
            ['Tipo', function () { return Serenity.StringEditor; }],
            ['Archivo', function () { return Serenity.StringEditor; }],
            ['Nota', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(InformeTrimestralForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeTrimestralRow;
        (function (InformeTrimestralRow) {
            InformeTrimestralRow.idProperty = 'Id';
            InformeTrimestralRow.nameProperty = 'Numero';
            InformeTrimestralRow.localTextPrefix = 'Documentos.InformeTrimestral';
            var Fields;
            (function (Fields) {
            })(Fields = InformeTrimestralRow.Fields || (InformeTrimestralRow.Fields = {}));
            [
                'Id',
                'Numero',
                'Analista',
                'Destinatario',
                'Fecha',
                'Tipo',
                'Archivo',
                'Nota',
                'AnalistaNombre',
                'AnalistaEstado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(InformeTrimestralRow = Documentos.InformeTrimestralRow || (Documentos.InformeTrimestralRow = {}));
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeTrimestralService;
        (function (InformeTrimestralService) {
            InformeTrimestralService.baseUrl = 'Documentos/InformeTrimestral';
            var Methods;
            (function (Methods) {
            })(Methods = InformeTrimestralService.Methods || (InformeTrimestralService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                InformeTrimestralService[x] = function (r, s, o) {
                    return Q.serviceRequest(InformeTrimestralService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = InformeTrimestralService.baseUrl + '/' + x;
            });
        })(InformeTrimestralService = Documentos.InformeTrimestralService || (Documentos.InformeTrimestralService = {}));
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Egsi;
    (function (Egsi) {
        var HitoForm = /** @class */ (function (_super) {
            __extends(HitoForm, _super);
            function HitoForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            HitoForm.formKey = 'Egsi.Hito';
            return HitoForm;
        }(Serenity.PrefixedContext));
        Egsi.HitoForm = HitoForm;
        [,
            ['Area', function () { return Serenity.IntegerEditor; }],
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Procedimiento', function () { return Serenity.StringEditor; }],
            ['Verificable', function () { return Serenity.StringEditor; }],
            ['Nota', function () { return Serenity.StringEditor; }],
            ['Cumple', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(HitoForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Egsi = CntAppSeguridad.Egsi || (CntAppSeguridad.Egsi = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Egsi;
    (function (Egsi) {
        var HitoRow;
        (function (HitoRow) {
            HitoRow.idProperty = 'Id';
            HitoRow.nameProperty = 'Descripcion';
            HitoRow.localTextPrefix = 'Egsi.Hito';
            var Fields;
            (function (Fields) {
            })(Fields = HitoRow.Fields || (HitoRow.Fields = {}));
            [
                'Id',
                'Area',
                'Analista',
                'Descripcion',
                'Procedimiento',
                'Verificable',
                'Nota',
                'Cumple',
                'AreaNombre',
                'AnalistaNombre',
                'AnalistaEstado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(HitoRow = Egsi.HitoRow || (Egsi.HitoRow = {}));
    })(Egsi = CntAppSeguridad.Egsi || (CntAppSeguridad.Egsi = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Egsi;
    (function (Egsi) {
        var HitoService;
        (function (HitoService) {
            HitoService.baseUrl = 'Egsi/Hito';
            var Methods;
            (function (Methods) {
            })(Methods = HitoService.Methods || (HitoService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                HitoService[x] = function (r, s, o) {
                    return Q.serviceRequest(HitoService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = HitoService.baseUrl + '/' + x;
            });
        })(HitoService = Egsi.HitoService || (Egsi.HitoService = {}));
    })(Egsi = CntAppSeguridad.Egsi || (CntAppSeguridad.Egsi = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var EsquemaEgsi;
    (function (EsquemaEgsi) {
        var HitoForm = /** @class */ (function (_super) {
            __extends(HitoForm, _super);
            function HitoForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            HitoForm.formKey = 'EsquemaEgsi.Hito';
            return HitoForm;
        }(Serenity.PrefixedContext));
        EsquemaEgsi.HitoForm = HitoForm;
        [,
            ['Numero', function () { return Serenity.StringEditor; }],
            ['Area', function () { return Serenity.IntegerEditor; }],
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Procedimiento', function () { return Serenity.StringEditor; }],
            ['Verificable', function () { return Serenity.StringEditor; }],
            ['Porcentaje', function () { return Serenity.IntegerEditor; }],
            ['Nota', function () { return Serenity.StringEditor; }],
            ['Cumple', function () { return Serenity.IntegerEditor; }]
        ].forEach(function (x) { return Object.defineProperty(HitoForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(EsquemaEgsi = CntAppSeguridad.EsquemaEgsi || (CntAppSeguridad.EsquemaEgsi = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var EsquemaEgsi;
    (function (EsquemaEgsi) {
        var HitoRow;
        (function (HitoRow) {
            HitoRow.idProperty = 'Id';
            HitoRow.nameProperty = 'Numero';
            HitoRow.localTextPrefix = 'EsquemaEgsi.Hito';
            var Fields;
            (function (Fields) {
            })(Fields = HitoRow.Fields || (HitoRow.Fields = {}));
            [
                'Id',
                'Numero',
                'Area',
                'Analista',
                'Descripcion',
                'Procedimiento',
                'Verificable',
                'Porcentaje',
                'Nota',
                'Cumple',
                'AreaNombre',
                'AnalistaNombre',
                'AnalistaEstado',
                'CumpleDescripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(HitoRow = EsquemaEgsi.HitoRow || (EsquemaEgsi.HitoRow = {}));
    })(EsquemaEgsi = CntAppSeguridad.EsquemaEgsi || (CntAppSeguridad.EsquemaEgsi = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var EsquemaEgsi;
    (function (EsquemaEgsi) {
        var HitoService;
        (function (HitoService) {
            HitoService.baseUrl = 'EsquemaEgsi/Hito';
            var Methods;
            (function (Methods) {
            })(Methods = HitoService.Methods || (HitoService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                HitoService[x] = function (r, s, o) {
                    return Q.serviceRequest(HitoService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = HitoService.baseUrl + '/' + x;
            });
        })(HitoService = EsquemaEgsi.HitoService || (EsquemaEgsi.HitoService = {}));
    })(EsquemaEgsi = CntAppSeguridad.EsquemaEgsi || (CntAppSeguridad.EsquemaEgsi = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var EsquemaGubernamentalSI;
    (function (EsquemaGubernamentalSI) {
        var HitoForm = /** @class */ (function (_super) {
            __extends(HitoForm, _super);
            function HitoForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            HitoForm.formKey = 'EsquemaGubernamentalSI.Hito';
            return HitoForm;
        }(Serenity.PrefixedContext));
        EsquemaGubernamentalSI.HitoForm = HitoForm;
        [,
            ['Numero', function () { return Serenity.StringEditor; }],
            ['Area', function () { return Serenity.IntegerEditor; }],
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Procedimiento', function () { return Serenity.StringEditor; }],
            ['Verificable', function () { return Serenity.StringEditor; }],
            ['Porcentaje', function () { return Serenity.IntegerEditor; }],
            ['Nota', function () { return Serenity.StringEditor; }],
            ['Cumple', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(HitoForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(EsquemaGubernamentalSI = CntAppSeguridad.EsquemaGubernamentalSI || (CntAppSeguridad.EsquemaGubernamentalSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var EsquemaGubernamentalSI;
    (function (EsquemaGubernamentalSI) {
        var HitoRow;
        (function (HitoRow) {
            HitoRow.idProperty = 'Id';
            HitoRow.nameProperty = 'Numero';
            HitoRow.localTextPrefix = 'EsquemaGubernamentalSI.Hito';
            var Fields;
            (function (Fields) {
            })(Fields = HitoRow.Fields || (HitoRow.Fields = {}));
            [
                'Id',
                'Numero',
                'Area',
                'Analista',
                'Descripcion',
                'Procedimiento',
                'Verificable',
                'Porcentaje',
                'Nota',
                'Cumple',
                'AreaNombre',
                'AnalistaNombre',
                'AnalistaEstado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(HitoRow = EsquemaGubernamentalSI.HitoRow || (EsquemaGubernamentalSI.HitoRow = {}));
    })(EsquemaGubernamentalSI = CntAppSeguridad.EsquemaGubernamentalSI || (CntAppSeguridad.EsquemaGubernamentalSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var EsquemaGubernamentalSI;
    (function (EsquemaGubernamentalSI) {
        var HitoService;
        (function (HitoService) {
            HitoService.baseUrl = 'EsquemaGubernamentalSI/Hito';
            var Methods;
            (function (Methods) {
            })(Methods = HitoService.Methods || (HitoService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                HitoService[x] = function (r, s, o) {
                    return Q.serviceRequest(HitoService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = HitoService.baseUrl + '/' + x;
            });
        })(HitoService = EsquemaGubernamentalSI.HitoService || (EsquemaGubernamentalSI.HitoService = {}));
    })(EsquemaGubernamentalSI = CntAppSeguridad.EsquemaGubernamentalSI || (CntAppSeguridad.EsquemaGubernamentalSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Membership;
    (function (Membership) {
        var ChangePasswordForm = /** @class */ (function (_super) {
            __extends(ChangePasswordForm, _super);
            function ChangePasswordForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ChangePasswordForm.formKey = 'Membership.ChangePassword';
            return ChangePasswordForm;
        }(Serenity.PrefixedContext));
        Membership.ChangePasswordForm = ChangePasswordForm;
        [['OldPassword', function () { return Serenity.PasswordEditor; }], ['NewPassword', function () { return Serenity.PasswordEditor; }], ['ConfirmPassword', function () { return Serenity.PasswordEditor; }]].forEach(function (x) { return Object.defineProperty(ChangePasswordForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Membership = CntAppSeguridad.Membership || (CntAppSeguridad.Membership = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Membership;
    (function (Membership) {
        var ForgotPasswordForm = /** @class */ (function (_super) {
            __extends(ForgotPasswordForm, _super);
            function ForgotPasswordForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ForgotPasswordForm.formKey = 'Membership.ForgotPassword';
            return ForgotPasswordForm;
        }(Serenity.PrefixedContext));
        Membership.ForgotPasswordForm = ForgotPasswordForm;
        [['Email', function () { return Serenity.EmailEditor; }]].forEach(function (x) { return Object.defineProperty(ForgotPasswordForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Membership = CntAppSeguridad.Membership || (CntAppSeguridad.Membership = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Membership;
    (function (Membership) {
        var LoginForm = /** @class */ (function (_super) {
            __extends(LoginForm, _super);
            function LoginForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            LoginForm.formKey = 'Membership.Login';
            return LoginForm;
        }(Serenity.PrefixedContext));
        Membership.LoginForm = LoginForm;
        [['Username', function () { return Serenity.StringEditor; }], ['Password', function () { return Serenity.PasswordEditor; }]].forEach(function (x) { return Object.defineProperty(LoginForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Membership = CntAppSeguridad.Membership || (CntAppSeguridad.Membership = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Membership;
    (function (Membership) {
        var ResetPasswordForm = /** @class */ (function (_super) {
            __extends(ResetPasswordForm, _super);
            function ResetPasswordForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ResetPasswordForm.formKey = 'Membership.ResetPassword';
            return ResetPasswordForm;
        }(Serenity.PrefixedContext));
        Membership.ResetPasswordForm = ResetPasswordForm;
        [['NewPassword', function () { return Serenity.PasswordEditor; }], ['ConfirmPassword', function () { return Serenity.PasswordEditor; }]].forEach(function (x) { return Object.defineProperty(ResetPasswordForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Membership = CntAppSeguridad.Membership || (CntAppSeguridad.Membership = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Membership;
    (function (Membership) {
        var SignUpForm = /** @class */ (function (_super) {
            __extends(SignUpForm, _super);
            function SignUpForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            SignUpForm.formKey = 'Membership.SignUp';
            return SignUpForm;
        }(Serenity.PrefixedContext));
        Membership.SignUpForm = SignUpForm;
        [['DisplayName', function () { return Serenity.StringEditor; }], ['Email', function () { return Serenity.EmailEditor; }], ['ConfirmEmail', function () { return Serenity.EmailEditor; }], ['Password', function () { return Serenity.PasswordEditor; }], ['ConfirmPassword', function () { return Serenity.PasswordEditor; }]].forEach(function (x) { return Object.defineProperty(SignUpForm.prototype, x[0], { get: function () { return this.w(x[0], x[1]()); }, enumerable: true, configurable: true }); });
    })(Membership = CntAppSeguridad.Membership || (CntAppSeguridad.Membership = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var AreaForm = /** @class */ (function (_super) {
            __extends(AreaForm, _super);
            function AreaForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AreaForm.formKey = 'Oficios.Area';
            return AreaForm;
        }(Serenity.PrefixedContext));
        Oficios.AreaForm = AreaForm;
        [,
            ['Nombre', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(AreaForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var AreaRow;
        (function (AreaRow) {
            AreaRow.idProperty = 'Id';
            AreaRow.nameProperty = 'Nombre';
            AreaRow.localTextPrefix = 'Oficios.Area';
            var Fields;
            (function (Fields) {
            })(Fields = AreaRow.Fields || (AreaRow.Fields = {}));
            [
                'Id',
                'Nombre'
            ].forEach(function (x) { return Fields[x] = x; });
        })(AreaRow = Oficios.AreaRow || (Oficios.AreaRow = {}));
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var AreaService;
        (function (AreaService) {
            AreaService.baseUrl = 'Oficios/Area';
            var Methods;
            (function (Methods) {
            })(Methods = AreaService.Methods || (AreaService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                AreaService[x] = function (r, s, o) {
                    return Q.serviceRequest(AreaService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = AreaService.baseUrl + '/' + x;
            });
        })(AreaService = Oficios.AreaService || (Oficios.AreaService = {}));
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var OficioForm = /** @class */ (function (_super) {
            __extends(OficioForm, _super);
            function OficioForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            OficioForm.formKey = 'Oficios.Oficio';
            return OficioForm;
        }(Serenity.PrefixedContext));
        Oficios.OficioForm = OficioForm;
        [,
            ['Area', function () { return Serenity.IntegerEditor; }],
            ['Responsable', function () { return Serenity.IntegerEditor; }],
            ['NumeroOficio', function () { return Serenity.StringEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Fecha', function () { return Serenity.DateEditor; }],
            ['Destinatario', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(OficioForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var OficioRow;
        (function (OficioRow) {
            OficioRow.idProperty = 'Id';
            OficioRow.nameProperty = 'NumeroOficio';
            OficioRow.localTextPrefix = 'Oficios.Oficio';
            var Fields;
            (function (Fields) {
            })(Fields = OficioRow.Fields || (OficioRow.Fields = {}));
            [
                'Id',
                'Area',
                'Responsable',
                'NumeroOficio',
                'Descripcion',
                'Fecha',
                'Destinatario',
                'AreaNombre',
                'ResponsableNombre'
            ].forEach(function (x) { return Fields[x] = x; });
        })(OficioRow = Oficios.OficioRow || (Oficios.OficioRow = {}));
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var OficioService;
        (function (OficioService) {
            OficioService.baseUrl = 'Oficios/Oficio';
            var Methods;
            (function (Methods) {
            })(Methods = OficioService.Methods || (OficioService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                OficioService[x] = function (r, s, o) {
                    return Q.serviceRequest(OficioService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = OficioService.baseUrl + '/' + x;
            });
        })(OficioService = Oficios.OficioService || (Oficios.OficioService = {}));
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var OficioSiForm = /** @class */ (function (_super) {
            __extends(OficioSiForm, _super);
            function OficioSiForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            OficioSiForm.formKey = 'Oficios.OficioSi';
            return OficioSiForm;
        }(Serenity.PrefixedContext));
        Oficios.OficioSiForm = OficioSiForm;
        [,
            ['Area', function () { return Serenity.IntegerEditor; }],
            ['Responsable', function () { return Serenity.IntegerEditor; }],
            ['NumeroOficio', function () { return Serenity.StringEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Fecha', function () { return Serenity.DateEditor; }],
            ['Destinatario', function () { return Serenity.StringEditor; }],
            ['Archivo', function () { return Serenity.StringEditor; }],
            ['Tipo', function () { return Serenity.IntegerEditor; }]
        ].forEach(function (x) { return Object.defineProperty(OficioSiForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var OficioSiRow;
        (function (OficioSiRow) {
            OficioSiRow.idProperty = 'Id';
            OficioSiRow.nameProperty = 'NumeroOficio';
            OficioSiRow.localTextPrefix = 'Oficios.OficioSi';
            var Fields;
            (function (Fields) {
            })(Fields = OficioSiRow.Fields || (OficioSiRow.Fields = {}));
            [
                'Id',
                'Area',
                'Responsable',
                'NumeroOficio',
                'Descripcion',
                'Fecha',
                'Destinatario',
                'Archivo',
                'Tipo',
                'AreaNombre',
                'ResponsableNombre',
                'TipoDescripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(OficioSiRow = Oficios.OficioSiRow || (Oficios.OficioSiRow = {}));
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var OficioSiService;
        (function (OficioSiService) {
            OficioSiService.baseUrl = 'Oficios/OficioSi';
            var Methods;
            (function (Methods) {
            })(Methods = OficioSiService.Methods || (OficioSiService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                OficioSiService[x] = function (r, s, o) {
                    return Q.serviceRequest(OficioSiService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = OficioSiService.baseUrl + '/' + x;
            });
        })(OficioSiService = Oficios.OficioSiService || (Oficios.OficioSiService = {}));
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var ResponsableForm = /** @class */ (function (_super) {
            __extends(ResponsableForm, _super);
            function ResponsableForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ResponsableForm.formKey = 'Oficios.Responsable';
            return ResponsableForm;
        }(Serenity.PrefixedContext));
        Oficios.ResponsableForm = ResponsableForm;
        [,
            ['Nombre', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(ResponsableForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var ResponsableRow;
        (function (ResponsableRow) {
            ResponsableRow.idProperty = 'Id';
            ResponsableRow.nameProperty = 'Nombre';
            ResponsableRow.localTextPrefix = 'Oficios.Responsable';
            var Fields;
            (function (Fields) {
            })(Fields = ResponsableRow.Fields || (ResponsableRow.Fields = {}));
            [
                'Id',
                'Nombre'
            ].forEach(function (x) { return Fields[x] = x; });
        })(ResponsableRow = Oficios.ResponsableRow || (Oficios.ResponsableRow = {}));
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var ResponsableService;
        (function (ResponsableService) {
            ResponsableService.baseUrl = 'Oficios/Responsable';
            var Methods;
            (function (Methods) {
            })(Methods = ResponsableService.Methods || (ResponsableService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ResponsableService[x] = function (r, s, o) {
                    return Q.serviceRequest(ResponsableService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = ResponsableService.baseUrl + '/' + x;
            });
        })(ResponsableService = Oficios.ResponsableService || (Oficios.ResponsableService = {}));
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var ActividadesPForm = /** @class */ (function (_super) {
            __extends(ActividadesPForm, _super);
            function ActividadesPForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ActividadesPForm.formKey = 'PETI.ActividadesP';
            return ActividadesPForm;
        }(Serenity.PrefixedContext));
        PETI.ActividadesPForm = ActividadesPForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(ActividadesPForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var ActividadesPRow;
        (function (ActividadesPRow) {
            ActividadesPRow.idProperty = 'Id';
            ActividadesPRow.nameProperty = 'Descripcion';
            ActividadesPRow.localTextPrefix = 'PETI.ActividadesP';
            var Fields;
            (function (Fields) {
            })(Fields = ActividadesPRow.Fields || (ActividadesPRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(ActividadesPRow = PETI.ActividadesPRow || (PETI.ActividadesPRow = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var ActividadesPService;
        (function (ActividadesPService) {
            ActividadesPService.baseUrl = 'PETI/ActividadesP';
            var Methods;
            (function (Methods) {
            })(Methods = ActividadesPService.Methods || (ActividadesPService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ActividadesPService[x] = function (r, s, o) {
                    return Q.serviceRequest(ActividadesPService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = ActividadesPService.baseUrl + '/' + x;
            });
        })(ActividadesPService = PETI.ActividadesPService || (PETI.ActividadesPService = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var AnalistaForm = /** @class */ (function (_super) {
            __extends(AnalistaForm, _super);
            function AnalistaForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AnalistaForm.formKey = 'PETI.Analista';
            return AnalistaForm;
        }(Serenity.PrefixedContext));
        PETI.AnalistaForm = AnalistaForm;
        [,
            ['Nombre', function () { return Serenity.StringEditor; }],
            ['Estado', function () { return Serenity.IntegerEditor; }]
        ].forEach(function (x) { return Object.defineProperty(AnalistaForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var AnalistaRow;
        (function (AnalistaRow) {
            AnalistaRow.idProperty = 'Id';
            AnalistaRow.nameProperty = 'Nombre';
            AnalistaRow.localTextPrefix = 'PETI.Analista';
            var Fields;
            (function (Fields) {
            })(Fields = AnalistaRow.Fields || (AnalistaRow.Fields = {}));
            [
                'Id',
                'Nombre',
                'Estado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(AnalistaRow = PETI.AnalistaRow || (PETI.AnalistaRow = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var AnalistaService;
        (function (AnalistaService) {
            AnalistaService.baseUrl = 'PETI/Analista';
            var Methods;
            (function (Methods) {
            })(Methods = AnalistaService.Methods || (AnalistaService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                AnalistaService[x] = function (r, s, o) {
                    return Q.serviceRequest(AnalistaService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = AnalistaService.baseUrl + '/' + x;
            });
        })(AnalistaService = PETI.AnalistaService || (PETI.AnalistaService = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var AnioForm = /** @class */ (function (_super) {
            __extends(AnioForm, _super);
            function AnioForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AnioForm.formKey = 'PETI.Anio';
            return AnioForm;
        }(Serenity.PrefixedContext));
        PETI.AnioForm = AnioForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(AnioForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var AnioRow;
        (function (AnioRow) {
            AnioRow.idProperty = 'Id';
            AnioRow.nameProperty = 'Descripcion';
            AnioRow.localTextPrefix = 'PETI.Anio';
            var Fields;
            (function (Fields) {
            })(Fields = AnioRow.Fields || (AnioRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(AnioRow = PETI.AnioRow || (PETI.AnioRow = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var AnioService;
        (function (AnioService) {
            AnioService.baseUrl = 'PETI/Anio';
            var Methods;
            (function (Methods) {
            })(Methods = AnioService.Methods || (AnioService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                AnioService[x] = function (r, s, o) {
                    return Q.serviceRequest(AnioService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = AnioService.baseUrl + '/' + x;
            });
        })(AnioService = PETI.AnioService || (PETI.AnioService = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var EstadoPForm = /** @class */ (function (_super) {
            __extends(EstadoPForm, _super);
            function EstadoPForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            EstadoPForm.formKey = 'PETI.EstadoP';
            return EstadoPForm;
        }(Serenity.PrefixedContext));
        PETI.EstadoPForm = EstadoPForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(EstadoPForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var EstadoPRow;
        (function (EstadoPRow) {
            EstadoPRow.idProperty = 'Id';
            EstadoPRow.nameProperty = 'Descripcion';
            EstadoPRow.localTextPrefix = 'PETI.EstadoP';
            var Fields;
            (function (Fields) {
            })(Fields = EstadoPRow.Fields || (EstadoPRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(EstadoPRow = PETI.EstadoPRow || (PETI.EstadoPRow = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var EstadoPService;
        (function (EstadoPService) {
            EstadoPService.baseUrl = 'PETI/EstadoP';
            var Methods;
            (function (Methods) {
            })(Methods = EstadoPService.Methods || (EstadoPService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EstadoPService[x] = function (r, s, o) {
                    return Q.serviceRequest(EstadoPService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = EstadoPService.baseUrl + '/' + x;
            });
        })(EstadoPService = PETI.EstadoPService || (PETI.EstadoPService = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var IndicadorPetiForm = /** @class */ (function (_super) {
            __extends(IndicadorPetiForm, _super);
            function IndicadorPetiForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            IndicadorPetiForm.formKey = 'PETI.IndicadorPeti';
            return IndicadorPetiForm;
        }(Serenity.PrefixedContext));
        PETI.IndicadorPetiForm = IndicadorPetiForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(IndicadorPetiForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var IndicadorPetiRow;
        (function (IndicadorPetiRow) {
            IndicadorPetiRow.idProperty = 'Id';
            IndicadorPetiRow.nameProperty = 'Descripcion';
            IndicadorPetiRow.localTextPrefix = 'PETI.IndicadorPeti';
            var Fields;
            (function (Fields) {
            })(Fields = IndicadorPetiRow.Fields || (IndicadorPetiRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(IndicadorPetiRow = PETI.IndicadorPetiRow || (PETI.IndicadorPetiRow = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var IndicadorPetiService;
        (function (IndicadorPetiService) {
            IndicadorPetiService.baseUrl = 'PETI/IndicadorPeti';
            var Methods;
            (function (Methods) {
            })(Methods = IndicadorPetiService.Methods || (IndicadorPetiService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                IndicadorPetiService[x] = function (r, s, o) {
                    return Q.serviceRequest(IndicadorPetiService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = IndicadorPetiService.baseUrl + '/' + x;
            });
        })(IndicadorPetiService = PETI.IndicadorPetiService || (PETI.IndicadorPetiService = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var PetiForm = /** @class */ (function (_super) {
            __extends(PetiForm, _super);
            function PetiForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            PetiForm.formKey = 'PETI.Peti';
            return PetiForm;
        }(Serenity.PrefixedContext));
        PETI.PetiForm = PetiForm;
        [,
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Actividad', function () { return Serenity.StringEditor; }],
            ['FechaEntrega', function () { return Serenity.DateEditor; }],
            ['Avance', function () { return Serenity.IntegerEditor; }],
            ['Cumple', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(PetiForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var PetiRow;
        (function (PetiRow) {
            PetiRow.idProperty = 'Id';
            PetiRow.nameProperty = 'Actividad';
            PetiRow.localTextPrefix = 'PETI.Peti';
            var Fields;
            (function (Fields) {
            })(Fields = PetiRow.Fields || (PetiRow.Fields = {}));
            [
                'Id',
                'Analista',
                'Actividad',
                'FechaEntrega',
                'Avance',
                'Cumple',
                'AnalistaNombre',
                'AnalistaEstado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(PetiRow = PETI.PetiRow || (PETI.PetiRow = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var PetiService;
        (function (PetiService) {
            PetiService.baseUrl = 'PETI/Peti';
            var Methods;
            (function (Methods) {
            })(Methods = PetiService.Methods || (PetiService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                PetiService[x] = function (r, s, o) {
                    return Q.serviceRequest(PetiService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = PetiService.baseUrl + '/' + x;
            });
        })(PetiService = PETI.PetiService || (PETI.PetiService = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var PrioridadForm = /** @class */ (function (_super) {
            __extends(PrioridadForm, _super);
            function PrioridadForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            PrioridadForm.formKey = 'PETI.Prioridad';
            return PrioridadForm;
        }(Serenity.PrefixedContext));
        PETI.PrioridadForm = PrioridadForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(PrioridadForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var PrioridadRow;
        (function (PrioridadRow) {
            PrioridadRow.idProperty = 'Id';
            PrioridadRow.nameProperty = 'Descripcion';
            PrioridadRow.localTextPrefix = 'PETI.Prioridad';
            var Fields;
            (function (Fields) {
            })(Fields = PrioridadRow.Fields || (PrioridadRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(PrioridadRow = PETI.PrioridadRow || (PETI.PrioridadRow = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var PrioridadService;
        (function (PrioridadService) {
            PrioridadService.baseUrl = 'PETI/Prioridad';
            var Methods;
            (function (Methods) {
            })(Methods = PrioridadService.Methods || (PrioridadService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                PrioridadService[x] = function (r, s, o) {
                    return Q.serviceRequest(PrioridadService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = PrioridadService.baseUrl + '/' + x;
            });
        })(PrioridadService = PETI.PrioridadService || (PETI.PrioridadService = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var ProcesoPForm = /** @class */ (function (_super) {
            __extends(ProcesoPForm, _super);
            function ProcesoPForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ProcesoPForm.formKey = 'PETI.ProcesoP';
            return ProcesoPForm;
        }(Serenity.PrefixedContext));
        PETI.ProcesoPForm = ProcesoPForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Tipo', function () { return Serenity.IntegerEditor; }],
            ['Prioridad', function () { return Serenity.IntegerEditor; }],
            ['Estado', function () { return Serenity.IntegerEditor; }],
            ['Anio', function () { return Serenity.IntegerEditor; }],
            ['Semestre', function () { return Serenity.IntegerEditor; }],
            ['Actividad', function () { return Serenity.IntegerEditor; }],
            ['Indicador', function () { return Serenity.IntegerEditor; }],
            ['Personal', function () { return Serenity.IntegerEditor; }],
            ['Infraestructura', function () { return Serenity.StringEditor; }],
            ['Presupuesto', function () { return Serenity.DecimalEditor; }],
            ['Avance', function () { return Serenity.IntegerEditor; }],
            ['Observacion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(ProcesoPForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var ProcesoPRow;
        (function (ProcesoPRow) {
            ProcesoPRow.idProperty = 'Id';
            ProcesoPRow.nameProperty = 'Descripcion';
            ProcesoPRow.localTextPrefix = 'PETI.ProcesoP';
            var Fields;
            (function (Fields) {
            })(Fields = ProcesoPRow.Fields || (ProcesoPRow.Fields = {}));
            [
                'Id',
                'Descripcion',
                'Tipo',
                'Prioridad',
                'Estado',
                'Anio',
                'Semestre',
                'Actividad',
                'Indicador',
                'Personal',
                'Infraestructura',
                'Presupuesto',
                'Avance',
                'Observacion',
                'TipoDescripcion',
                'PrioridadDescripcion',
                'EstadoDescripcion',
                'AnioDescripcion',
                'SemestreDescripcion',
                'ActividadDescripcion',
                'IndicadorDescripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(ProcesoPRow = PETI.ProcesoPRow || (PETI.ProcesoPRow = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var ProcesoPService;
        (function (ProcesoPService) {
            ProcesoPService.baseUrl = 'PETI/ProcesoP';
            var Methods;
            (function (Methods) {
            })(Methods = ProcesoPService.Methods || (ProcesoPService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ProcesoPService[x] = function (r, s, o) {
                    return Q.serviceRequest(ProcesoPService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = ProcesoPService.baseUrl + '/' + x;
            });
        })(ProcesoPService = PETI.ProcesoPService || (PETI.ProcesoPService = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var SemestrePForm = /** @class */ (function (_super) {
            __extends(SemestrePForm, _super);
            function SemestrePForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            SemestrePForm.formKey = 'PETI.SemestreP';
            return SemestrePForm;
        }(Serenity.PrefixedContext));
        PETI.SemestrePForm = SemestrePForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(SemestrePForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var SemestrePRow;
        (function (SemestrePRow) {
            SemestrePRow.idProperty = 'Id';
            SemestrePRow.nameProperty = 'Descripcion';
            SemestrePRow.localTextPrefix = 'PETI.SemestreP';
            var Fields;
            (function (Fields) {
            })(Fields = SemestrePRow.Fields || (SemestrePRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(SemestrePRow = PETI.SemestrePRow || (PETI.SemestrePRow = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var SemestrePService;
        (function (SemestrePService) {
            SemestrePService.baseUrl = 'PETI/SemestreP';
            var Methods;
            (function (Methods) {
            })(Methods = SemestrePService.Methods || (SemestrePService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                SemestrePService[x] = function (r, s, o) {
                    return Q.serviceRequest(SemestrePService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = SemestrePService.baseUrl + '/' + x;
            });
        })(SemestrePService = PETI.SemestrePService || (PETI.SemestrePService = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var TipoPForm = /** @class */ (function (_super) {
            __extends(TipoPForm, _super);
            function TipoPForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TipoPForm.formKey = 'PETI.TipoP';
            return TipoPForm;
        }(Serenity.PrefixedContext));
        PETI.TipoPForm = TipoPForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TipoPForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var TipoPRow;
        (function (TipoPRow) {
            TipoPRow.idProperty = 'Id';
            TipoPRow.nameProperty = 'Descripcion';
            TipoPRow.localTextPrefix = 'PETI.TipoP';
            var Fields;
            (function (Fields) {
            })(Fields = TipoPRow.Fields || (TipoPRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TipoPRow = PETI.TipoPRow || (PETI.TipoPRow = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PETI;
    (function (PETI) {
        var TipoPService;
        (function (TipoPService) {
            TipoPService.baseUrl = 'PETI/TipoP';
            var Methods;
            (function (Methods) {
            })(Methods = TipoPService.Methods || (TipoPService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TipoPService[x] = function (r, s, o) {
                    return Q.serviceRequest(TipoPService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TipoPService.baseUrl + '/' + x;
            });
        })(TipoPService = PETI.TipoPService || (PETI.TipoPService = {}));
    })(PETI = CntAppSeguridad.PETI || (CntAppSeguridad.PETI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PlanAnualSI;
    (function (PlanAnualSI) {
        var PlanAnualForm = /** @class */ (function (_super) {
            __extends(PlanAnualForm, _super);
            function PlanAnualForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            PlanAnualForm.formKey = 'PlanAnualSI.PlanAnual';
            return PlanAnualForm;
        }(Serenity.PrefixedContext));
        PlanAnualSI.PlanAnualForm = PlanAnualForm;
        [,
            ['Numero', function () { return Serenity.StringEditor; }],
            ['Analista', function () { return Serenity.StringEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Fecha', function () { return Serenity.DateEditor; }],
            ['Informe', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(PlanAnualForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(PlanAnualSI = CntAppSeguridad.PlanAnualSI || (CntAppSeguridad.PlanAnualSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PlanAnualSI;
    (function (PlanAnualSI) {
        var PlanAnualRow;
        (function (PlanAnualRow) {
            PlanAnualRow.idProperty = 'Id';
            PlanAnualRow.nameProperty = 'Numero';
            PlanAnualRow.localTextPrefix = 'PlanAnualSI.PlanAnual';
            var Fields;
            (function (Fields) {
            })(Fields = PlanAnualRow.Fields || (PlanAnualRow.Fields = {}));
            [
                'Id',
                'Numero',
                'Analista',
                'Descripcion',
                'Fecha',
                'Informe'
            ].forEach(function (x) { return Fields[x] = x; });
        })(PlanAnualRow = PlanAnualSI.PlanAnualRow || (PlanAnualSI.PlanAnualRow = {}));
    })(PlanAnualSI = CntAppSeguridad.PlanAnualSI || (CntAppSeguridad.PlanAnualSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PlanAnualSI;
    (function (PlanAnualSI) {
        var PlanAnualService;
        (function (PlanAnualService) {
            PlanAnualService.baseUrl = 'PlanAnualSI/PlanAnual';
            var Methods;
            (function (Methods) {
            })(Methods = PlanAnualService.Methods || (PlanAnualService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                PlanAnualService[x] = function (r, s, o) {
                    return Q.serviceRequest(PlanAnualService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = PlanAnualService.baseUrl + '/' + x;
            });
        })(PlanAnualService = PlanAnualSI.PlanAnualService || (PlanAnualSI.PlanAnualService = {}));
    })(PlanAnualSI = CntAppSeguridad.PlanAnualSI || (CntAppSeguridad.PlanAnualSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var EstadoForm = /** @class */ (function (_super) {
            __extends(EstadoForm, _super);
            function EstadoForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            EstadoForm.formKey = 'Requerimientos.Estado';
            return EstadoForm;
        }(Serenity.PrefixedContext));
        Requerimientos.EstadoForm = EstadoForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(EstadoForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var EstadoRow;
        (function (EstadoRow) {
            EstadoRow.idProperty = 'Id';
            EstadoRow.nameProperty = 'Descripcion';
            EstadoRow.localTextPrefix = 'Requerimientos.Estado';
            var Fields;
            (function (Fields) {
            })(Fields = EstadoRow.Fields || (EstadoRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(EstadoRow = Requerimientos.EstadoRow || (Requerimientos.EstadoRow = {}));
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var EstadoService;
        (function (EstadoService) {
            EstadoService.baseUrl = 'Requerimientos/Estado';
            var Methods;
            (function (Methods) {
            })(Methods = EstadoService.Methods || (EstadoService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EstadoService[x] = function (r, s, o) {
                    return Q.serviceRequest(EstadoService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = EstadoService.baseUrl + '/' + x;
            });
        })(EstadoService = Requerimientos.EstadoService || (Requerimientos.EstadoService = {}));
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var IncidenteForm = /** @class */ (function (_super) {
            __extends(IncidenteForm, _super);
            function IncidenteForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            IncidenteForm.formKey = 'Requerimientos.Incidente';
            return IncidenteForm;
        }(Serenity.PrefixedContext));
        Requerimientos.IncidenteForm = IncidenteForm;
        [,
            ['Numero', function () { return Serenity.StringEditor; }],
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Prioridad', function () { return Serenity.IntegerEditor; }],
            ['Resumen', function () { return Serenity.StringEditor; }],
            ['Estado', function () { return Serenity.IntegerEditor; }],
            ['Observacion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(IncidenteForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var IncidenteRow;
        (function (IncidenteRow) {
            IncidenteRow.idProperty = 'Id';
            IncidenteRow.nameProperty = 'Numero';
            IncidenteRow.localTextPrefix = 'Requerimientos.Incidente';
            var Fields;
            (function (Fields) {
            })(Fields = IncidenteRow.Fields || (IncidenteRow.Fields = {}));
            [
                'Id',
                'Numero',
                'Analista',
                'Prioridad',
                'Resumen',
                'Estado',
                'Observacion',
                'AnalistaNombre',
                'AnalistaEstado',
                'PrioridadDescripcion',
                'EstadoDescripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(IncidenteRow = Requerimientos.IncidenteRow || (Requerimientos.IncidenteRow = {}));
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var IncidenteService;
        (function (IncidenteService) {
            IncidenteService.baseUrl = 'Requerimientos/Incidente';
            var Methods;
            (function (Methods) {
            })(Methods = IncidenteService.Methods || (IncidenteService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                IncidenteService[x] = function (r, s, o) {
                    return Q.serviceRequest(IncidenteService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = IncidenteService.baseUrl + '/' + x;
            });
        })(IncidenteService = Requerimientos.IncidenteService || (Requerimientos.IncidenteService = {}));
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var PrioridadForm = /** @class */ (function (_super) {
            __extends(PrioridadForm, _super);
            function PrioridadForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            PrioridadForm.formKey = 'Requerimientos.Prioridad';
            return PrioridadForm;
        }(Serenity.PrefixedContext));
        Requerimientos.PrioridadForm = PrioridadForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(PrioridadForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var PrioridadRow;
        (function (PrioridadRow) {
            PrioridadRow.idProperty = 'Id';
            PrioridadRow.nameProperty = 'Descripcion';
            PrioridadRow.localTextPrefix = 'Requerimientos.Prioridad';
            var Fields;
            (function (Fields) {
            })(Fields = PrioridadRow.Fields || (PrioridadRow.Fields = {}));
            [
                'Id',
                'Descripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(PrioridadRow = Requerimientos.PrioridadRow || (Requerimientos.PrioridadRow = {}));
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var PrioridadService;
        (function (PrioridadService) {
            PrioridadService.baseUrl = 'Requerimientos/Prioridad';
            var Methods;
            (function (Methods) {
            })(Methods = PrioridadService.Methods || (PrioridadService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                PrioridadService[x] = function (r, s, o) {
                    return Q.serviceRequest(PrioridadService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = PrioridadService.baseUrl + '/' + x;
            });
        })(PrioridadService = Requerimientos.PrioridadService || (Requerimientos.PrioridadService = {}));
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var TicketForm = /** @class */ (function (_super) {
            __extends(TicketForm, _super);
            function TicketForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TicketForm.formKey = 'Requerimientos.Ticket';
            return TicketForm;
        }(Serenity.PrefixedContext));
        Requerimientos.TicketForm = TicketForm;
        [,
            ['Numero', function () { return Serenity.StringEditor; }],
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Status', function () { return Serenity.IntegerEditor; }],
            ['Requirente', function () { return Serenity.StringEditor; }],
            ['Resumen', function () { return Serenity.StringEditor; }],
            ['Observacion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TicketForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var TicketRow;
        (function (TicketRow) {
            TicketRow.idProperty = 'Id';
            TicketRow.nameProperty = 'Numero';
            TicketRow.localTextPrefix = 'Requerimientos.Ticket';
            var Fields;
            (function (Fields) {
            })(Fields = TicketRow.Fields || (TicketRow.Fields = {}));
            [
                'Id',
                'Numero',
                'Analista',
                'Status',
                'Requirente',
                'Resumen',
                'Observacion',
                'AnalistaNombre',
                'AnalistaEstado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TicketRow = Requerimientos.TicketRow || (Requerimientos.TicketRow = {}));
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var TicketService;
        (function (TicketService) {
            TicketService.baseUrl = 'Requerimientos/Ticket';
            var Methods;
            (function (Methods) {
            })(Methods = TicketService.Methods || (TicketService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TicketService[x] = function (r, s, o) {
                    return Q.serviceRequest(TicketService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TicketService.baseUrl + '/' + x;
            });
        })(TicketService = Requerimientos.TicketService || (Requerimientos.TicketService = {}));
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Riesgos;
    (function (Riesgos) {
        var LogsForm = /** @class */ (function (_super) {
            __extends(LogsForm, _super);
            function LogsForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            LogsForm.formKey = 'Riesgos.Logs';
            return LogsForm;
        }(Serenity.PrefixedContext));
        Riesgos.LogsForm = LogsForm;
        [,
            ['Activo', function () { return Serenity.StringEditor; }],
            ['Avance', function () { return Serenity.IntegerEditor; }],
            ['FechaFin', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(LogsForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Riesgos = CntAppSeguridad.Riesgos || (CntAppSeguridad.Riesgos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Riesgos;
    (function (Riesgos) {
        var LogsRow;
        (function (LogsRow) {
            LogsRow.idProperty = 'Id';
            LogsRow.nameProperty = 'Activo';
            LogsRow.localTextPrefix = 'Riesgos.Logs';
            var Fields;
            (function (Fields) {
            })(Fields = LogsRow.Fields || (LogsRow.Fields = {}));
            [
                'Id',
                'Activo',
                'Avance',
                'FechaFin'
            ].forEach(function (x) { return Fields[x] = x; });
        })(LogsRow = Riesgos.LogsRow || (Riesgos.LogsRow = {}));
    })(Riesgos = CntAppSeguridad.Riesgos || (CntAppSeguridad.Riesgos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Riesgos;
    (function (Riesgos) {
        var LogsService;
        (function (LogsService) {
            LogsService.baseUrl = 'Riesgos/Logs';
            var Methods;
            (function (Methods) {
            })(Methods = LogsService.Methods || (LogsService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                LogsService[x] = function (r, s, o) {
                    return Q.serviceRequest(LogsService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = LogsService.baseUrl + '/' + x;
            });
        })(LogsService = Riesgos.LogsService || (Riesgos.LogsService = {}));
    })(Riesgos = CntAppSeguridad.Riesgos || (CntAppSeguridad.Riesgos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Riesgos;
    (function (Riesgos) {
        var PerfilesForm = /** @class */ (function (_super) {
            __extends(PerfilesForm, _super);
            function PerfilesForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            PerfilesForm.formKey = 'Riesgos.Perfiles';
            return PerfilesForm;
        }(Serenity.PrefixedContext));
        Riesgos.PerfilesForm = PerfilesForm;
        [,
            ['Activo', function () { return Serenity.StringEditor; }],
            ['Avance', function () { return Serenity.IntegerEditor; }],
            ['FechaFin', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(PerfilesForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Riesgos = CntAppSeguridad.Riesgos || (CntAppSeguridad.Riesgos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Riesgos;
    (function (Riesgos) {
        var PerfilesRow;
        (function (PerfilesRow) {
            PerfilesRow.idProperty = 'Id';
            PerfilesRow.nameProperty = 'Activo';
            PerfilesRow.localTextPrefix = 'Riesgos.Perfiles';
            var Fields;
            (function (Fields) {
            })(Fields = PerfilesRow.Fields || (PerfilesRow.Fields = {}));
            [
                'Id',
                'Activo',
                'Avance',
                'FechaFin'
            ].forEach(function (x) { return Fields[x] = x; });
        })(PerfilesRow = Riesgos.PerfilesRow || (Riesgos.PerfilesRow = {}));
    })(Riesgos = CntAppSeguridad.Riesgos || (CntAppSeguridad.Riesgos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Riesgos;
    (function (Riesgos) {
        var PerfilesService;
        (function (PerfilesService) {
            PerfilesService.baseUrl = 'Riesgos/Perfiles';
            var Methods;
            (function (Methods) {
            })(Methods = PerfilesService.Methods || (PerfilesService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                PerfilesService[x] = function (r, s, o) {
                    return Q.serviceRequest(PerfilesService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = PerfilesService.baseUrl + '/' + x;
            });
        })(PerfilesService = Riesgos.PerfilesService || (Riesgos.PerfilesService = {}));
    })(Riesgos = CntAppSeguridad.Riesgos || (CntAppSeguridad.Riesgos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var ActividadesForm = /** @class */ (function (_super) {
            __extends(ActividadesForm, _super);
            function ActividadesForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            ActividadesForm.formKey = 'SeguridadInformatica.Actividades';
            return ActividadesForm;
        }(Serenity.PrefixedContext));
        SeguridadInformatica.ActividadesForm = ActividadesForm;
        [,
            ['Egsi', function () { return Serenity.IntegerEditor; }],
            ['Indicador', function () { return Serenity.IntegerEditor; }]
        ].forEach(function (x) { return Object.defineProperty(ActividadesForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var ActividadesRow;
        (function (ActividadesRow) {
            ActividadesRow.idProperty = 'Id';
            ActividadesRow.localTextPrefix = 'SeguridadInformatica.Actividades';
            var Fields;
            (function (Fields) {
            })(Fields = ActividadesRow.Fields || (ActividadesRow.Fields = {}));
            [
                'Id',
                'Egsi',
                'Indicador',
                'IndicadorEgsi',
                'IndicadorDescripcion',
                'IndicadorPorcentaje',
                'IndicadorAnio',
                'IndicadorMes'
            ].forEach(function (x) { return Fields[x] = x; });
        })(ActividadesRow = SeguridadInformatica.ActividadesRow || (SeguridadInformatica.ActividadesRow = {}));
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var ActividadesService;
        (function (ActividadesService) {
            ActividadesService.baseUrl = 'SeguridadInformatica/Actividades';
            var Methods;
            (function (Methods) {
            })(Methods = ActividadesService.Methods || (ActividadesService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                ActividadesService[x] = function (r, s, o) {
                    return Q.serviceRequest(ActividadesService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = ActividadesService.baseUrl + '/' + x;
            });
        })(ActividadesService = SeguridadInformatica.ActividadesService || (SeguridadInformatica.ActividadesService = {}));
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var AreaForm = /** @class */ (function (_super) {
            __extends(AreaForm, _super);
            function AreaForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AreaForm.formKey = 'SeguridadInformatica.Area';
            return AreaForm;
        }(Serenity.PrefixedContext));
        SeguridadInformatica.AreaForm = AreaForm;
        [,
            ['Nombre', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(AreaForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var AreaRow;
        (function (AreaRow) {
            AreaRow.idProperty = 'Id';
            AreaRow.nameProperty = 'Nombre';
            AreaRow.localTextPrefix = 'SeguridadInformatica.Area';
            var Fields;
            (function (Fields) {
            })(Fields = AreaRow.Fields || (AreaRow.Fields = {}));
            [
                'Id',
                'Nombre'
            ].forEach(function (x) { return Fields[x] = x; });
        })(AreaRow = SeguridadInformatica.AreaRow || (SeguridadInformatica.AreaRow = {}));
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var AreaService;
        (function (AreaService) {
            AreaService.baseUrl = 'SeguridadInformatica/Area';
            var Methods;
            (function (Methods) {
            })(Methods = AreaService.Methods || (AreaService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                AreaService[x] = function (r, s, o) {
                    return Q.serviceRequest(AreaService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = AreaService.baseUrl + '/' + x;
            });
        })(AreaService = SeguridadInformatica.AreaService || (SeguridadInformatica.AreaService = {}));
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var EgsiForm = /** @class */ (function (_super) {
            __extends(EgsiForm, _super);
            function EgsiForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            EgsiForm.formKey = 'SeguridadInformatica.Egsi';
            return EgsiForm;
        }(Serenity.PrefixedContext));
        SeguridadInformatica.EgsiForm = EgsiForm;
        [,
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Area', function () { return Serenity.IntegerEditor; }]
        ].forEach(function (x) { return Object.defineProperty(EgsiForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var EgsiRow;
        (function (EgsiRow) {
            EgsiRow.idProperty = 'Id';
            EgsiRow.nameProperty = 'Descripcion';
            EgsiRow.localTextPrefix = 'SeguridadInformatica.Egsi';
            var Fields;
            (function (Fields) {
            })(Fields = EgsiRow.Fields || (EgsiRow.Fields = {}));
            [
                'Id',
                'Descripcion',
                'Area',
                'AreaNombre'
            ].forEach(function (x) { return Fields[x] = x; });
        })(EgsiRow = SeguridadInformatica.EgsiRow || (SeguridadInformatica.EgsiRow = {}));
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var EgsiService;
        (function (EgsiService) {
            EgsiService.baseUrl = 'SeguridadInformatica/Egsi';
            var Methods;
            (function (Methods) {
            })(Methods = EgsiService.Methods || (EgsiService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                EgsiService[x] = function (r, s, o) {
                    return Q.serviceRequest(EgsiService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = EgsiService.baseUrl + '/' + x;
            });
        })(EgsiService = SeguridadInformatica.EgsiService || (SeguridadInformatica.EgsiService = {}));
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var IndicadorForm = /** @class */ (function (_super) {
            __extends(IndicadorForm, _super);
            function IndicadorForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            IndicadorForm.formKey = 'SeguridadInformatica.Indicador';
            return IndicadorForm;
        }(Serenity.PrefixedContext));
        SeguridadInformatica.IndicadorForm = IndicadorForm;
        [,
            ['Egsi', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Porcentaje', function () { return Serenity.IntegerEditor; }],
            ['Anio', function () { return Serenity.StringEditor; }],
            ['Mes', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(IndicadorForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var IndicadorRow;
        (function (IndicadorRow) {
            IndicadorRow.idProperty = 'Id';
            IndicadorRow.nameProperty = 'Descripcion';
            IndicadorRow.localTextPrefix = 'SeguridadInformatica.Indicador';
            var Fields;
            (function (Fields) {
            })(Fields = IndicadorRow.Fields || (IndicadorRow.Fields = {}));
            [
                'Id',
                'Egsi',
                'Descripcion',
                'Porcentaje',
                'Anio',
                'Mes',
                'EgsiDescripcion',
                'EgsiArea'
            ].forEach(function (x) { return Fields[x] = x; });
        })(IndicadorRow = SeguridadInformatica.IndicadorRow || (SeguridadInformatica.IndicadorRow = {}));
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var SeguridadInformatica;
    (function (SeguridadInformatica) {
        var IndicadorService;
        (function (IndicadorService) {
            IndicadorService.baseUrl = 'SeguridadInformatica/Indicador';
            var Methods;
            (function (Methods) {
            })(Methods = IndicadorService.Methods || (IndicadorService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                IndicadorService[x] = function (r, s, o) {
                    return Q.serviceRequest(IndicadorService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = IndicadorService.baseUrl + '/' + x;
            });
        })(IndicadorService = SeguridadInformatica.IndicadorService || (SeguridadInformatica.IndicadorService = {}));
    })(SeguridadInformatica = CntAppSeguridad.SeguridadInformatica || (CntAppSeguridad.SeguridadInformatica = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Tareas;
    (function (Tareas) {
        var AnalistaForm = /** @class */ (function (_super) {
            __extends(AnalistaForm, _super);
            function AnalistaForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            AnalistaForm.formKey = 'Tareas.Analista';
            return AnalistaForm;
        }(Serenity.PrefixedContext));
        Tareas.AnalistaForm = AnalistaForm;
        [,
            ['Nombre', function () { return Serenity.StringEditor; }],
            ['Estado', function () { return Serenity.IntegerEditor; }]
        ].forEach(function (x) { return Object.defineProperty(AnalistaForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Tareas = CntAppSeguridad.Tareas || (CntAppSeguridad.Tareas = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Tareas;
    (function (Tareas) {
        var AnalistaRow;
        (function (AnalistaRow) {
            AnalistaRow.idProperty = 'Id';
            AnalistaRow.nameProperty = 'Nombre';
            AnalistaRow.localTextPrefix = 'Tareas.Analista';
            var Fields;
            (function (Fields) {
            })(Fields = AnalistaRow.Fields || (AnalistaRow.Fields = {}));
            [
                'Id',
                'Nombre',
                'Estado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(AnalistaRow = Tareas.AnalistaRow || (Tareas.AnalistaRow = {}));
    })(Tareas = CntAppSeguridad.Tareas || (CntAppSeguridad.Tareas = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Tareas;
    (function (Tareas) {
        var AnalistaService;
        (function (AnalistaService) {
            AnalistaService.baseUrl = 'Tareas/Analista';
            var Methods;
            (function (Methods) {
            })(Methods = AnalistaService.Methods || (AnalistaService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                AnalistaService[x] = function (r, s, o) {
                    return Q.serviceRequest(AnalistaService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = AnalistaService.baseUrl + '/' + x;
            });
        })(AnalistaService = Tareas.AnalistaService || (Tareas.AnalistaService = {}));
    })(Tareas = CntAppSeguridad.Tareas || (CntAppSeguridad.Tareas = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Tareas;
    (function (Tareas) {
        var IndicadorTForm = /** @class */ (function (_super) {
            __extends(IndicadorTForm, _super);
            function IndicadorTForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            IndicadorTForm.formKey = 'Tareas.IndicadorT';
            return IndicadorTForm;
        }(Serenity.PrefixedContext));
        Tareas.IndicadorTForm = IndicadorTForm;
        [,
            ['TareaSeguridad', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Porcentaje', function () { return Serenity.StringEditor; }],
            ['Anio', function () { return Serenity.StringEditor; }],
            ['Mes', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(IndicadorTForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Tareas = CntAppSeguridad.Tareas || (CntAppSeguridad.Tareas = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Tareas;
    (function (Tareas) {
        var IndicadorTRow;
        (function (IndicadorTRow) {
            IndicadorTRow.idProperty = 'Id';
            IndicadorTRow.nameProperty = 'Descripcion';
            IndicadorTRow.localTextPrefix = 'Tareas.IndicadorT';
            var Fields;
            (function (Fields) {
            })(Fields = IndicadorTRow.Fields || (IndicadorTRow.Fields = {}));
            [
                'Id',
                'TareaSeguridad',
                'Descripcion',
                'Porcentaje',
                'Anio',
                'Mes',
                'TareaSeguridadAnalista',
                'TareaSeguridadDescripcion',
                'TareaSeguridadEstado',
                'TareaSeguridadObservacion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(IndicadorTRow = Tareas.IndicadorTRow || (Tareas.IndicadorTRow = {}));
    })(Tareas = CntAppSeguridad.Tareas || (CntAppSeguridad.Tareas = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Tareas;
    (function (Tareas) {
        var IndicadorTService;
        (function (IndicadorTService) {
            IndicadorTService.baseUrl = 'Tareas/IndicadorT';
            var Methods;
            (function (Methods) {
            })(Methods = IndicadorTService.Methods || (IndicadorTService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                IndicadorTService[x] = function (r, s, o) {
                    return Q.serviceRequest(IndicadorTService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = IndicadorTService.baseUrl + '/' + x;
            });
        })(IndicadorTService = Tareas.IndicadorTService || (Tareas.IndicadorTService = {}));
    })(Tareas = CntAppSeguridad.Tareas || (CntAppSeguridad.Tareas = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Tareas;
    (function (Tareas) {
        var TareasForm = /** @class */ (function (_super) {
            __extends(TareasForm, _super);
            function TareasForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TareasForm.formKey = 'Tareas.Tareas';
            return TareasForm;
        }(Serenity.PrefixedContext));
        Tareas.TareasForm = TareasForm;
        [,
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Estado', function () { return Serenity.IntegerEditor; }],
            ['Observacion', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TareasForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(Tareas = CntAppSeguridad.Tareas || (CntAppSeguridad.Tareas = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Tareas;
    (function (Tareas) {
        var TareasRow;
        (function (TareasRow) {
            TareasRow.idProperty = 'Id';
            TareasRow.nameProperty = 'Descripcion';
            TareasRow.localTextPrefix = 'Tareas.Tareas';
            var Fields;
            (function (Fields) {
            })(Fields = TareasRow.Fields || (TareasRow.Fields = {}));
            [
                'Id',
                'Analista',
                'Descripcion',
                'Estado',
                'Observacion',
                'AnalistaNombre',
                'AnalistaEstado'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TareasRow = Tareas.TareasRow || (Tareas.TareasRow = {}));
    })(Tareas = CntAppSeguridad.Tareas || (CntAppSeguridad.Tareas = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Tareas;
    (function (Tareas) {
        var TareasService;
        (function (TareasService) {
            TareasService.baseUrl = 'Tareas/Tareas';
            var Methods;
            (function (Methods) {
            })(Methods = TareasService.Methods || (TareasService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TareasService[x] = function (r, s, o) {
                    return Q.serviceRequest(TareasService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TareasService.baseUrl + '/' + x;
            });
        })(TareasService = Tareas.TareasService || (Tareas.TareasService = {}));
    })(Tareas = CntAppSeguridad.Tareas || (CntAppSeguridad.Tareas = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var TareasSI;
    (function (TareasSI) {
        var IndicadorTForm = /** @class */ (function (_super) {
            __extends(IndicadorTForm, _super);
            function IndicadorTForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            IndicadorTForm.formKey = 'TareasSI.IndicadorT';
            return IndicadorTForm;
        }(Serenity.PrefixedContext));
        TareasSI.IndicadorTForm = IndicadorTForm;
        [,
            ['TareaSeguridad', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['Porcentaje', function () { return Serenity.StringEditor; }],
            ['Anio', function () { return Serenity.StringEditor; }],
            ['Mes', function () { return Serenity.StringEditor; }]
        ].forEach(function (x) { return Object.defineProperty(IndicadorTForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(TareasSI = CntAppSeguridad.TareasSI || (CntAppSeguridad.TareasSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var TareasSI;
    (function (TareasSI) {
        var IndicadorTRow;
        (function (IndicadorTRow) {
            IndicadorTRow.idProperty = 'Id';
            IndicadorTRow.nameProperty = 'Descripcion';
            IndicadorTRow.localTextPrefix = 'TareasSI.IndicadorT';
            var Fields;
            (function (Fields) {
            })(Fields = IndicadorTRow.Fields || (IndicadorTRow.Fields = {}));
            [
                'Id',
                'TareaSeguridad',
                'Descripcion',
                'Porcentaje',
                'Anio',
                'Mes',
                'TareaSeguridadAnalista',
                'TareaSeguridadDescripcion',
                'TareaSeguridadFechaAsignacion',
                'TareaSeguridadEstado',
                'TareaSeguridadObservacion',
                'TareaSeguridadFechaEntrega'
            ].forEach(function (x) { return Fields[x] = x; });
        })(IndicadorTRow = TareasSI.IndicadorTRow || (TareasSI.IndicadorTRow = {}));
    })(TareasSI = CntAppSeguridad.TareasSI || (CntAppSeguridad.TareasSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var TareasSI;
    (function (TareasSI) {
        var IndicadorTService;
        (function (IndicadorTService) {
            IndicadorTService.baseUrl = 'TareasSI/IndicadorT';
            var Methods;
            (function (Methods) {
            })(Methods = IndicadorTService.Methods || (IndicadorTService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                IndicadorTService[x] = function (r, s, o) {
                    return Q.serviceRequest(IndicadorTService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = IndicadorTService.baseUrl + '/' + x;
            });
        })(IndicadorTService = TareasSI.IndicadorTService || (TareasSI.IndicadorTService = {}));
    })(TareasSI = CntAppSeguridad.TareasSI || (CntAppSeguridad.TareasSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var TareasSI;
    (function (TareasSI) {
        var TareasForm = /** @class */ (function (_super) {
            __extends(TareasForm, _super);
            function TareasForm() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            TareasForm.formKey = 'TareasSI.Tareas';
            return TareasForm;
        }(Serenity.PrefixedContext));
        TareasSI.TareasForm = TareasForm;
        [,
            ['Analista', function () { return Serenity.IntegerEditor; }],
            ['Descripcion', function () { return Serenity.StringEditor; }],
            ['FechaAsignacion', function () { return Serenity.DateEditor; }],
            ['Estado', function () { return Serenity.IntegerEditor; }],
            ['Observacion', function () { return Serenity.StringEditor; }],
            ['FechaEntrega', function () { return Serenity.DateEditor; }]
        ].forEach(function (x) { return Object.defineProperty(TareasForm.prototype, x[0], {
            get: function () {
                return this.w(x[0], x[1]());
            },
            enumerable: true,
            configurable: true
        }); });
    })(TareasSI = CntAppSeguridad.TareasSI || (CntAppSeguridad.TareasSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var TareasSI;
    (function (TareasSI) {
        var TareasRow;
        (function (TareasRow) {
            TareasRow.idProperty = 'Id';
            TareasRow.nameProperty = 'Descripcion';
            TareasRow.localTextPrefix = 'TareasSI.Tareas';
            var Fields;
            (function (Fields) {
            })(Fields = TareasRow.Fields || (TareasRow.Fields = {}));
            [
                'Id',
                'Analista',
                'Descripcion',
                'FechaAsignacion',
                'Estado',
                'Observacion',
                'FechaEntrega',
                'AnalistaNombre',
                'AnalistaEstado',
                'EstadoDescripcion'
            ].forEach(function (x) { return Fields[x] = x; });
        })(TareasRow = TareasSI.TareasRow || (TareasSI.TareasRow = {}));
    })(TareasSI = CntAppSeguridad.TareasSI || (CntAppSeguridad.TareasSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var TareasSI;
    (function (TareasSI) {
        var TareasService;
        (function (TareasService) {
            TareasService.baseUrl = 'TareasSI/Tareas';
            var Methods;
            (function (Methods) {
            })(Methods = TareasService.Methods || (TareasService.Methods = {}));
            [
                'Create',
                'Update',
                'Delete',
                'Retrieve',
                'List'
            ].forEach(function (x) {
                TareasService[x] = function (r, s, o) {
                    return Q.serviceRequest(TareasService.baseUrl + '/' + x, r, s, o);
                };
                Methods[x] = TareasService.baseUrl + '/' + x;
            });
        })(TareasService = TareasSI.TareasService || (TareasSI.TareasService = {}));
    })(TareasSI = CntAppSeguridad.TareasSI || (CntAppSeguridad.TareasSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var LanguageList;
    (function (LanguageList) {
        function getValue() {
            var result = [];
            for (var _i = 0, _a = CntAppSeguridad.Administration.LanguageRow.getLookup().items; _i < _a.length; _i++) {
                var k = _a[_i];
                if (k.LanguageId !== 'en') {
                    result.push([k.Id.toString(), k.LanguageName]);
                }
            }
            return result;
        }
        LanguageList.getValue = getValue;
    })(LanguageList = CntAppSeguridad.LanguageList || (CntAppSeguridad.LanguageList = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
/// <reference path="../Common/Helpers/LanguageList.ts" />
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var ScriptInitialization;
    (function (ScriptInitialization) {
        Q.Config.responsiveDialogs = true;
        Q.Config.rootNamespaces.push('CntAppSeguridad');
        Serenity.EntityDialog.defaultLanguageList = CntAppSeguridad.LanguageList.getValue;
    })(ScriptInitialization = CntAppSeguridad.ScriptInitialization || (CntAppSeguridad.ScriptInitialization = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var LanguageDialog = /** @class */ (function (_super) {
            __extends(LanguageDialog, _super);
            function LanguageDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Administration.LanguageForm(_this.idPrefix);
                return _this;
            }
            LanguageDialog.prototype.getFormKey = function () { return Administration.LanguageForm.formKey; };
            LanguageDialog.prototype.getIdProperty = function () { return Administration.LanguageRow.idProperty; };
            LanguageDialog.prototype.getLocalTextPrefix = function () { return Administration.LanguageRow.localTextPrefix; };
            LanguageDialog.prototype.getNameProperty = function () { return Administration.LanguageRow.nameProperty; };
            LanguageDialog.prototype.getService = function () { return Administration.LanguageService.baseUrl; };
            LanguageDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], LanguageDialog);
            return LanguageDialog;
        }(Serenity.EntityDialog));
        Administration.LanguageDialog = LanguageDialog;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var LanguageGrid = /** @class */ (function (_super) {
            __extends(LanguageGrid, _super);
            function LanguageGrid(container) {
                return _super.call(this, container) || this;
            }
            LanguageGrid.prototype.getColumnsKey = function () { return "Administration.Language"; };
            LanguageGrid.prototype.getDialogType = function () { return Administration.LanguageDialog; };
            LanguageGrid.prototype.getIdProperty = function () { return Administration.LanguageRow.idProperty; };
            LanguageGrid.prototype.getLocalTextPrefix = function () { return Administration.LanguageRow.localTextPrefix; };
            LanguageGrid.prototype.getService = function () { return Administration.LanguageService.baseUrl; };
            LanguageGrid.prototype.getDefaultSortBy = function () {
                return [Administration.LanguageRow.Fields.LanguageName];
            };
            LanguageGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], LanguageGrid);
            return LanguageGrid;
        }(Serenity.EntityGrid));
        Administration.LanguageGrid = LanguageGrid;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var RoleDialog = /** @class */ (function (_super) {
            __extends(RoleDialog, _super);
            function RoleDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Administration.RoleForm(_this.idPrefix);
                return _this;
            }
            RoleDialog.prototype.getFormKey = function () { return Administration.RoleForm.formKey; };
            RoleDialog.prototype.getIdProperty = function () { return Administration.RoleRow.idProperty; };
            RoleDialog.prototype.getLocalTextPrefix = function () { return Administration.RoleRow.localTextPrefix; };
            RoleDialog.prototype.getNameProperty = function () { return Administration.RoleRow.nameProperty; };
            RoleDialog.prototype.getService = function () { return Administration.RoleService.baseUrl; };
            RoleDialog.prototype.getToolbarButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.push({
                    title: Q.text('Site.RolePermissionDialog.EditButton'),
                    cssClass: 'edit-permissions-button',
                    icon: 'icon-lock-open text-green',
                    onClick: function () {
                        new Administration.RolePermissionDialog({
                            roleID: _this.entity.RoleId,
                            title: _this.entity.RoleName
                        }).dialogOpen();
                    }
                });
                return buttons;
            };
            RoleDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                this.toolbar.findButton("edit-permissions-button").toggleClass("disabled", this.isNewOrDeleted());
            };
            RoleDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], RoleDialog);
            return RoleDialog;
        }(Serenity.EntityDialog));
        Administration.RoleDialog = RoleDialog;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var RoleGrid = /** @class */ (function (_super) {
            __extends(RoleGrid, _super);
            function RoleGrid(container) {
                return _super.call(this, container) || this;
            }
            RoleGrid.prototype.getColumnsKey = function () { return "Administration.Role"; };
            RoleGrid.prototype.getDialogType = function () { return Administration.RoleDialog; };
            RoleGrid.prototype.getIdProperty = function () { return Administration.RoleRow.idProperty; };
            RoleGrid.prototype.getLocalTextPrefix = function () { return Administration.RoleRow.localTextPrefix; };
            RoleGrid.prototype.getService = function () { return Administration.RoleService.baseUrl; };
            RoleGrid.prototype.getDefaultSortBy = function () {
                return [Administration.RoleRow.Fields.RoleName];
            };
            RoleGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], RoleGrid);
            return RoleGrid;
        }(Serenity.EntityGrid));
        Administration.RoleGrid = RoleGrid;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var RolePermissionDialog = /** @class */ (function (_super) {
            __extends(RolePermissionDialog, _super);
            function RolePermissionDialog(opt) {
                var _this = _super.call(this, opt) || this;
                _this.permissions = new Administration.PermissionCheckEditor(_this.byId('Permissions'), {
                    showRevoke: false
                });
                Administration.RolePermissionService.List({
                    RoleID: _this.options.roleID,
                    Module: null,
                    Submodule: null
                }, function (response) {
                    _this.permissions.value = response.Entities.map(function (x) { return ({ PermissionKey: x }); });
                });
                _this.permissions.implicitPermissions = Q.getRemoteData('Administration.ImplicitPermissions');
                return _this;
            }
            RolePermissionDialog.prototype.getDialogOptions = function () {
                var _this = this;
                var opt = _super.prototype.getDialogOptions.call(this);
                opt.buttons = [
                    {
                        text: Q.text('Dialogs.OkButton'),
                        click: function (e) {
                            Administration.RolePermissionService.Update({
                                RoleID: _this.options.roleID,
                                Permissions: _this.permissions.value.map(function (x) { return x.PermissionKey; }),
                                Module: null,
                                Submodule: null
                            }, function (response) {
                                _this.dialogClose();
                                window.setTimeout(function () { return Q.notifySuccess(Q.text('Site.RolePermissionDialog.SaveSuccess')); }, 0);
                            });
                        }
                    }, {
                        text: Q.text('Dialogs.CancelButton'),
                        click: function () { return _this.dialogClose(); }
                    }
                ];
                opt.title = Q.format(Q.text('Site.RolePermissionDialog.DialogTitle'), this.options.title);
                return opt;
            };
            RolePermissionDialog.prototype.getTemplate = function () {
                return '<div id="~_Permissions"></div>';
            };
            RolePermissionDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], RolePermissionDialog);
            return RolePermissionDialog;
        }(Serenity.TemplatedDialog));
        Administration.RolePermissionDialog = RolePermissionDialog;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var TranslationGrid = /** @class */ (function (_super) {
            __extends(TranslationGrid, _super);
            function TranslationGrid(container) {
                var _this = _super.call(this, container) || this;
                _this.element.on('keyup.' + _this.uniqueName + ' change.' + _this.uniqueName, 'input.custom-text', function (e) {
                    var value = Q.trimToNull($(e.target).val());
                    if (value === '') {
                        value = null;
                    }
                    _this.view.getItemById($(e.target).data('key')).CustomText = value;
                    _this.hasChanges = true;
                });
                return _this;
            }
            TranslationGrid.prototype.getIdProperty = function () { return "Key"; };
            TranslationGrid.prototype.getLocalTextPrefix = function () { return "Administration.Translation"; };
            TranslationGrid.prototype.getService = function () { return Administration.TranslationService.baseUrl; };
            TranslationGrid.prototype.onClick = function (e, row, cell) {
                var _this = this;
                _super.prototype.onClick.call(this, e, row, cell);
                if (e.isDefaultPrevented()) {
                    return;
                }
                var item = this.itemAt(row);
                var done;
                if ($(e.target).hasClass('source-text')) {
                    e.preventDefault();
                    done = function () {
                        item.CustomText = item.SourceText;
                        _this.view.updateItem(item.Key, item);
                        _this.hasChanges = true;
                    };
                    if (Q.isTrimmedEmpty(item.CustomText) ||
                        (Q.trimToEmpty(item.CustomText) === Q.trimToEmpty(item.SourceText))) {
                        done();
                        return;
                    }
                    Q.confirm(Q.text('Db.Administration.Translation.OverrideConfirmation'), done);
                    return;
                }
                if ($(e.target).hasClass('target-text')) {
                    e.preventDefault();
                    done = function () {
                        item.CustomText = item.TargetText;
                        _this.view.updateItem(item.Key, item);
                        _this.hasChanges = true;
                    };
                    if (Q.isTrimmedEmpty(item.CustomText) ||
                        (Q.trimToEmpty(item.CustomText) === Q.trimToEmpty(item.TargetText))) {
                        done();
                        return;
                    }
                    Q.confirm(Q.text('Db.Administration.Translation.OverrideConfirmation'), done);
                    return;
                }
            };
            TranslationGrid.prototype.getColumns = function () {
                var columns = [];
                columns.push({ field: 'Key', width: 300, sortable: false });
                columns.push({
                    field: 'SourceText',
                    width: 300,
                    sortable: false,
                    format: function (ctx) {
                        return Q.outerHtml($('<a/>')
                            .addClass('source-text')
                            .text(ctx.value || ''));
                    }
                });
                columns.push({
                    field: 'CustomText',
                    width: 300,
                    sortable: false,
                    format: function (ctx) { return Q.outerHtml($('<input/>')
                        .addClass('custom-text')
                        .attr('value', ctx.value)
                        .attr('type', 'text')
                        .attr('data-key', ctx.item.Key)); }
                });
                columns.push({
                    field: 'TargetText',
                    width: 300,
                    sortable: false,
                    format: function (ctx) { return Q.outerHtml($('<a/>')
                        .addClass('target-text')
                        .text(ctx.value || '')); }
                });
                return columns;
            };
            TranslationGrid.prototype.createToolbarExtensions = function () {
                var _this = this;
                _super.prototype.createToolbarExtensions.call(this);
                var opt = {
                    lookupKey: 'Administration.Language'
                };
                this.sourceLanguage = Serenity.Widget.create({
                    type: Serenity.LookupEditor,
                    element: function (el) { return el.appendTo(_this.toolbar.element).attr('placeholder', '--- ' +
                        Q.text('Db.Administration.Translation.SourceLanguage') + ' ---'); },
                    options: opt
                });
                this.sourceLanguage.changeSelect2(function (e) {
                    if (_this.hasChanges) {
                        _this.saveChanges(_this.targetLanguageKey).then(function () { return _this.refresh(); });
                    }
                    else {
                        _this.refresh();
                    }
                });
                this.targetLanguage = Serenity.Widget.create({
                    type: Serenity.LookupEditor,
                    element: function (el) { return el.appendTo(_this.toolbar.element).attr('placeholder', '--- ' +
                        Q.text('Db.Administration.Translation.TargetLanguage') + ' ---'); },
                    options: opt
                });
                this.targetLanguage.changeSelect2(function (e) {
                    if (_this.hasChanges) {
                        _this.saveChanges(_this.targetLanguageKey).then(function () { return _this.refresh(); });
                    }
                    else {
                        _this.refresh();
                    }
                });
            };
            TranslationGrid.prototype.saveChanges = function (language) {
                var _this = this;
                var translations = {};
                for (var _i = 0, _a = this.getItems(); _i < _a.length; _i++) {
                    var item = _a[_i];
                    translations[item.Key] = item.CustomText;
                }
                return Promise.resolve(Administration.TranslationService.Update({
                    TargetLanguageID: language,
                    Translations: translations
                })).then(function () {
                    _this.hasChanges = false;
                    language = Q.trimToNull(language) || 'invariant';
                    Q.notifySuccess('User translations in "' + language +
                        '" language are saved to "user.texts.' +
                        language + '.json" ' + 'file under "~/App_Data/texts/"', '');
                });
            };
            TranslationGrid.prototype.onViewSubmit = function () {
                var request = this.view.params;
                request.SourceLanguageID = this.sourceLanguage.value;
                this.targetLanguageKey = this.targetLanguage.value || '';
                request.TargetLanguageID = this.targetLanguageKey;
                this.hasChanges = false;
                return _super.prototype.onViewSubmit.call(this);
            };
            TranslationGrid.prototype.getButtons = function () {
                var _this = this;
                return [{
                        title: Q.text('Db.Administration.Translation.SaveChangesButton'),
                        onClick: function (e) { return _this.saveChanges(_this.targetLanguageKey).then(function () { return _this.refresh(); }); },
                        cssClass: 'apply-changes-button'
                    }];
            };
            TranslationGrid.prototype.createQuickSearchInput = function () {
                var _this = this;
                Serenity.GridUtils.addQuickSearchInputCustom(this.toolbar.element, function (field, searchText) {
                    _this.searchText = searchText;
                    _this.view.setItems(_this.view.getItems(), true);
                });
            };
            TranslationGrid.prototype.onViewFilter = function (item) {
                if (!_super.prototype.onViewFilter.call(this, item)) {
                    return false;
                }
                if (!this.searchText) {
                    return true;
                }
                var sd = Select2.util.stripDiacritics;
                var searching = sd(this.searchText).toLowerCase();
                function match(str) {
                    if (!str)
                        return false;
                    return str.toLowerCase().indexOf(searching) >= 0;
                }
                return Q.isEmptyOrNull(searching) || match(item.Key) || match(item.SourceText) ||
                    match(item.TargetText) || match(item.CustomText);
            };
            TranslationGrid.prototype.usePager = function () {
                return false;
            };
            TranslationGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TranslationGrid);
            return TranslationGrid;
        }(Serenity.EntityGrid));
        Administration.TranslationGrid = TranslationGrid;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var UserDialog = /** @class */ (function (_super) {
            __extends(UserDialog, _super);
            function UserDialog() {
                var _this = _super.call(this) || this;
                _this.form = new Administration.UserForm(_this.idPrefix);
                _this.form.Password.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.Password.value.length < 7)
                        return "Password must be at least 7 characters!";
                });
                _this.form.PasswordConfirm.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.Password.value != _this.form.PasswordConfirm.value)
                        return "The passwords entered doesn't match!";
                });
                return _this;
            }
            UserDialog.prototype.getFormKey = function () { return Administration.UserForm.formKey; };
            UserDialog.prototype.getIdProperty = function () { return Administration.UserRow.idProperty; };
            UserDialog.prototype.getIsActiveProperty = function () { return Administration.UserRow.isActiveProperty; };
            UserDialog.prototype.getLocalTextPrefix = function () { return Administration.UserRow.localTextPrefix; };
            UserDialog.prototype.getNameProperty = function () { return Administration.UserRow.nameProperty; };
            UserDialog.prototype.getService = function () { return Administration.UserService.baseUrl; };
            UserDialog.prototype.getToolbarButtons = function () {
                var _this = this;
                var buttons = _super.prototype.getToolbarButtons.call(this);
                buttons.push({
                    title: Q.text('Site.UserDialog.EditRolesButton'),
                    cssClass: 'edit-roles-button',
                    icon: 'icon-people text-blue',
                    onClick: function () {
                        new Administration.UserRoleDialog({
                            userID: _this.entity.UserId,
                            username: _this.entity.Username
                        }).dialogOpen();
                    }
                });
                buttons.push({
                    title: Q.text('Site.UserDialog.EditPermissionsButton'),
                    cssClass: 'edit-permissions-button',
                    icon: 'icon-lock-open text-green',
                    onClick: function () {
                        new Administration.UserPermissionDialog({
                            userID: _this.entity.UserId,
                            username: _this.entity.Username
                        }).dialogOpen();
                    }
                });
                return buttons;
            };
            UserDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                this.toolbar.findButton('edit-roles-button').toggleClass('disabled', this.isNewOrDeleted());
                this.toolbar.findButton("edit-permissions-button").toggleClass("disabled", this.isNewOrDeleted());
            };
            UserDialog.prototype.afterLoadEntity = function () {
                _super.prototype.afterLoadEntity.call(this);
                // these fields are only required in new record mode
                this.form.Password.element.toggleClass('required', this.isNew())
                    .closest('.field').find('sup').toggle(this.isNew());
                this.form.PasswordConfirm.element.toggleClass('required', this.isNew())
                    .closest('.field').find('sup').toggle(this.isNew());
            };
            UserDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], UserDialog);
            return UserDialog;
        }(Serenity.EntityDialog));
        Administration.UserDialog = UserDialog;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var UserGrid = /** @class */ (function (_super) {
            __extends(UserGrid, _super);
            function UserGrid(container) {
                return _super.call(this, container) || this;
            }
            UserGrid.prototype.getColumnsKey = function () { return "Administration.User"; };
            UserGrid.prototype.getDialogType = function () { return Administration.UserDialog; };
            UserGrid.prototype.getIdProperty = function () { return Administration.UserRow.idProperty; };
            UserGrid.prototype.getIsActiveProperty = function () { return Administration.UserRow.isActiveProperty; };
            UserGrid.prototype.getLocalTextPrefix = function () { return Administration.UserRow.localTextPrefix; };
            UserGrid.prototype.getService = function () { return Administration.UserService.baseUrl; };
            UserGrid.prototype.getDefaultSortBy = function () {
                return [Administration.UserRow.Fields.Username];
            };
            UserGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], UserGrid);
            return UserGrid;
        }(Serenity.EntityGrid));
        Administration.UserGrid = UserGrid;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var PermissionCheckEditor = /** @class */ (function (_super) {
            __extends(PermissionCheckEditor, _super);
            function PermissionCheckEditor(container, opt) {
                var _this = _super.call(this, container, opt) || this;
                _this._rolePermissions = {};
                _this._implicitPermissions = {};
                var titleByKey = {};
                var permissionKeys = _this.getSortedGroupAndPermissionKeys(titleByKey);
                var items = permissionKeys.map(function (key) { return ({
                    Key: key,
                    ParentKey: _this.getParentKey(key),
                    Title: titleByKey[key],
                    GrantRevoke: null,
                    IsGroup: key.charAt(key.length - 1) === ':'
                }); });
                _this.byParentKey = Q.toGrouping(items, function (x) { return x.ParentKey; });
                _this.setItems(items);
                return _this;
            }
            PermissionCheckEditor.prototype.getIdProperty = function () { return "Key"; };
            PermissionCheckEditor.prototype.getItemGrantRevokeClass = function (item, grant) {
                if (!item.IsGroup) {
                    return ((item.GrantRevoke === grant) ? ' checked' : '');
                }
                var desc = this.getDescendants(item, true);
                var granted = desc.filter(function (x) { return x.GrantRevoke === grant; });
                if (!granted.length) {
                    return '';
                }
                if (desc.length === granted.length) {
                    return 'checked';
                }
                return 'checked partial';
            };
            PermissionCheckEditor.prototype.roleOrImplicit = function (key) {
                if (this._rolePermissions[key])
                    return true;
                for (var _i = 0, _a = Object.keys(this._rolePermissions); _i < _a.length; _i++) {
                    var k = _a[_i];
                    var d = this._implicitPermissions[k];
                    if (d && d[key])
                        return true;
                }
                for (var _b = 0, _c = Object.keys(this._implicitPermissions); _b < _c.length; _b++) {
                    var i = _c[_b];
                    var item = this.view.getItemById(i);
                    if (item && item.GrantRevoke == true) {
                        var d = this._implicitPermissions[i];
                        if (d && d[key])
                            return true;
                    }
                }
            };
            PermissionCheckEditor.prototype.getItemEffectiveClass = function (item) {
                var _this = this;
                if (item.IsGroup) {
                    var desc = this.getDescendants(item, true);
                    var grantCount = Q.count(desc, function (x) { return x.GrantRevoke === true ||
                        (x.GrantRevoke == null && _this.roleOrImplicit(x.Key)); });
                    if (grantCount === desc.length || desc.length === 0) {
                        return 'allow';
                    }
                    if (grantCount === 0) {
                        return 'deny';
                    }
                    return 'partial';
                }
                var granted = item.GrantRevoke === true ||
                    (item.GrantRevoke == null && this.roleOrImplicit(item.Key));
                return (granted ? ' allow' : ' deny');
            };
            PermissionCheckEditor.prototype.getColumns = function () {
                var _this = this;
                var columns = [{
                        name: Q.text('Site.UserPermissionDialog.Permission'),
                        field: 'Title',
                        format: Serenity.SlickFormatting.treeToggle(function () { return _this.view; }, function (x) { return x.Key; }, function (ctx) {
                            var item = ctx.item;
                            var klass = _this.getItemEffectiveClass(item);
                            return '<span class="effective-permission ' + klass + '">' + Q.htmlEncode(ctx.value) + '</span>';
                        }),
                        width: 495,
                        sortable: false
                    }, {
                        name: Q.text('Site.UserPermissionDialog.Grant'), field: 'Grant',
                        format: function (ctx) {
                            var item1 = ctx.item;
                            var klass1 = _this.getItemGrantRevokeClass(item1, true);
                            return "<span class='check-box grant no-float " + klass1 + "'></span>";
                        },
                        width: 65,
                        sortable: false,
                        headerCssClass: 'align-center',
                        cssClass: 'align-center'
                    }];
                if (this.options.showRevoke) {
                    columns.push({
                        name: Q.text('Site.UserPermissionDialog.Revoke'), field: 'Revoke',
                        format: function (ctx) {
                            var item2 = ctx.item;
                            var klass2 = _this.getItemGrantRevokeClass(item2, false);
                            return '<span class="check-box revoke no-float ' + klass2 + '"></span>';
                        },
                        width: 65,
                        sortable: false,
                        headerCssClass: 'align-center',
                        cssClass: 'align-center'
                    });
                }
                return columns;
            };
            PermissionCheckEditor.prototype.setItems = function (items) {
                Serenity.SlickTreeHelper.setIndents(items, function (x) { return x.Key; }, function (x) { return x.ParentKey; }, false);
                this.view.setItems(items, true);
            };
            PermissionCheckEditor.prototype.onViewSubmit = function () {
                return false;
            };
            PermissionCheckEditor.prototype.onViewFilter = function (item) {
                var _this = this;
                if (!_super.prototype.onViewFilter.call(this, item)) {
                    return false;
                }
                if (!Serenity.SlickTreeHelper.filterById(item, this.view, function (x) { return x.ParentKey; }))
                    return false;
                if (this.searchText) {
                    return this.matchContains(item) || item.IsGroup && Q.any(this.getDescendants(item, false), function (x) { return _this.matchContains(x); });
                }
                return true;
            };
            PermissionCheckEditor.prototype.matchContains = function (item) {
                return Select2.util.stripDiacritics(item.Title || '').toLowerCase().indexOf(this.searchText) >= 0;
            };
            PermissionCheckEditor.prototype.getDescendants = function (item, excludeGroups) {
                var result = [];
                var stack = [item];
                while (stack.length > 0) {
                    var i = stack.pop();
                    var children = this.byParentKey[i.Key];
                    if (!children)
                        continue;
                    for (var _i = 0, children_1 = children; _i < children_1.length; _i++) {
                        var child = children_1[_i];
                        if (!excludeGroups || !child.IsGroup) {
                            result.push(child);
                        }
                        stack.push(child);
                    }
                }
                return result;
            };
            PermissionCheckEditor.prototype.onClick = function (e, row, cell) {
                _super.prototype.onClick.call(this, e, row, cell);
                if (!e.isDefaultPrevented()) {
                    Serenity.SlickTreeHelper.toggleClick(e, row, cell, this.view, function (x) { return x.Key; });
                }
                if (e.isDefaultPrevented()) {
                    return;
                }
                var target = $(e.target);
                var grant = target.hasClass('grant');
                if (grant || target.hasClass('revoke')) {
                    e.preventDefault();
                    var item = this.itemAt(row);
                    var checkedOrPartial = target.hasClass('checked') || target.hasClass('partial');
                    if (checkedOrPartial) {
                        grant = null;
                    }
                    else {
                        grant = grant !== checkedOrPartial;
                    }
                    if (item.IsGroup) {
                        for (var _i = 0, _a = this.getDescendants(item, true); _i < _a.length; _i++) {
                            var d = _a[_i];
                            d.GrantRevoke = grant;
                        }
                    }
                    else
                        item.GrantRevoke = grant;
                    this.slickGrid.invalidate();
                }
            };
            PermissionCheckEditor.prototype.getParentKey = function (key) {
                if (key.charAt(key.length - 1) === ':') {
                    key = key.substr(0, key.length - 1);
                }
                var idx = key.lastIndexOf(':');
                if (idx >= 0) {
                    return key.substr(0, idx + 1);
                }
                return null;
            };
            PermissionCheckEditor.prototype.getButtons = function () {
                return [];
            };
            PermissionCheckEditor.prototype.createToolbarExtensions = function () {
                var _this = this;
                _super.prototype.createToolbarExtensions.call(this);
                Serenity.GridUtils.addQuickSearchInputCustom(this.toolbar.element, function (field, text) {
                    _this.searchText = Select2.util.stripDiacritics(Q.trimToNull(text) || '').toLowerCase();
                    _this.view.setItems(_this.view.getItems(), true);
                });
            };
            PermissionCheckEditor.prototype.getSortedGroupAndPermissionKeys = function (titleByKey) {
                var keys = Q.getRemoteData('Administration.PermissionKeys').Entities;
                var titleWithGroup = {};
                for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                    var k = keys_1[_i];
                    var s = k;
                    if (!s) {
                        continue;
                    }
                    if (s.charAt(s.length - 1) == ':') {
                        s = s.substr(0, s.length - 1);
                        if (s.length === 0) {
                            continue;
                        }
                    }
                    if (titleByKey[s]) {
                        continue;
                    }
                    titleByKey[s] = Q.coalesce(Q.tryGetText('Permission.' + s), s);
                    var parts = s.split(':');
                    var group = '';
                    var groupTitle = '';
                    for (var i = 0; i < parts.length - 1; i++) {
                        group = group + parts[i] + ':';
                        var txt = Q.tryGetText('Permission.' + group);
                        if (txt == null) {
                            txt = parts[i];
                        }
                        titleByKey[group] = txt;
                        groupTitle = groupTitle + titleByKey[group] + ':';
                        titleWithGroup[group] = groupTitle;
                    }
                    titleWithGroup[s] = groupTitle + titleByKey[s];
                }
                keys = Object.keys(titleByKey);
                keys = keys.sort(function (x, y) { return Q.turkishLocaleCompare(titleWithGroup[x], titleWithGroup[y]); });
                return keys;
            };
            Object.defineProperty(PermissionCheckEditor.prototype, "value", {
                get: function () {
                    var result = [];
                    for (var _i = 0, _a = this.view.getItems(); _i < _a.length; _i++) {
                        var item = _a[_i];
                        if (item.GrantRevoke != null && item.Key.charAt(item.Key.length - 1) != ':') {
                            result.push({ PermissionKey: item.Key, Granted: item.GrantRevoke });
                        }
                    }
                    return result;
                },
                set: function (value) {
                    for (var _i = 0, _a = this.view.getItems(); _i < _a.length; _i++) {
                        var item = _a[_i];
                        item.GrantRevoke = null;
                    }
                    if (value != null) {
                        for (var _b = 0, value_1 = value; _b < value_1.length; _b++) {
                            var row = value_1[_b];
                            var r = this.view.getItemById(row.PermissionKey);
                            if (r) {
                                r.GrantRevoke = Q.coalesce(row.Granted, true);
                            }
                        }
                    }
                    this.setItems(this.getItems());
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PermissionCheckEditor.prototype, "rolePermissions", {
                get: function () {
                    return Object.keys(this._rolePermissions);
                },
                set: function (value) {
                    this._rolePermissions = {};
                    if (value) {
                        for (var _i = 0, value_2 = value; _i < value_2.length; _i++) {
                            var k = value_2[_i];
                            this._rolePermissions[k] = true;
                        }
                    }
                    this.setItems(this.getItems());
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PermissionCheckEditor.prototype, "implicitPermissions", {
                set: function (value) {
                    this._implicitPermissions = {};
                    if (value) {
                        for (var _i = 0, _a = Object.keys(value); _i < _a.length; _i++) {
                            var k = _a[_i];
                            this._implicitPermissions[k] = this._implicitPermissions[k] || {};
                            var l = value[k];
                            if (l) {
                                for (var _b = 0, l_1 = l; _b < l_1.length; _b++) {
                                    var s = l_1[_b];
                                    this._implicitPermissions[k][s] = true;
                                }
                            }
                        }
                    }
                },
                enumerable: true,
                configurable: true
            });
            PermissionCheckEditor = __decorate([
                Serenity.Decorators.registerEditor([Serenity.IGetEditValue, Serenity.ISetEditValue])
            ], PermissionCheckEditor);
            return PermissionCheckEditor;
        }(Serenity.DataGrid));
        Administration.PermissionCheckEditor = PermissionCheckEditor;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var UserPermissionDialog = /** @class */ (function (_super) {
            __extends(UserPermissionDialog, _super);
            function UserPermissionDialog(opt) {
                var _this = _super.call(this, opt) || this;
                _this.permissions = new Administration.PermissionCheckEditor(_this.byId('Permissions'), {
                    showRevoke: true
                });
                Administration.UserPermissionService.List({
                    UserID: _this.options.userID,
                    Module: null,
                    Submodule: null
                }, function (response) {
                    _this.permissions.value = response.Entities;
                });
                Administration.UserPermissionService.ListRolePermissions({
                    UserID: _this.options.userID,
                    Module: null,
                    Submodule: null,
                }, function (response) {
                    _this.permissions.rolePermissions = response.Entities;
                });
                _this.permissions.implicitPermissions = Q.getRemoteData('Administration.ImplicitPermissions');
                return _this;
            }
            UserPermissionDialog.prototype.getDialogOptions = function () {
                var _this = this;
                var opt = _super.prototype.getDialogOptions.call(this);
                opt.buttons = [
                    {
                        text: Q.text('Dialogs.OkButton'),
                        click: function (e) {
                            Administration.UserPermissionService.Update({
                                UserID: _this.options.userID,
                                Permissions: _this.permissions.value,
                                Module: null,
                                Submodule: null
                            }, function (response) {
                                _this.dialogClose();
                                window.setTimeout(function () { return Q.notifySuccess(Q.text('Site.UserPermissionDialog.SaveSuccess')); }, 0);
                            });
                        }
                    }, {
                        text: Q.text('Dialogs.CancelButton'),
                        click: function () { return _this.dialogClose(); }
                    }
                ];
                opt.title = Q.format(Q.text('Site.UserPermissionDialog.DialogTitle'), this.options.username);
                return opt;
            };
            UserPermissionDialog.prototype.getTemplate = function () {
                return '<div id="~_Permissions"></div>';
            };
            UserPermissionDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], UserPermissionDialog);
            return UserPermissionDialog;
        }(Serenity.TemplatedDialog));
        Administration.UserPermissionDialog = UserPermissionDialog;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var RoleCheckEditor = /** @class */ (function (_super) {
            __extends(RoleCheckEditor, _super);
            function RoleCheckEditor(div) {
                return _super.call(this, div) || this;
            }
            RoleCheckEditor.prototype.createToolbarExtensions = function () {
                var _this = this;
                _super.prototype.createToolbarExtensions.call(this);
                Serenity.GridUtils.addQuickSearchInputCustom(this.toolbar.element, function (field, text) {
                    _this.searchText = Select2.util.stripDiacritics(text || '').toUpperCase();
                    _this.view.setItems(_this.view.getItems(), true);
                });
            };
            RoleCheckEditor.prototype.getButtons = function () {
                return [];
            };
            RoleCheckEditor.prototype.getTreeItems = function () {
                return Administration.RoleRow.getLookup().items.map(function (role) { return ({
                    id: role.RoleId.toString(),
                    text: role.RoleName
                }); });
            };
            RoleCheckEditor.prototype.onViewFilter = function (item) {
                return _super.prototype.onViewFilter.call(this, item) &&
                    (Q.isEmptyOrNull(this.searchText) ||
                        Select2.util.stripDiacritics(item.text || '')
                            .toUpperCase().indexOf(this.searchText) >= 0);
            };
            RoleCheckEditor = __decorate([
                Serenity.Decorators.registerEditor()
            ], RoleCheckEditor);
            return RoleCheckEditor;
        }(Serenity.CheckTreeEditor));
        Administration.RoleCheckEditor = RoleCheckEditor;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Administration;
    (function (Administration) {
        var UserRoleDialog = /** @class */ (function (_super) {
            __extends(UserRoleDialog, _super);
            function UserRoleDialog(opt) {
                var _this = _super.call(this, opt) || this;
                _this.permissions = new Administration.RoleCheckEditor(_this.byId('Roles'));
                Administration.UserRoleService.List({
                    UserID: _this.options.userID
                }, function (response) {
                    _this.permissions.value = response.Entities.map(function (x) { return x.toString(); });
                });
                return _this;
            }
            UserRoleDialog.prototype.getDialogOptions = function () {
                var _this = this;
                var opt = _super.prototype.getDialogOptions.call(this);
                opt.buttons = [{
                        text: Q.text('Dialogs.OkButton'),
                        click: function () {
                            Q.serviceRequest('Administration/UserRole/Update', {
                                UserID: _this.options.userID,
                                Roles: _this.permissions.value.map(function (x) { return parseInt(x, 10); })
                            }, function (response) {
                                _this.dialogClose();
                                Q.notifySuccess(Q.text('Site.UserRoleDialog.SaveSuccess'));
                            });
                        }
                    }, {
                        text: Q.text('Dialogs.CancelButton'),
                        click: function () { return _this.dialogClose(); }
                    }];
                opt.title = Q.format(Q.text('Site.UserRoleDialog.DialogTitle'), this.options.username);
                return opt;
            };
            UserRoleDialog.prototype.getTemplate = function () {
                return "<div id='~_Roles'></div>";
            };
            UserRoleDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], UserRoleDialog);
            return UserRoleDialog;
        }(Serenity.TemplatedDialog));
        Administration.UserRoleDialog = UserRoleDialog;
    })(Administration = CntAppSeguridad.Administration || (CntAppSeguridad.Administration = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Auditorias;
    (function (Auditorias) {
        var Auditoria2016Dialog = /** @class */ (function (_super) {
            __extends(Auditoria2016Dialog, _super);
            function Auditoria2016Dialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Auditorias.Auditoria2016Form(_this.idPrefix);
                return _this;
            }
            Auditoria2016Dialog.prototype.getFormKey = function () { return Auditorias.Auditoria2016Form.formKey; };
            Auditoria2016Dialog.prototype.getIdProperty = function () { return Auditorias.Auditoria2016Row.idProperty; };
            Auditoria2016Dialog.prototype.getLocalTextPrefix = function () { return Auditorias.Auditoria2016Row.localTextPrefix; };
            Auditoria2016Dialog.prototype.getNameProperty = function () { return Auditorias.Auditoria2016Row.nameProperty; };
            Auditoria2016Dialog.prototype.getService = function () { return Auditorias.Auditoria2016Service.baseUrl; };
            Auditoria2016Dialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], Auditoria2016Dialog);
            return Auditoria2016Dialog;
        }(Serenity.EntityDialog));
        Auditorias.Auditoria2016Dialog = Auditoria2016Dialog;
    })(Auditorias = CntAppSeguridad.Auditorias || (CntAppSeguridad.Auditorias = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Auditorias;
    (function (Auditorias) {
        var Auditoria2016Grid = /** @class */ (function (_super) {
            __extends(Auditoria2016Grid, _super);
            function Auditoria2016Grid(container) {
                return _super.call(this, container) || this;
            }
            Auditoria2016Grid.prototype.getColumnsKey = function () { return 'Auditorias.Auditoria2016'; };
            Auditoria2016Grid.prototype.getDialogType = function () { return Auditorias.Auditoria2016Dialog; };
            Auditoria2016Grid.prototype.getIdProperty = function () { return Auditorias.Auditoria2016Row.idProperty; };
            Auditoria2016Grid.prototype.getLocalTextPrefix = function () { return Auditorias.Auditoria2016Row.localTextPrefix; };
            Auditoria2016Grid.prototype.getService = function () { return Auditorias.Auditoria2016Service.baseUrl; };
            Auditoria2016Grid = __decorate([
                Serenity.Decorators.registerClass()
            ], Auditoria2016Grid);
            return Auditoria2016Grid;
        }(Serenity.EntityGrid));
        Auditorias.Auditoria2016Grid = Auditoria2016Grid;
    })(Auditorias = CntAppSeguridad.Auditorias || (CntAppSeguridad.Auditorias = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Auditorias;
    (function (Auditorias) {
        var Auditoria2017Dialog = /** @class */ (function (_super) {
            __extends(Auditoria2017Dialog, _super);
            function Auditoria2017Dialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Auditorias.Auditoria2017Form(_this.idPrefix);
                return _this;
            }
            Auditoria2017Dialog.prototype.getFormKey = function () { return Auditorias.Auditoria2017Form.formKey; };
            Auditoria2017Dialog.prototype.getIdProperty = function () { return Auditorias.Auditoria2017Row.idProperty; };
            Auditoria2017Dialog.prototype.getLocalTextPrefix = function () { return Auditorias.Auditoria2017Row.localTextPrefix; };
            Auditoria2017Dialog.prototype.getNameProperty = function () { return Auditorias.Auditoria2017Row.nameProperty; };
            Auditoria2017Dialog.prototype.getService = function () { return Auditorias.Auditoria2017Service.baseUrl; };
            Auditoria2017Dialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], Auditoria2017Dialog);
            return Auditoria2017Dialog;
        }(Serenity.EntityDialog));
        Auditorias.Auditoria2017Dialog = Auditoria2017Dialog;
    })(Auditorias = CntAppSeguridad.Auditorias || (CntAppSeguridad.Auditorias = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Auditorias;
    (function (Auditorias) {
        var Auditoria2017Grid = /** @class */ (function (_super) {
            __extends(Auditoria2017Grid, _super);
            function Auditoria2017Grid(container) {
                return _super.call(this, container) || this;
            }
            Auditoria2017Grid.prototype.getColumnsKey = function () { return 'Auditorias.Auditoria2017'; };
            Auditoria2017Grid.prototype.getDialogType = function () { return Auditorias.Auditoria2017Dialog; };
            Auditoria2017Grid.prototype.getIdProperty = function () { return Auditorias.Auditoria2017Row.idProperty; };
            Auditoria2017Grid.prototype.getLocalTextPrefix = function () { return Auditorias.Auditoria2017Row.localTextPrefix; };
            Auditoria2017Grid.prototype.getService = function () { return Auditorias.Auditoria2017Service.baseUrl; };
            Auditoria2017Grid = __decorate([
                Serenity.Decorators.registerClass()
            ], Auditoria2017Grid);
            return Auditoria2017Grid;
        }(Serenity.EntityGrid));
        Auditorias.Auditoria2017Grid = Auditoria2017Grid;
    })(Auditorias = CntAppSeguridad.Auditorias || (CntAppSeguridad.Auditorias = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var BasicProgressDialog = /** @class */ (function (_super) {
        __extends(BasicProgressDialog, _super);
        function BasicProgressDialog() {
            var _this = _super.call(this) || this;
            _this.byId('ProgressBar').progressbar({
                max: 100,
                value: 0,
                change: function (e, v) {
                    _this.byId('ProgressLabel').text(_this.value + ' / ' + _this.max);
                }
            });
            return _this;
        }
        Object.defineProperty(BasicProgressDialog.prototype, "max", {
            get: function () {
                return this.byId('ProgressBar').progressbar().progressbar('option', 'max');
            },
            set: function (value) {
                this.byId('ProgressBar').progressbar().progressbar('option', 'max', value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BasicProgressDialog.prototype, "value", {
            get: function () {
                return this.byId('ProgressBar').progressbar('value');
            },
            set: function (value) {
                this.byId('ProgressBar').progressbar().progressbar('value', value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BasicProgressDialog.prototype, "title", {
            get: function () {
                return this.element.dialog().dialog('option', 'title');
            },
            set: function (value) {
                this.element.dialog().dialog('option', 'title', value);
            },
            enumerable: true,
            configurable: true
        });
        BasicProgressDialog.prototype.getDialogOptions = function () {
            var _this = this;
            var opt = _super.prototype.getDialogOptions.call(this);
            opt.title = Q.text('Site.BasicProgressDialog.PleaseWait');
            opt.width = 600;
            opt.buttons = [{
                    text: Q.text('Dialogs.CancelButton'),
                    click: function () {
                        _this.cancelled = true;
                        _this.element.closest('.ui-dialog')
                            .find('.ui-dialog-buttonpane .ui-button')
                            .attr('disabled', 'disabled')
                            .css('opacity', '0.5');
                        _this.element.dialog('option', 'title', Q.trimToNull(_this.cancelTitle) ||
                            Q.text('Site.BasicProgressDialog.CancelTitle'));
                    }
                }];
            return opt;
        };
        BasicProgressDialog.prototype.initDialog = function () {
            _super.prototype.initDialog.call(this);
            this.element.closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
        };
        BasicProgressDialog.prototype.getTemplate = function () {
            return ("<div class='s-DialogContent s-BasicProgressDialogContent'>" +
                "<div id='~_StatusText' class='status-text' ></div>" +
                "<div id='~_ProgressBar' class='progress-bar'>" +
                "<div id='~_ProgressLabel' class='progress-label' ></div>" +
                "</div>" +
                "</div>");
        };
        return BasicProgressDialog;
    }(Serenity.TemplatedDialog));
    CntAppSeguridad.BasicProgressDialog = BasicProgressDialog;
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var BulkServiceAction = /** @class */ (function () {
            function BulkServiceAction() {
            }
            BulkServiceAction.prototype.createProgressDialog = function () {
                this.progressDialog = new CntAppSeguridad.BasicProgressDialog();
                this.progressDialog.dialogOpen();
                this.progressDialog.max = this.keys.length;
                this.progressDialog.value = 0;
            };
            BulkServiceAction.prototype.getConfirmationFormat = function () {
                return Q.text('Site.BulkServiceAction.ConfirmationFormat');
            };
            BulkServiceAction.prototype.getConfirmationMessage = function (targetCount) {
                return Q.format(this.getConfirmationFormat(), targetCount);
            };
            BulkServiceAction.prototype.confirm = function (targetCount, action) {
                Q.confirm(this.getConfirmationMessage(targetCount), action);
            };
            BulkServiceAction.prototype.getNothingToProcessMessage = function () {
                return Q.text('Site.BulkServiceAction.NothingToProcess');
            };
            BulkServiceAction.prototype.nothingToProcess = function () {
                Q.notifyError(this.getNothingToProcessMessage());
            };
            BulkServiceAction.prototype.getParallelRequests = function () {
                return 1;
            };
            BulkServiceAction.prototype.getBatchSize = function () {
                return 1;
            };
            BulkServiceAction.prototype.startParallelExecution = function () {
                this.createProgressDialog();
                this.successCount = 0;
                this.errorCount = 0;
                this.pendingRequests = 0;
                this.completedRequests = 0;
                this.errorCount = 0;
                this.errorByKey = {};
                this.queue = this.keys.slice();
                this.queueIndex = 0;
                var parallelRequests = this.getParallelRequests();
                while (parallelRequests-- > 0) {
                    this.executeNextBatch();
                }
            };
            BulkServiceAction.prototype.serviceCallCleanup = function () {
                this.pendingRequests--;
                this.completedRequests++;
                var title = Q.text((this.progressDialog.cancelled ?
                    'Site.BasicProgressDialog.CancelTitle' : 'Site.BasicProgressDialog.PleaseWait'));
                title += ' (';
                if (this.successCount > 0) {
                    title += Q.format(Q.text('Site.BulkServiceAction.SuccessCount'), this.successCount);
                }
                if (this.errorCount > 0) {
                    if (this.successCount > 0) {
                        title += ', ';
                    }
                    title += Q.format(Q.text('Site.BulkServiceAction.ErrorCount'), this.errorCount);
                }
                this.progressDialog.title = title + ')';
                this.progressDialog.value = this.successCount + this.errorCount;
                if (!this.progressDialog.cancelled && this.progressDialog.value < this.keys.length) {
                    this.executeNextBatch();
                }
                else if (this.pendingRequests === 0) {
                    this.progressDialog.dialogClose();
                    this.showResults();
                    if (this.done) {
                        this.done();
                        this.done = null;
                    }
                }
            };
            BulkServiceAction.prototype.executeForBatch = function (batch) {
            };
            BulkServiceAction.prototype.executeNextBatch = function () {
                var batchSize = this.getBatchSize();
                var batch = [];
                while (true) {
                    if (batch.length >= batchSize) {
                        break;
                    }
                    if (this.queueIndex >= this.queue.length) {
                        break;
                    }
                    batch.push(this.queue[this.queueIndex++]);
                }
                if (batch.length > 0) {
                    this.pendingRequests++;
                    this.executeForBatch(batch);
                }
            };
            BulkServiceAction.prototype.getAllHadErrorsFormat = function () {
                return Q.text('Site.BulkServiceAction.AllHadErrorsFormat');
            };
            BulkServiceAction.prototype.showAllHadErrors = function () {
                Q.notifyError(Q.format(this.getAllHadErrorsFormat(), this.errorCount));
            };
            BulkServiceAction.prototype.getSomeHadErrorsFormat = function () {
                return Q.text('Site.BulkServiceAction.SomeHadErrorsFormat');
            };
            BulkServiceAction.prototype.showSomeHadErrors = function () {
                Q.notifyWarning(Q.format(this.getSomeHadErrorsFormat(), this.successCount, this.errorCount));
            };
            BulkServiceAction.prototype.getAllSuccessFormat = function () {
                return Q.text('Site.BulkServiceAction.AllSuccessFormat');
            };
            BulkServiceAction.prototype.showAllSuccess = function () {
                Q.notifySuccess(Q.format(this.getAllSuccessFormat(), this.successCount));
            };
            BulkServiceAction.prototype.showResults = function () {
                if (this.errorCount === 0 && this.successCount === 0) {
                    this.nothingToProcess();
                    return;
                }
                if (this.errorCount > 0 && this.successCount === 0) {
                    this.showAllHadErrors();
                    return;
                }
                if (this.errorCount > 0) {
                    this.showSomeHadErrors();
                    return;
                }
                this.showAllSuccess();
            };
            BulkServiceAction.prototype.execute = function (keys) {
                var _this = this;
                this.keys = keys;
                if (this.keys.length === 0) {
                    this.nothingToProcess();
                    return;
                }
                this.confirm(this.keys.length, function () { return _this.startParallelExecution(); });
            };
            BulkServiceAction.prototype.get_successCount = function () {
                return this.successCount;
            };
            BulkServiceAction.prototype.set_successCount = function (value) {
                this.successCount = value;
            };
            BulkServiceAction.prototype.get_errorCount = function () {
                return this.errorCount;
            };
            BulkServiceAction.prototype.set_errorCount = function (value) {
                this.errorCount = value;
            };
            return BulkServiceAction;
        }());
        Common.BulkServiceAction = BulkServiceAction;
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var DialogUtils;
    (function (DialogUtils) {
        function pendingChangesConfirmation(element, hasPendingChanges) {
            element.on('dialogbeforeclose panelbeforeclose', function (e) {
                if (!Serenity.WX.hasOriginalEvent(e) || !hasPendingChanges()) {
                    return;
                }
                e.preventDefault();
                Q.confirm('You have pending changes. Save them?', function () { return element.find('div.save-and-close-button').click(); }, {
                    onNo: function () {
                        if (element.hasClass('ui-dialog-content'))
                            element.dialog('close');
                        else if (element.hasClass('s-Panel'))
                            Serenity.TemplatedDialog.closePanel(element);
                    }
                });
            });
        }
        DialogUtils.pendingChangesConfirmation = pendingChangesConfirmation;
    })(DialogUtils = CntAppSeguridad.DialogUtils || (CntAppSeguridad.DialogUtils = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var EnumSelectFormatter = /** @class */ (function () {
            function EnumSelectFormatter() {
                this.allowClear = true;
            }
            EnumSelectFormatter.prototype.format = function (ctx) {
                var enumType = Serenity.EnumTypeRegistry.get(this.enumKey);
                var sb = "<select>";
                if (this.allowClear) {
                    sb += '<option value="">';
                    sb += Q.htmlEncode(this.emptyItemText || Q.text("Controls.SelectEditor.EmptyItemText"));
                    sb += '</option>';
                }
                for (var _i = 0, _a = Object.keys(enumType).filter(function (v) { return !isNaN(parseInt(v, 10)); }); _i < _a.length; _i++) {
                    var x = _a[_i];
                    sb += '<option value="' + Q.attrEncode(x) + '"';
                    if (x == ctx.value)
                        sb += " selected";
                    var name = enumType[x];
                    sb += ">";
                    sb += Q.htmlEncode(Q.tryGetText("Enums." + this.enumKey + "." + name) || name);
                    sb += "</option>";
                }
                sb += "</select>";
                return sb;
            };
            __decorate([
                Serenity.Decorators.option()
            ], EnumSelectFormatter.prototype, "enumKey", void 0);
            __decorate([
                Serenity.Decorators.option()
            ], EnumSelectFormatter.prototype, "allowClear", void 0);
            __decorate([
                Serenity.Decorators.option()
            ], EnumSelectFormatter.prototype, "emptyItemText", void 0);
            EnumSelectFormatter = __decorate([
                Serenity.Decorators.registerFormatter()
            ], EnumSelectFormatter);
            return EnumSelectFormatter;
        }());
        Common.EnumSelectFormatter = EnumSelectFormatter;
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var ExcelExportHelper;
        (function (ExcelExportHelper) {
            function createToolButton(options) {
                return {
                    hint: Q.coalesce(options.hint, 'Excel'),
                    title: Q.coalesce(options.title, ''),
                    cssClass: 'export-xlsx-button',
                    onClick: function () {
                        if (!options.onViewSubmit()) {
                            return;
                        }
                        var grid = options.grid;
                        var request = Q.deepClone(grid.getView().params);
                        request.Take = 0;
                        request.Skip = 0;
                        var sortBy = grid.getView().sortBy;
                        if (sortBy) {
                            request.Sort = sortBy;
                        }
                        request.IncludeColumns = [];
                        var columns = grid.getGrid().getColumns();
                        for (var _i = 0, columns_1 = columns; _i < columns_1.length; _i++) {
                            var column = columns_1[_i];
                            request.IncludeColumns.push(column.id || column.field);
                        }
                        Q.postToService({ service: options.service, request: request, target: '_blank' });
                    },
                    separator: options.separator
                };
            }
            ExcelExportHelper.createToolButton = createToolButton;
        })(ExcelExportHelper = Common.ExcelExportHelper || (Common.ExcelExportHelper = {}));
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var GridEditorBase = /** @class */ (function (_super) {
            __extends(GridEditorBase, _super);
            function GridEditorBase(container) {
                var _this = _super.call(this, container) || this;
                _this.nextId = 1;
                return _this;
            }
            GridEditorBase.prototype.getIdProperty = function () { return "__id"; };
            GridEditorBase.prototype.id = function (entity) {
                return entity[this.getIdProperty()];
            };
            GridEditorBase.prototype.getNextId = function () {
                return "`" + this.nextId++;
            };
            GridEditorBase.prototype.setNewId = function (entity) {
                entity[this.getIdProperty()] = this.getNextId();
            };
            GridEditorBase.prototype.save = function (opt, callback) {
                var _this = this;
                var request = opt.request;
                var row = Q.deepClone(request.Entity);
                var id = this.id(row);
                if (id == null) {
                    row[this.getIdProperty()] = this.getNextId();
                }
                if (!this.validateEntity(row, id)) {
                    return;
                }
                var items = this.view.getItems().slice();
                if (id == null) {
                    items.push(row);
                }
                else {
                    var index = Q.indexOf(items, function (x) { return _this.id(x) === id; });
                    items[index] = Q.deepClone({}, items[index], row);
                }
                this.setEntities(items);
                callback({});
            };
            GridEditorBase.prototype.deleteEntity = function (id) {
                this.view.deleteItem(id);
                return true;
            };
            GridEditorBase.prototype.validateEntity = function (row, id) {
                return true;
            };
            GridEditorBase.prototype.setEntities = function (items) {
                this.view.setItems(items, true);
            };
            GridEditorBase.prototype.getNewEntity = function () {
                return {};
            };
            GridEditorBase.prototype.getButtons = function () {
                var _this = this;
                return [{
                        title: this.getAddButtonCaption(),
                        cssClass: 'add-button',
                        onClick: function () {
                            _this.createEntityDialog(_this.getItemType(), function (dlg) {
                                var dialog = dlg;
                                dialog.onSave = function (opt, callback) { return _this.save(opt, callback); };
                                dialog.loadEntityAndOpenDialog(_this.getNewEntity());
                            });
                        }
                    }];
            };
            GridEditorBase.prototype.editItem = function (entityOrId) {
                var _this = this;
                var id = entityOrId;
                var item = this.view.getItemById(id);
                this.createEntityDialog(this.getItemType(), function (dlg) {
                    var dialog = dlg;
                    dialog.onDelete = function (opt, callback) {
                        if (!_this.deleteEntity(id)) {
                            return;
                        }
                        callback({});
                    };
                    dialog.onSave = function (opt, callback) { return _this.save(opt, callback); };
                    dialog.loadEntityAndOpenDialog(item);
                });
                ;
            };
            GridEditorBase.prototype.getEditValue = function (property, target) {
                target[property.name] = this.value;
            };
            GridEditorBase.prototype.setEditValue = function (source, property) {
                this.value = source[property.name];
            };
            Object.defineProperty(GridEditorBase.prototype, "value", {
                get: function () {
                    var p = this.getIdProperty();
                    return this.view.getItems().map(function (x) {
                        var y = Q.deepClone(x);
                        var id = y[p];
                        if (id && id.toString().charAt(0) == '`')
                            delete y[p];
                        return y;
                    });
                },
                set: function (value) {
                    var _this = this;
                    var p = this.getIdProperty();
                    this.view.setItems((value || []).map(function (x) {
                        var y = Q.deepClone(x);
                        if (y[p] == null)
                            y[p] = "`" + _this.getNextId();
                        return y;
                    }), true);
                },
                enumerable: true,
                configurable: true
            });
            GridEditorBase.prototype.getGridCanLoad = function () {
                return false;
            };
            GridEditorBase.prototype.usePager = function () {
                return false;
            };
            GridEditorBase.prototype.getInitialTitle = function () {
                return null;
            };
            GridEditorBase.prototype.createQuickSearchInput = function () {
            };
            GridEditorBase = __decorate([
                Serenity.Decorators.registerClass([Serenity.IGetEditValue, Serenity.ISetEditValue]),
                Serenity.Decorators.editor(),
                Serenity.Decorators.element("<div/>")
            ], GridEditorBase);
            return GridEditorBase;
        }(Serenity.EntityGrid));
        Common.GridEditorBase = GridEditorBase;
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var GridEditorDialog = /** @class */ (function (_super) {
            __extends(GridEditorDialog, _super);
            function GridEditorDialog() {
                return _super !== null && _super.apply(this, arguments) || this;
            }
            GridEditorDialog.prototype.getIdProperty = function () { return "__id"; };
            GridEditorDialog.prototype.destroy = function () {
                this.onSave = null;
                this.onDelete = null;
                _super.prototype.destroy.call(this);
            };
            GridEditorDialog.prototype.updateInterface = function () {
                _super.prototype.updateInterface.call(this);
                // apply changes button doesn't work properly with in-memory grids yet
                if (this.applyChangesButton) {
                    this.applyChangesButton.hide();
                }
            };
            GridEditorDialog.prototype.saveHandler = function (options, callback) {
                this.onSave && this.onSave(options, callback);
            };
            GridEditorDialog.prototype.deleteHandler = function (options, callback) {
                this.onDelete && this.onDelete(options, callback);
            };
            GridEditorDialog = __decorate([
                Serenity.Decorators.registerClass()
            ], GridEditorDialog);
            return GridEditorDialog;
        }(Serenity.EntityDialog));
        Common.GridEditorDialog = GridEditorDialog;
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    /**
     * This is an editor widget but it only displays a text, not edits it.
     *
     */
    var StaticTextBlock = /** @class */ (function (_super) {
        __extends(StaticTextBlock, _super);
        function StaticTextBlock(container, options) {
            var _this = _super.call(this, container, options) || this;
            // hide the caption label for this editor if in a form. ugly hack
            if (_this.options.hideLabel)
                _this.element.closest('.field').find('.caption').hide();
            _this.updateElementContent();
            return _this;
        }
        StaticTextBlock.prototype.updateElementContent = function () {
            var text = Q.coalesce(this.options.text, this.value);
            // if isLocalText is set, text is actually a local text key
            if (this.options.isLocalText)
                text = Q.text(text);
            // don't html encode if isHtml option is true
            if (this.options.isHtml)
                this.element.html(text);
            else
                this.element.text(text);
        };
        /**
         * By implementing ISetEditValue interface, we allow this editor to display its field value.
         * But only do this when our text content is not explicitly set in options
         */
        StaticTextBlock.prototype.setEditValue = function (source, property) {
            if (this.options.text == null) {
                this.value = Q.coalesce(this.options.text, source[property.name]);
                this.updateElementContent();
            }
        };
        StaticTextBlock = __decorate([
            Serenity.Decorators.element("<div/>"),
            Serenity.Decorators.registerEditor([Serenity.ISetEditValue])
        ], StaticTextBlock);
        return StaticTextBlock;
    }(Serenity.Widget));
    CntAppSeguridad.StaticTextBlock = StaticTextBlock;
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var LanguageSelection = /** @class */ (function (_super) {
            __extends(LanguageSelection, _super);
            function LanguageSelection(select, currentLanguage) {
                var _this = _super.call(this, select) || this;
                currentLanguage = Q.coalesce(currentLanguage, 'en');
                _this.change(function (e) {
                    $.cookie('LanguagePreference', select.val(), {
                        path: Q.Config.applicationPath,
                        expires: 365
                    });
                    window.location.reload(true);
                });
                Q.getLookupAsync('Administration.Language').then(function (x) {
                    if (!Q.any(x.items, function (z) { return z.LanguageId === currentLanguage; })) {
                        var idx = currentLanguage.lastIndexOf('-');
                        if (idx >= 0) {
                            currentLanguage = currentLanguage.substr(0, idx);
                            if (!Q.any(x.items, function (y) { return y.LanguageId === currentLanguage; })) {
                                currentLanguage = 'en';
                            }
                        }
                        else {
                            currentLanguage = 'en';
                        }
                    }
                    for (var _i = 0, _a = x.items; _i < _a.length; _i++) {
                        var l = _a[_i];
                        Q.addOption(select, l.LanguageId, l.LanguageName);
                    }
                    select.val(currentLanguage);
                });
                return _this;
            }
            return LanguageSelection;
        }(Serenity.Widget));
        Common.LanguageSelection = LanguageSelection;
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var SidebarSearch = /** @class */ (function (_super) {
            __extends(SidebarSearch, _super);
            function SidebarSearch(input, menuUL) {
                var _this = _super.call(this, input) || this;
                new Serenity.QuickSearchInput(input, {
                    onSearch: function (field, text, success) {
                        _this.updateMatchFlags(text);
                        success(true);
                    }
                });
                _this.menuUL = menuUL;
                return _this;
            }
            SidebarSearch.prototype.updateMatchFlags = function (text) {
                var liList = this.menuUL.find('li').removeClass('non-match');
                text = Q.trimToNull(text);
                if (text == null) {
                    liList.show();
                    liList.removeClass('expanded');
                    return;
                }
                var parts = text.replace(',', ' ').split(' ').filter(function (x) { return !Q.isTrimmedEmpty(x); });
                for (var i = 0; i < parts.length; i++) {
                    parts[i] = Q.trimToNull(Select2.util.stripDiacritics(parts[i]).toUpperCase());
                }
                var items = liList;
                items.each(function (idx, e) {
                    var x = $(e);
                    var title = Select2.util.stripDiacritics(Q.coalesce(x.text(), '').toUpperCase());
                    for (var _i = 0, parts_1 = parts; _i < parts_1.length; _i++) {
                        var p = parts_1[_i];
                        if (p != null && !(title.indexOf(p) !== -1)) {
                            x.addClass('non-match');
                            break;
                        }
                    }
                });
                var matchingItems = items.not('.non-match');
                var visibles = matchingItems.parents('li').add(matchingItems);
                var nonVisibles = liList.not(visibles);
                nonVisibles.hide().addClass('non-match');
                visibles.show();
                liList.addClass('expanded');
            };
            return SidebarSearch;
        }(Serenity.Widget));
        Common.SidebarSearch = SidebarSearch;
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var ThemeSelection = /** @class */ (function (_super) {
            __extends(ThemeSelection, _super);
            function ThemeSelection(select) {
                var _this = _super.call(this, select) || this;
                _this.change(function (e) {
                    $.cookie('ThemePreference', select.val(), {
                        path: Q.Config.applicationPath,
                        expires: 365
                    });
                    $('body').removeClass('skin-' + _this.getCurrentTheme());
                    $('body').addClass('skin-' + select.val());
                });
                Q.addOption(select, 'blue', Q.text('Site.Layout.ThemeBlue'));
                Q.addOption(select, 'blue-light', Q.text('Site.Layout.ThemeBlueLight'));
                Q.addOption(select, 'purple', Q.text('Site.Layout.ThemePurple'));
                Q.addOption(select, 'purple-light', Q.text('Site.Layout.ThemePurpleLight'));
                Q.addOption(select, 'red', Q.text('Site.Layout.ThemeRed'));
                Q.addOption(select, 'red-light', Q.text('Site.Layout.ThemeRedLight'));
                Q.addOption(select, 'green', Q.text('Site.Layout.ThemeGreen'));
                Q.addOption(select, 'green-light', Q.text('Site.Layout.ThemeGreenLight'));
                Q.addOption(select, 'yellow', Q.text('Site.Layout.ThemeYellow'));
                Q.addOption(select, 'yellow-light', Q.text('Site.Layout.ThemeYellowLight'));
                Q.addOption(select, 'black', Q.text('Site.Layout.ThemeBlack'));
                Q.addOption(select, 'black-light', Q.text('Site.Layout.ThemeBlackLight'));
                select.val(_this.getCurrentTheme());
                return _this;
            }
            ThemeSelection.prototype.getCurrentTheme = function () {
                var skinClass = Q.first(($('body').attr('class') || '').split(' '), function (x) { return Q.startsWith(x, 'skin-'); });
                if (skinClass) {
                    return skinClass.substr(5);
                }
                return 'blue';
            };
            return ThemeSelection;
        }(Serenity.Widget));
        Common.ThemeSelection = ThemeSelection;
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var PdfExportHelper;
        (function (PdfExportHelper) {
            function toAutoTableColumns(srcColumns, columnStyles, columnTitles) {
                return srcColumns.map(function (src) {
                    var col = {
                        dataKey: src.id || src.field,
                        title: src.name || ''
                    };
                    if (columnTitles && columnTitles[col.dataKey] != null)
                        col.title = columnTitles[col.dataKey];
                    var style = {};
                    if ((src.cssClass || '').indexOf("align-right") >= 0)
                        style.halign = 'right';
                    else if ((src.cssClass || '').indexOf("align-center") >= 0)
                        style.halign = 'center';
                    columnStyles[col.dataKey] = style;
                    return col;
                });
            }
            function toAutoTableData(entities, keys, srcColumns) {
                var el = document.createElement('span');
                var row = 0;
                return entities.map(function (item) {
                    var dst = {};
                    for (var cell = 0; cell < srcColumns.length; cell++) {
                        var src = srcColumns[cell];
                        var fld = src.field || '';
                        var key = keys[cell];
                        var txt = void 0;
                        var html = void 0;
                        if (src.formatter) {
                            html = src.formatter(row, cell, item[fld], src, item);
                        }
                        else if (src.format) {
                            html = src.format({ row: row, cell: cell, item: item, value: item[fld] });
                        }
                        else {
                            dst[key] = item[fld];
                            continue;
                        }
                        if (!html || (html.indexOf('<') < 0 && html.indexOf('&') < 0))
                            dst[key] = html;
                        else {
                            el.innerHTML = html;
                            if (el.children.length == 1 &&
                                $(el.children[0]).is(":input")) {
                                dst[key] = $(el.children[0]).val();
                            }
                            else if (el.children.length == 1 &&
                                $(el.children).is('.check-box')) {
                                dst[key] = $(el.children).hasClass("checked") ? "X" : "";
                            }
                            else
                                dst[key] = el.textContent || '';
                        }
                    }
                    row++;
                    return dst;
                });
            }
            function exportToPdf(options) {
                var g = options.grid;
                if (!options.onViewSubmit())
                    return;
                includeAutoTable();
                var request = Q.deepClone(g.view.params);
                request.Take = 0;
                request.Skip = 0;
                var sortBy = g.view.sortBy;
                if (sortBy != null)
                    request.Sort = sortBy;
                var gridColumns = g.slickGrid.getColumns();
                gridColumns = gridColumns.filter(function (x) { return x.id !== "__select__"; });
                request.IncludeColumns = [];
                for (var _i = 0, gridColumns_1 = gridColumns; _i < gridColumns_1.length; _i++) {
                    var column = gridColumns_1[_i];
                    request.IncludeColumns.push(column.id || column.field);
                }
                Q.serviceCall({
                    url: g.view.url,
                    request: request,
                    onSuccess: function (response) {
                        var doc = new jsPDF('l', 'pt');
                        var srcColumns = gridColumns;
                        var columnStyles = {};
                        var columns = toAutoTableColumns(srcColumns, columnStyles, options.columnTitles);
                        var keys = columns.map(function (x) { return x.dataKey; });
                        var entities = response.Entities || [];
                        var data = toAutoTableData(entities, keys, srcColumns);
                        doc.setFontSize(options.titleFontSize || 10);
                        doc.setFontStyle('bold');
                        var reportTitle = options.reportTitle || g.getTitle() || "Report";
                        doc.autoTableText(reportTitle, doc.internal.pageSize.width / 2, options.titleTop || 25, { halign: 'center' });
                        var totalPagesExp = "{{T}}";
                        var pageNumbers = options.pageNumbers == null || options.pageNumbers;
                        var autoOptions = $.extend({
                            margin: { top: 25, left: 25, right: 25, bottom: pageNumbers ? 25 : 30 },
                            startY: 60,
                            styles: {
                                fontSize: 8,
                                overflow: 'linebreak',
                                cellPadding: 2,
                                valign: 'middle'
                            },
                            columnStyles: columnStyles
                        }, options.tableOptions);
                        if (pageNumbers) {
                            var footer = function (data) {
                                var str = data.pageCount;
                                // Total page number plugin only available in jspdf v1.0+
                                if (typeof doc.putTotalPages === 'function') {
                                    str = str + " / " + totalPagesExp;
                                }
                                doc.autoTableText(str, doc.internal.pageSize.width / 2, doc.internal.pageSize.height - autoOptions.margin.bottom, {
                                    halign: 'center'
                                });
                            };
                            autoOptions.afterPageContent = footer;
                        }
                        // Print header of page
                        if (options.printDateTimeHeader == null || options.printDateTimeHeader) {
                            var beforePage = function (data) {
                                doc.setFontStyle('normal');
                                doc.setFontSize(8);
                                // Date and time of the report
                                doc.autoTableText(Q.formatDate(new Date(), "dd-MM-yyyy HH:mm"), doc.internal.pageSize.width - autoOptions.margin.right, 13, {
                                    halign: 'right'
                                });
                            };
                            autoOptions.beforePageContent = beforePage;
                        }
                        doc.autoTable(columns, data, autoOptions);
                        if (typeof doc.putTotalPages === 'function') {
                            doc.putTotalPages(totalPagesExp);
                        }
                        if (!options.output || options.output == "file") {
                            var fileName = options.fileName || options.reportTitle || "{0}_{1}.pdf";
                            fileName = Q.format(fileName, g.getTitle() || "report", Q.formatDate(new Date(), "yyyyMMdd_HHmm"));
                            doc.save(fileName);
                            return;
                        }
                        if (options.autoPrint)
                            doc.autoPrint();
                        var output = options.output;
                        if (output == 'newwindow' || '_blank')
                            output = 'dataurlnewwindow';
                        else if (output == 'window')
                            output = 'datauri';
                        if (output == 'datauri')
                            doc.output(output);
                        else {
                            var tmpOut = doc.output('datauristring');
                            if (output == 'dataurlnewwindow') {
                                var fileTmpName = options.reportTitle || g.getTitle();
                                var url_with_name = tmpOut.replace("data:application/pdf;", "data:application/pdf;name=" + fileTmpName + ".pdf;");
                                var html = '<html>' +
                                    '<style>html, body { padding: 0; margin: 0; } iframe { width: 100%; height: 100%; border: 0;}  </style>' +
                                    '<body>' +
                                    '<p></p>' +
                                    '<iframe type="application/pdf" src="' + url_with_name + '"></iframe>' +
                                    '</body></html>';
                                var a = window.open("about:blank", "_blank");
                                a.document.write(html);
                                a.document.close();
                            }
                        }
                    }
                });
            }
            PdfExportHelper.exportToPdf = exportToPdf;
            function createToolButton(options) {
                return {
                    title: options.title || '',
                    hint: options.hint || 'PDF',
                    cssClass: 'export-pdf-button',
                    onClick: function () { return exportToPdf(options); },
                    separator: options.separator
                };
            }
            PdfExportHelper.createToolButton = createToolButton;
            function includeJsPDF() {
                if (typeof jsPDF !== "undefined")
                    return;
                var script = $("jsPDFScript");
                if (script.length > 0)
                    return;
                $("<script/>")
                    .attr("type", "text/javascript")
                    .attr("id", "jsPDFScript")
                    .attr("src", Q.resolveUrl("~/Scripts/jspdf.min.js"))
                    .appendTo(document.head);
            }
            function includeAutoTable() {
                includeJsPDF();
                if (typeof jsPDF === "undefined" ||
                    typeof jsPDF.API == "undefined" ||
                    typeof jsPDF.API.autoTable !== "undefined")
                    return;
                var script = $("jsPDFAutoTableScript");
                if (script.length > 0)
                    return;
                $("<script/>")
                    .attr("type", "text/javascript")
                    .attr("id", "jsPDFAutoTableScript")
                    .attr("src", Q.resolveUrl("~/Scripts/jspdf.plugin.autotable.min.js"))
                    .appendTo(document.head);
            }
        })(PdfExportHelper = Common.PdfExportHelper || (Common.PdfExportHelper = {}));
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var ReportDialog = /** @class */ (function (_super) {
            __extends(ReportDialog, _super);
            function ReportDialog(options) {
                var _this = _super.call(this, options) || this;
                _this.updateInterface();
                _this.loadReport(_this.options.reportKey);
                return _this;
            }
            ReportDialog.prototype.getDialogButtons = function () {
                return null;
            };
            ReportDialog.prototype.createPropertyGrid = function () {
                this.propertyGrid && this.byId('PropertyGrid').html('').attr('class', '');
                this.propertyGrid = new Serenity.PropertyGrid(this.byId('PropertyGrid'), {
                    idPrefix: this.idPrefix,
                    useCategories: true,
                    items: this.report.Properties
                }).init(null);
            };
            ReportDialog.prototype.loadReport = function (reportKey) {
                var _this = this;
                Q.serviceCall({
                    url: Q.resolveUrl('~/Report/Retrieve'),
                    request: {
                        ReportKey: reportKey
                    },
                    onSuccess: function (response) {
                        _this.report = response;
                        _this.element.dialog().dialog('option', 'title', _this.report.Title);
                        _this.createPropertyGrid();
                        _this.propertyGrid.load(_this.report.InitialSettings || {});
                        _this.updateInterface();
                        _this.dialogOpen();
                    }
                });
            };
            ReportDialog.prototype.updateInterface = function () {
                this.toolbar.findButton('print-preview-button')
                    .toggle(this.report && !this.report.IsDataOnlyReport);
                this.toolbar.findButton('export-pdf-button')
                    .toggle(this.report && !this.report.IsDataOnlyReport);
                this.toolbar.findButton('export-xlsx-button')
                    .toggle(this.report && this.report.IsDataOnlyReport);
            };
            ReportDialog.prototype.executeReport = function (target, ext, download) {
                if (!this.validateForm()) {
                    return;
                }
                var opt = {};
                this.propertyGrid.save(opt);
                Common.ReportHelper.execute({
                    download: download,
                    reportKey: this.report.ReportKey,
                    extension: ext,
                    target: target,
                    params: opt
                });
            };
            ReportDialog.prototype.getToolbarButtons = function () {
                var _this = this;
                return [
                    {
                        title: 'Preview',
                        cssClass: 'print-preview-button',
                        onClick: function () { return _this.executeReport('_blank', null, false); }
                    },
                    {
                        title: 'PDF',
                        cssClass: 'export-pdf-button',
                        onClick: function () { return _this.executeReport('_blank', 'pdf', true); }
                    },
                    {
                        title: 'Excel',
                        cssClass: 'export-xlsx-button',
                        onClick: function () { return _this.executeReport('_blank', 'xlsx', true); }
                    }
                ];
            };
            return ReportDialog;
        }(Serenity.TemplatedDialog));
        Common.ReportDialog = ReportDialog;
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var ReportHelper;
        (function (ReportHelper) {
            function createToolButton(options) {
                return {
                    title: Q.coalesce(options.title, 'Report'),
                    cssClass: Q.coalesce(options.cssClass, 'print-button'),
                    icon: options.icon,
                    onClick: function () {
                        ReportHelper.execute(options);
                    }
                };
            }
            ReportHelper.createToolButton = createToolButton;
            function execute(options) {
                var opt = options.getParams ? options.getParams() : options.params;
                Q.postToUrl({
                    url: '~/Report/' + (options.download ? 'Download' : 'Render'),
                    params: {
                        key: options.reportKey,
                        ext: Q.coalesce(options.extension, 'pdf'),
                        opt: opt ? $.toJSON(opt) : ''
                    },
                    target: Q.coalesce(options.target, '_blank')
                });
            }
            ReportHelper.execute = execute;
        })(ReportHelper = Common.ReportHelper || (Common.ReportHelper = {}));
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var ReportPage = /** @class */ (function (_super) {
            __extends(ReportPage, _super);
            function ReportPage(element) {
                var _this = _super.call(this, element) || this;
                $('.report-link', element).click(function (e) { return _this.reportLinkClick(e); });
                $('div.line', element).click(function (e) { return _this.categoryClick(e); });
                new Serenity.QuickSearchInput($('.s-QuickSearchBar input', element), {
                    onSearch: function (field, text, done) {
                        _this.updateMatchFlags(text);
                        done(true);
                    }
                });
                return _this;
            }
            ReportPage.prototype.updateMatchFlags = function (text) {
                var liList = $('.report-list', this.element).find('li').removeClass('non-match');
                text = Q.trimToNull(text);
                if (!text) {
                    liList.children('ul').hide();
                    liList.show().removeClass('expanded');
                    return;
                }
                text = Select2.util.stripDiacritics(text).toUpperCase();
                var reportItems = liList.filter('.report-item');
                reportItems.each(function (ix, e) {
                    var x = $(e);
                    var title = Select2.util.stripDiacritics(Q.coalesce(x.text(), '').toUpperCase());
                    if (title.indexOf(text) < 0) {
                        x.addClass('non-match');
                    }
                });
                var matchingItems = reportItems.not('.non-match');
                var visibles = matchingItems.parents('li').add(matchingItems);
                var nonVisibles = liList.not(visibles);
                nonVisibles.hide().addClass('non-match');
                visibles.show();
                if (visibles.length <= 100) {
                    liList.children('ul').show();
                    liList.addClass('expanded');
                }
            };
            ReportPage.prototype.categoryClick = function (e) {
                var li = $(e.target).closest('li');
                if (li.hasClass('expanded')) {
                    li.find('ul').hide('fast');
                    li.removeClass('expanded');
                    li.find('li').removeClass('expanded');
                }
                else {
                    li.addClass('expanded');
                    li.children('ul').show('fast');
                    if (li.children('ul').children('li').length === 1 && !li.children('ul').children('li').hasClass('expanded')) {
                        li.children('ul').children('li').children('.line').click();
                    }
                }
            };
            ReportPage.prototype.reportLinkClick = function (e) {
                e.preventDefault();
                new Common.ReportDialog({
                    reportKey: $(e.target).data('key')
                }).dialogOpen();
            };
            return ReportPage;
        }(Serenity.Widget));
        Common.ReportPage = ReportPage;
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Common;
    (function (Common) {
        var UserPreferenceStorage = /** @class */ (function () {
            function UserPreferenceStorage() {
            }
            UserPreferenceStorage.prototype.getItem = function (key) {
                var value;
                Common.UserPreferenceService.Retrieve({
                    PreferenceType: "UserPreferenceStorage",
                    Name: key
                }, function (response) { return value = response.Value; }, {
                    async: false
                });
                return value;
            };
            UserPreferenceStorage.prototype.setItem = function (key, data) {
                Common.UserPreferenceService.Update({
                    PreferenceType: "UserPreferenceStorage",
                    Name: key,
                    Value: data
                });
            };
            return UserPreferenceStorage;
        }());
        Common.UserPreferenceStorage = UserPreferenceStorage;
    })(Common = CntAppSeguridad.Common || (CntAppSeguridad.Common = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var AceptacionAuDialog = /** @class */ (function (_super) {
            __extends(AceptacionAuDialog, _super);
            function AceptacionAuDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Configuracion.AceptacionAuForm(_this.idPrefix);
                return _this;
            }
            AceptacionAuDialog.prototype.getFormKey = function () { return Configuracion.AceptacionAuForm.formKey; };
            AceptacionAuDialog.prototype.getIdProperty = function () { return Configuracion.AceptacionAuRow.idProperty; };
            AceptacionAuDialog.prototype.getLocalTextPrefix = function () { return Configuracion.AceptacionAuRow.localTextPrefix; };
            AceptacionAuDialog.prototype.getNameProperty = function () { return Configuracion.AceptacionAuRow.nameProperty; };
            AceptacionAuDialog.prototype.getService = function () { return Configuracion.AceptacionAuService.baseUrl; };
            AceptacionAuDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AceptacionAuDialog);
            return AceptacionAuDialog;
        }(Serenity.EntityDialog));
        Configuracion.AceptacionAuDialog = AceptacionAuDialog;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var AceptacionAuGrid = /** @class */ (function (_super) {
            __extends(AceptacionAuGrid, _super);
            function AceptacionAuGrid(container) {
                return _super.call(this, container) || this;
            }
            AceptacionAuGrid.prototype.getColumnsKey = function () { return 'Configuracion.AceptacionAu'; };
            AceptacionAuGrid.prototype.getDialogType = function () { return Configuracion.AceptacionAuDialog; };
            AceptacionAuGrid.prototype.getIdProperty = function () { return Configuracion.AceptacionAuRow.idProperty; };
            AceptacionAuGrid.prototype.getLocalTextPrefix = function () { return Configuracion.AceptacionAuRow.localTextPrefix; };
            AceptacionAuGrid.prototype.getService = function () { return Configuracion.AceptacionAuService.baseUrl; };
            AceptacionAuGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AceptacionAuGrid);
            return AceptacionAuGrid;
        }(Serenity.EntityGrid));
        Configuracion.AceptacionAuGrid = AceptacionAuGrid;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var AnalistaDialog = /** @class */ (function (_super) {
            __extends(AnalistaDialog, _super);
            function AnalistaDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Configuracion.AnalistaForm(_this.idPrefix);
                return _this;
            }
            AnalistaDialog.prototype.getFormKey = function () { return Configuracion.AnalistaForm.formKey; };
            AnalistaDialog.prototype.getIdProperty = function () { return Configuracion.AnalistaRow.idProperty; };
            AnalistaDialog.prototype.getLocalTextPrefix = function () { return Configuracion.AnalistaRow.localTextPrefix; };
            AnalistaDialog.prototype.getNameProperty = function () { return Configuracion.AnalistaRow.nameProperty; };
            AnalistaDialog.prototype.getService = function () { return Configuracion.AnalistaService.baseUrl; };
            AnalistaDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AnalistaDialog);
            return AnalistaDialog;
        }(Serenity.EntityDialog));
        Configuracion.AnalistaDialog = AnalistaDialog;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var AnalistaGrid = /** @class */ (function (_super) {
            __extends(AnalistaGrid, _super);
            function AnalistaGrid(container) {
                return _super.call(this, container) || this;
            }
            AnalistaGrid.prototype.getColumnsKey = function () { return 'Configuracion.Analista'; };
            AnalistaGrid.prototype.getDialogType = function () { return Configuracion.AnalistaDialog; };
            AnalistaGrid.prototype.getIdProperty = function () { return Configuracion.AnalistaRow.idProperty; };
            AnalistaGrid.prototype.getLocalTextPrefix = function () { return Configuracion.AnalistaRow.localTextPrefix; };
            AnalistaGrid.prototype.getService = function () { return Configuracion.AnalistaService.baseUrl; };
            AnalistaGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AnalistaGrid);
            return AnalistaGrid;
        }(Serenity.EntityGrid));
        Configuracion.AnalistaGrid = AnalistaGrid;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var FuenteAuDialog = /** @class */ (function (_super) {
            __extends(FuenteAuDialog, _super);
            function FuenteAuDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Configuracion.FuenteAuForm(_this.idPrefix);
                return _this;
            }
            FuenteAuDialog.prototype.getFormKey = function () { return Configuracion.FuenteAuForm.formKey; };
            FuenteAuDialog.prototype.getIdProperty = function () { return Configuracion.FuenteAuRow.idProperty; };
            FuenteAuDialog.prototype.getLocalTextPrefix = function () { return Configuracion.FuenteAuRow.localTextPrefix; };
            FuenteAuDialog.prototype.getNameProperty = function () { return Configuracion.FuenteAuRow.nameProperty; };
            FuenteAuDialog.prototype.getService = function () { return Configuracion.FuenteAuService.baseUrl; };
            FuenteAuDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], FuenteAuDialog);
            return FuenteAuDialog;
        }(Serenity.EntityDialog));
        Configuracion.FuenteAuDialog = FuenteAuDialog;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var FuenteAuGrid = /** @class */ (function (_super) {
            __extends(FuenteAuGrid, _super);
            function FuenteAuGrid(container) {
                return _super.call(this, container) || this;
            }
            FuenteAuGrid.prototype.getColumnsKey = function () { return 'Configuracion.FuenteAu'; };
            FuenteAuGrid.prototype.getDialogType = function () { return Configuracion.FuenteAuDialog; };
            FuenteAuGrid.prototype.getIdProperty = function () { return Configuracion.FuenteAuRow.idProperty; };
            FuenteAuGrid.prototype.getLocalTextPrefix = function () { return Configuracion.FuenteAuRow.localTextPrefix; };
            FuenteAuGrid.prototype.getService = function () { return Configuracion.FuenteAuService.baseUrl; };
            FuenteAuGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], FuenteAuGrid);
            return FuenteAuGrid;
        }(Serenity.EntityGrid));
        Configuracion.FuenteAuGrid = FuenteAuGrid;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var GradoAuDialog = /** @class */ (function (_super) {
            __extends(GradoAuDialog, _super);
            function GradoAuDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Configuracion.GradoAuForm(_this.idPrefix);
                return _this;
            }
            GradoAuDialog.prototype.getFormKey = function () { return Configuracion.GradoAuForm.formKey; };
            GradoAuDialog.prototype.getIdProperty = function () { return Configuracion.GradoAuRow.idProperty; };
            GradoAuDialog.prototype.getLocalTextPrefix = function () { return Configuracion.GradoAuRow.localTextPrefix; };
            GradoAuDialog.prototype.getNameProperty = function () { return Configuracion.GradoAuRow.nameProperty; };
            GradoAuDialog.prototype.getService = function () { return Configuracion.GradoAuService.baseUrl; };
            GradoAuDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], GradoAuDialog);
            return GradoAuDialog;
        }(Serenity.EntityDialog));
        Configuracion.GradoAuDialog = GradoAuDialog;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var GradoAuGrid = /** @class */ (function (_super) {
            __extends(GradoAuGrid, _super);
            function GradoAuGrid(container) {
                return _super.call(this, container) || this;
            }
            GradoAuGrid.prototype.getColumnsKey = function () { return 'Configuracion.GradoAu'; };
            GradoAuGrid.prototype.getDialogType = function () { return Configuracion.GradoAuDialog; };
            GradoAuGrid.prototype.getIdProperty = function () { return Configuracion.GradoAuRow.idProperty; };
            GradoAuGrid.prototype.getLocalTextPrefix = function () { return Configuracion.GradoAuRow.localTextPrefix; };
            GradoAuGrid.prototype.getService = function () { return Configuracion.GradoAuService.baseUrl; };
            GradoAuGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], GradoAuGrid);
            return GradoAuGrid;
        }(Serenity.EntityGrid));
        Configuracion.GradoAuGrid = GradoAuGrid;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var TipoAuDialog = /** @class */ (function (_super) {
            __extends(TipoAuDialog, _super);
            function TipoAuDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Configuracion.TipoAuForm(_this.idPrefix);
                return _this;
            }
            TipoAuDialog.prototype.getFormKey = function () { return Configuracion.TipoAuForm.formKey; };
            TipoAuDialog.prototype.getIdProperty = function () { return Configuracion.TipoAuRow.idProperty; };
            TipoAuDialog.prototype.getLocalTextPrefix = function () { return Configuracion.TipoAuRow.localTextPrefix; };
            TipoAuDialog.prototype.getNameProperty = function () { return Configuracion.TipoAuRow.nameProperty; };
            TipoAuDialog.prototype.getService = function () { return Configuracion.TipoAuService.baseUrl; };
            TipoAuDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], TipoAuDialog);
            return TipoAuDialog;
        }(Serenity.EntityDialog));
        Configuracion.TipoAuDialog = TipoAuDialog;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var TipoAuGrid = /** @class */ (function (_super) {
            __extends(TipoAuGrid, _super);
            function TipoAuGrid(container) {
                return _super.call(this, container) || this;
            }
            TipoAuGrid.prototype.getColumnsKey = function () { return 'Configuracion.TipoAu'; };
            TipoAuGrid.prototype.getDialogType = function () { return Configuracion.TipoAuDialog; };
            TipoAuGrid.prototype.getIdProperty = function () { return Configuracion.TipoAuRow.idProperty; };
            TipoAuGrid.prototype.getLocalTextPrefix = function () { return Configuracion.TipoAuRow.localTextPrefix; };
            TipoAuGrid.prototype.getService = function () { return Configuracion.TipoAuService.baseUrl; };
            TipoAuGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TipoAuGrid);
            return TipoAuGrid;
        }(Serenity.EntityGrid));
        Configuracion.TipoAuGrid = TipoAuGrid;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var TipoOficioDialog = /** @class */ (function (_super) {
            __extends(TipoOficioDialog, _super);
            function TipoOficioDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Configuracion.TipoOficioForm(_this.idPrefix);
                return _this;
            }
            TipoOficioDialog.prototype.getFormKey = function () { return Configuracion.TipoOficioForm.formKey; };
            TipoOficioDialog.prototype.getIdProperty = function () { return Configuracion.TipoOficioRow.idProperty; };
            TipoOficioDialog.prototype.getLocalTextPrefix = function () { return Configuracion.TipoOficioRow.localTextPrefix; };
            TipoOficioDialog.prototype.getNameProperty = function () { return Configuracion.TipoOficioRow.nameProperty; };
            TipoOficioDialog.prototype.getService = function () { return Configuracion.TipoOficioService.baseUrl; };
            TipoOficioDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], TipoOficioDialog);
            return TipoOficioDialog;
        }(Serenity.EntityDialog));
        Configuracion.TipoOficioDialog = TipoOficioDialog;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Configuracion;
    (function (Configuracion) {
        var TipoOficioGrid = /** @class */ (function (_super) {
            __extends(TipoOficioGrid, _super);
            function TipoOficioGrid(container) {
                return _super.call(this, container) || this;
            }
            TipoOficioGrid.prototype.getColumnsKey = function () { return 'Configuracion.TipoOficio'; };
            TipoOficioGrid.prototype.getDialogType = function () { return Configuracion.TipoOficioDialog; };
            TipoOficioGrid.prototype.getIdProperty = function () { return Configuracion.TipoOficioRow.idProperty; };
            TipoOficioGrid.prototype.getLocalTextPrefix = function () { return Configuracion.TipoOficioRow.localTextPrefix; };
            TipoOficioGrid.prototype.getService = function () { return Configuracion.TipoOficioService.baseUrl; };
            TipoOficioGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TipoOficioGrid);
            return TipoOficioGrid;
        }(Serenity.EntityGrid));
        Configuracion.TipoOficioGrid = TipoOficioGrid;
    })(Configuracion = CntAppSeguridad.Configuracion || (CntAppSeguridad.Configuracion = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var ActaBienDialog = /** @class */ (function (_super) {
            __extends(ActaBienDialog, _super);
            function ActaBienDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Documentos.ActaBienForm(_this.idPrefix);
                return _this;
            }
            ActaBienDialog.prototype.getFormKey = function () { return Documentos.ActaBienForm.formKey; };
            ActaBienDialog.prototype.getIdProperty = function () { return Documentos.ActaBienRow.idProperty; };
            ActaBienDialog.prototype.getLocalTextPrefix = function () { return Documentos.ActaBienRow.localTextPrefix; };
            ActaBienDialog.prototype.getNameProperty = function () { return Documentos.ActaBienRow.nameProperty; };
            ActaBienDialog.prototype.getService = function () { return Documentos.ActaBienService.baseUrl; };
            ActaBienDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], ActaBienDialog);
            return ActaBienDialog;
        }(Serenity.EntityDialog));
        Documentos.ActaBienDialog = ActaBienDialog;
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var ActaBienGrid = /** @class */ (function (_super) {
            __extends(ActaBienGrid, _super);
            function ActaBienGrid(container) {
                return _super.call(this, container) || this;
            }
            ActaBienGrid.prototype.getColumnsKey = function () { return 'Documentos.ActaBien'; };
            ActaBienGrid.prototype.getDialogType = function () { return Documentos.ActaBienDialog; };
            ActaBienGrid.prototype.getIdProperty = function () { return Documentos.ActaBienRow.idProperty; };
            ActaBienGrid.prototype.getLocalTextPrefix = function () { return Documentos.ActaBienRow.localTextPrefix; };
            ActaBienGrid.prototype.getService = function () { return Documentos.ActaBienService.baseUrl; };
            ActaBienGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], ActaBienGrid);
            return ActaBienGrid;
        }(Serenity.EntityGrid));
        Documentos.ActaBienGrid = ActaBienGrid;
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var ActaReunionDialog = /** @class */ (function (_super) {
            __extends(ActaReunionDialog, _super);
            function ActaReunionDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Documentos.ActaReunionForm(_this.idPrefix);
                return _this;
            }
            ActaReunionDialog.prototype.getFormKey = function () { return Documentos.ActaReunionForm.formKey; };
            ActaReunionDialog.prototype.getIdProperty = function () { return Documentos.ActaReunionRow.idProperty; };
            ActaReunionDialog.prototype.getLocalTextPrefix = function () { return Documentos.ActaReunionRow.localTextPrefix; };
            ActaReunionDialog.prototype.getNameProperty = function () { return Documentos.ActaReunionRow.nameProperty; };
            ActaReunionDialog.prototype.getService = function () { return Documentos.ActaReunionService.baseUrl; };
            ActaReunionDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], ActaReunionDialog);
            return ActaReunionDialog;
        }(Serenity.EntityDialog));
        Documentos.ActaReunionDialog = ActaReunionDialog;
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var ActaReunionGrid = /** @class */ (function (_super) {
            __extends(ActaReunionGrid, _super);
            function ActaReunionGrid(container) {
                return _super.call(this, container) || this;
            }
            ActaReunionGrid.prototype.getColumnsKey = function () { return 'Documentos.ActaReunion'; };
            ActaReunionGrid.prototype.getDialogType = function () { return Documentos.ActaReunionDialog; };
            ActaReunionGrid.prototype.getIdProperty = function () { return Documentos.ActaReunionRow.idProperty; };
            ActaReunionGrid.prototype.getLocalTextPrefix = function () { return Documentos.ActaReunionRow.localTextPrefix; };
            ActaReunionGrid.prototype.getService = function () { return Documentos.ActaReunionService.baseUrl; };
            ActaReunionGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], ActaReunionGrid);
            return ActaReunionGrid;
        }(Serenity.EntityGrid));
        Documentos.ActaReunionGrid = ActaReunionGrid;
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeMensualDialog = /** @class */ (function (_super) {
            __extends(InformeMensualDialog, _super);
            function InformeMensualDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Documentos.InformeMensualForm(_this.idPrefix);
                return _this;
            }
            InformeMensualDialog.prototype.getFormKey = function () { return Documentos.InformeMensualForm.formKey; };
            InformeMensualDialog.prototype.getIdProperty = function () { return Documentos.InformeMensualRow.idProperty; };
            InformeMensualDialog.prototype.getLocalTextPrefix = function () { return Documentos.InformeMensualRow.localTextPrefix; };
            InformeMensualDialog.prototype.getNameProperty = function () { return Documentos.InformeMensualRow.nameProperty; };
            InformeMensualDialog.prototype.getService = function () { return Documentos.InformeMensualService.baseUrl; };
            InformeMensualDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], InformeMensualDialog);
            return InformeMensualDialog;
        }(Serenity.EntityDialog));
        Documentos.InformeMensualDialog = InformeMensualDialog;
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeMensualGrid = /** @class */ (function (_super) {
            __extends(InformeMensualGrid, _super);
            function InformeMensualGrid(container) {
                return _super.call(this, container) || this;
            }
            InformeMensualGrid.prototype.getColumnsKey = function () { return 'Documentos.InformeMensual'; };
            InformeMensualGrid.prototype.getDialogType = function () { return Documentos.InformeMensualDialog; };
            InformeMensualGrid.prototype.getIdProperty = function () { return Documentos.InformeMensualRow.idProperty; };
            InformeMensualGrid.prototype.getLocalTextPrefix = function () { return Documentos.InformeMensualRow.localTextPrefix; };
            InformeMensualGrid.prototype.getService = function () { return Documentos.InformeMensualService.baseUrl; };
            InformeMensualGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], InformeMensualGrid);
            return InformeMensualGrid;
        }(Serenity.EntityGrid));
        Documentos.InformeMensualGrid = InformeMensualGrid;
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeTecnicoDialog = /** @class */ (function (_super) {
            __extends(InformeTecnicoDialog, _super);
            function InformeTecnicoDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Documentos.InformeTecnicoForm(_this.idPrefix);
                return _this;
            }
            InformeTecnicoDialog.prototype.getFormKey = function () { return Documentos.InformeTecnicoForm.formKey; };
            InformeTecnicoDialog.prototype.getIdProperty = function () { return Documentos.InformeTecnicoRow.idProperty; };
            InformeTecnicoDialog.prototype.getLocalTextPrefix = function () { return Documentos.InformeTecnicoRow.localTextPrefix; };
            InformeTecnicoDialog.prototype.getNameProperty = function () { return Documentos.InformeTecnicoRow.nameProperty; };
            InformeTecnicoDialog.prototype.getService = function () { return Documentos.InformeTecnicoService.baseUrl; };
            InformeTecnicoDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], InformeTecnicoDialog);
            return InformeTecnicoDialog;
        }(Serenity.EntityDialog));
        Documentos.InformeTecnicoDialog = InformeTecnicoDialog;
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Documentos;
    (function (Documentos) {
        var InformeTecnicoGrid = /** @class */ (function (_super) {
            __extends(InformeTecnicoGrid, _super);
            function InformeTecnicoGrid(container) {
                return _super.call(this, container) || this;
            }
            InformeTecnicoGrid.prototype.getColumnsKey = function () { return 'Documentos.InformeTecnico'; };
            InformeTecnicoGrid.prototype.getDialogType = function () { return Documentos.InformeTecnicoDialog; };
            InformeTecnicoGrid.prototype.getIdProperty = function () { return Documentos.InformeTecnicoRow.idProperty; };
            InformeTecnicoGrid.prototype.getLocalTextPrefix = function () { return Documentos.InformeTecnicoRow.localTextPrefix; };
            InformeTecnicoGrid.prototype.getService = function () { return Documentos.InformeTecnicoService.baseUrl; };
            InformeTecnicoGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], InformeTecnicoGrid);
            return InformeTecnicoGrid;
        }(Serenity.EntityGrid));
        Documentos.InformeTecnicoGrid = InformeTecnicoGrid;
    })(Documentos = CntAppSeguridad.Documentos || (CntAppSeguridad.Documentos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var AreaDialog = /** @class */ (function (_super) {
            __extends(AreaDialog, _super);
            function AreaDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Oficios.AreaForm(_this.idPrefix);
                return _this;
            }
            AreaDialog.prototype.getFormKey = function () { return Oficios.AreaForm.formKey; };
            AreaDialog.prototype.getIdProperty = function () { return Oficios.AreaRow.idProperty; };
            AreaDialog.prototype.getLocalTextPrefix = function () { return Oficios.AreaRow.localTextPrefix; };
            AreaDialog.prototype.getNameProperty = function () { return Oficios.AreaRow.nameProperty; };
            AreaDialog.prototype.getService = function () { return Oficios.AreaService.baseUrl; };
            AreaDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], AreaDialog);
            return AreaDialog;
        }(Serenity.EntityDialog));
        Oficios.AreaDialog = AreaDialog;
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var AreaGrid = /** @class */ (function (_super) {
            __extends(AreaGrid, _super);
            function AreaGrid(container) {
                return _super.call(this, container) || this;
            }
            AreaGrid.prototype.getColumnsKey = function () { return 'Oficios.Area'; };
            AreaGrid.prototype.getDialogType = function () { return Oficios.AreaDialog; };
            AreaGrid.prototype.getIdProperty = function () { return Oficios.AreaRow.idProperty; };
            AreaGrid.prototype.getLocalTextPrefix = function () { return Oficios.AreaRow.localTextPrefix; };
            AreaGrid.prototype.getService = function () { return Oficios.AreaService.baseUrl; };
            AreaGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], AreaGrid);
            return AreaGrid;
        }(Serenity.EntityGrid));
        Oficios.AreaGrid = AreaGrid;
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var OficioDialog = /** @class */ (function (_super) {
            __extends(OficioDialog, _super);
            function OficioDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Oficios.OficioForm(_this.idPrefix);
                return _this;
            }
            OficioDialog.prototype.getFormKey = function () { return Oficios.OficioForm.formKey; };
            OficioDialog.prototype.getIdProperty = function () { return Oficios.OficioRow.idProperty; };
            OficioDialog.prototype.getLocalTextPrefix = function () { return Oficios.OficioRow.localTextPrefix; };
            OficioDialog.prototype.getNameProperty = function () { return Oficios.OficioRow.nameProperty; };
            OficioDialog.prototype.getService = function () { return Oficios.OficioService.baseUrl; };
            OficioDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], OficioDialog);
            return OficioDialog;
        }(Serenity.EntityDialog));
        Oficios.OficioDialog = OficioDialog;
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var OficioGrid = /** @class */ (function (_super) {
            __extends(OficioGrid, _super);
            function OficioGrid(container) {
                return _super.call(this, container) || this;
            }
            OficioGrid.prototype.getColumnsKey = function () { return 'Oficios.Oficio'; };
            OficioGrid.prototype.getDialogType = function () { return Oficios.OficioDialog; };
            OficioGrid.prototype.getIdProperty = function () { return Oficios.OficioRow.idProperty; };
            OficioGrid.prototype.getLocalTextPrefix = function () { return Oficios.OficioRow.localTextPrefix; };
            OficioGrid.prototype.getService = function () { return Oficios.OficioService.baseUrl; };
            OficioGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], OficioGrid);
            return OficioGrid;
        }(Serenity.EntityGrid));
        Oficios.OficioGrid = OficioGrid;
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var OficioSiDialog = /** @class */ (function (_super) {
            __extends(OficioSiDialog, _super);
            function OficioSiDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Oficios.OficioSiForm(_this.idPrefix);
                return _this;
            }
            OficioSiDialog.prototype.getFormKey = function () { return Oficios.OficioSiForm.formKey; };
            OficioSiDialog.prototype.getIdProperty = function () { return Oficios.OficioSiRow.idProperty; };
            OficioSiDialog.prototype.getLocalTextPrefix = function () { return Oficios.OficioSiRow.localTextPrefix; };
            OficioSiDialog.prototype.getNameProperty = function () { return Oficios.OficioSiRow.nameProperty; };
            OficioSiDialog.prototype.getService = function () { return Oficios.OficioSiService.baseUrl; };
            OficioSiDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], OficioSiDialog);
            return OficioSiDialog;
        }(Serenity.EntityDialog));
        Oficios.OficioSiDialog = OficioSiDialog;
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var OficioSiGrid = /** @class */ (function (_super) {
            __extends(OficioSiGrid, _super);
            function OficioSiGrid(container) {
                return _super.call(this, container) || this;
            }
            OficioSiGrid.prototype.getColumnsKey = function () { return 'Oficios.OficioSi'; };
            OficioSiGrid.prototype.getDialogType = function () { return Oficios.OficioSiDialog; };
            OficioSiGrid.prototype.getIdProperty = function () { return Oficios.OficioSiRow.idProperty; };
            OficioSiGrid.prototype.getLocalTextPrefix = function () { return Oficios.OficioSiRow.localTextPrefix; };
            OficioSiGrid.prototype.getService = function () { return Oficios.OficioSiService.baseUrl; };
            OficioSiGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], OficioSiGrid);
            return OficioSiGrid;
        }(Serenity.EntityGrid));
        Oficios.OficioSiGrid = OficioSiGrid;
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var ResponsableDialog = /** @class */ (function (_super) {
            __extends(ResponsableDialog, _super);
            function ResponsableDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Oficios.ResponsableForm(_this.idPrefix);
                return _this;
            }
            ResponsableDialog.prototype.getFormKey = function () { return Oficios.ResponsableForm.formKey; };
            ResponsableDialog.prototype.getIdProperty = function () { return Oficios.ResponsableRow.idProperty; };
            ResponsableDialog.prototype.getLocalTextPrefix = function () { return Oficios.ResponsableRow.localTextPrefix; };
            ResponsableDialog.prototype.getNameProperty = function () { return Oficios.ResponsableRow.nameProperty; };
            ResponsableDialog.prototype.getService = function () { return Oficios.ResponsableService.baseUrl; };
            ResponsableDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], ResponsableDialog);
            return ResponsableDialog;
        }(Serenity.EntityDialog));
        Oficios.ResponsableDialog = ResponsableDialog;
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Oficios;
    (function (Oficios) {
        var ResponsableGrid = /** @class */ (function (_super) {
            __extends(ResponsableGrid, _super);
            function ResponsableGrid(container) {
                return _super.call(this, container) || this;
            }
            ResponsableGrid.prototype.getColumnsKey = function () { return 'Oficios.Responsable'; };
            ResponsableGrid.prototype.getDialogType = function () { return Oficios.ResponsableDialog; };
            ResponsableGrid.prototype.getIdProperty = function () { return Oficios.ResponsableRow.idProperty; };
            ResponsableGrid.prototype.getLocalTextPrefix = function () { return Oficios.ResponsableRow.localTextPrefix; };
            ResponsableGrid.prototype.getService = function () { return Oficios.ResponsableService.baseUrl; };
            ResponsableGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], ResponsableGrid);
            return ResponsableGrid;
        }(Serenity.EntityGrid));
        Oficios.ResponsableGrid = ResponsableGrid;
    })(Oficios = CntAppSeguridad.Oficios || (CntAppSeguridad.Oficios = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PlanAnualSI;
    (function (PlanAnualSI) {
        var PlanAnualDialog = /** @class */ (function (_super) {
            __extends(PlanAnualDialog, _super);
            function PlanAnualDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new PlanAnualSI.PlanAnualForm(_this.idPrefix);
                return _this;
            }
            PlanAnualDialog.prototype.getFormKey = function () { return PlanAnualSI.PlanAnualForm.formKey; };
            PlanAnualDialog.prototype.getIdProperty = function () { return PlanAnualSI.PlanAnualRow.idProperty; };
            PlanAnualDialog.prototype.getLocalTextPrefix = function () { return PlanAnualSI.PlanAnualRow.localTextPrefix; };
            PlanAnualDialog.prototype.getNameProperty = function () { return PlanAnualSI.PlanAnualRow.nameProperty; };
            PlanAnualDialog.prototype.getService = function () { return PlanAnualSI.PlanAnualService.baseUrl; };
            PlanAnualDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], PlanAnualDialog);
            return PlanAnualDialog;
        }(Serenity.EntityDialog));
        PlanAnualSI.PlanAnualDialog = PlanAnualDialog;
    })(PlanAnualSI = CntAppSeguridad.PlanAnualSI || (CntAppSeguridad.PlanAnualSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var PlanAnualSI;
    (function (PlanAnualSI) {
        var PlanAnualGrid = /** @class */ (function (_super) {
            __extends(PlanAnualGrid, _super);
            function PlanAnualGrid(container) {
                return _super.call(this, container) || this;
            }
            PlanAnualGrid.prototype.getColumnsKey = function () { return 'PlanAnualSI.PlanAnual'; };
            PlanAnualGrid.prototype.getDialogType = function () { return PlanAnualSI.PlanAnualDialog; };
            PlanAnualGrid.prototype.getIdProperty = function () { return PlanAnualSI.PlanAnualRow.idProperty; };
            PlanAnualGrid.prototype.getLocalTextPrefix = function () { return PlanAnualSI.PlanAnualRow.localTextPrefix; };
            PlanAnualGrid.prototype.getService = function () { return PlanAnualSI.PlanAnualService.baseUrl; };
            PlanAnualGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], PlanAnualGrid);
            return PlanAnualGrid;
        }(Serenity.EntityGrid));
        PlanAnualSI.PlanAnualGrid = PlanAnualGrid;
    })(PlanAnualSI = CntAppSeguridad.PlanAnualSI || (CntAppSeguridad.PlanAnualSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var EstadoDialog = /** @class */ (function (_super) {
            __extends(EstadoDialog, _super);
            function EstadoDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Requerimientos.EstadoForm(_this.idPrefix);
                return _this;
            }
            EstadoDialog.prototype.getFormKey = function () { return Requerimientos.EstadoForm.formKey; };
            EstadoDialog.prototype.getIdProperty = function () { return Requerimientos.EstadoRow.idProperty; };
            EstadoDialog.prototype.getLocalTextPrefix = function () { return Requerimientos.EstadoRow.localTextPrefix; };
            EstadoDialog.prototype.getNameProperty = function () { return Requerimientos.EstadoRow.nameProperty; };
            EstadoDialog.prototype.getService = function () { return Requerimientos.EstadoService.baseUrl; };
            EstadoDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], EstadoDialog);
            return EstadoDialog;
        }(Serenity.EntityDialog));
        Requerimientos.EstadoDialog = EstadoDialog;
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var EstadoGrid = /** @class */ (function (_super) {
            __extends(EstadoGrid, _super);
            function EstadoGrid(container) {
                return _super.call(this, container) || this;
            }
            EstadoGrid.prototype.getColumnsKey = function () { return 'Requerimientos.Estado'; };
            EstadoGrid.prototype.getDialogType = function () { return Requerimientos.EstadoDialog; };
            EstadoGrid.prototype.getIdProperty = function () { return Requerimientos.EstadoRow.idProperty; };
            EstadoGrid.prototype.getLocalTextPrefix = function () { return Requerimientos.EstadoRow.localTextPrefix; };
            EstadoGrid.prototype.getService = function () { return Requerimientos.EstadoService.baseUrl; };
            EstadoGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], EstadoGrid);
            return EstadoGrid;
        }(Serenity.EntityGrid));
        Requerimientos.EstadoGrid = EstadoGrid;
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var IncidenteDialog = /** @class */ (function (_super) {
            __extends(IncidenteDialog, _super);
            function IncidenteDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Requerimientos.IncidenteForm(_this.idPrefix);
                return _this;
            }
            IncidenteDialog.prototype.getFormKey = function () { return Requerimientos.IncidenteForm.formKey; };
            IncidenteDialog.prototype.getIdProperty = function () { return Requerimientos.IncidenteRow.idProperty; };
            IncidenteDialog.prototype.getLocalTextPrefix = function () { return Requerimientos.IncidenteRow.localTextPrefix; };
            IncidenteDialog.prototype.getNameProperty = function () { return Requerimientos.IncidenteRow.nameProperty; };
            IncidenteDialog.prototype.getService = function () { return Requerimientos.IncidenteService.baseUrl; };
            IncidenteDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], IncidenteDialog);
            return IncidenteDialog;
        }(Serenity.EntityDialog));
        Requerimientos.IncidenteDialog = IncidenteDialog;
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var IncidenteGrid = /** @class */ (function (_super) {
            __extends(IncidenteGrid, _super);
            function IncidenteGrid(container) {
                return _super.call(this, container) || this;
            }
            IncidenteGrid.prototype.getColumnsKey = function () { return 'Requerimientos.Incidente'; };
            IncidenteGrid.prototype.getDialogType = function () { return Requerimientos.IncidenteDialog; };
            IncidenteGrid.prototype.getIdProperty = function () { return Requerimientos.IncidenteRow.idProperty; };
            IncidenteGrid.prototype.getLocalTextPrefix = function () { return Requerimientos.IncidenteRow.localTextPrefix; };
            IncidenteGrid.prototype.getService = function () { return Requerimientos.IncidenteService.baseUrl; };
            IncidenteGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], IncidenteGrid);
            return IncidenteGrid;
        }(Serenity.EntityGrid));
        Requerimientos.IncidenteGrid = IncidenteGrid;
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var PrioridadDialog = /** @class */ (function (_super) {
            __extends(PrioridadDialog, _super);
            function PrioridadDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Requerimientos.PrioridadForm(_this.idPrefix);
                return _this;
            }
            PrioridadDialog.prototype.getFormKey = function () { return Requerimientos.PrioridadForm.formKey; };
            PrioridadDialog.prototype.getIdProperty = function () { return Requerimientos.PrioridadRow.idProperty; };
            PrioridadDialog.prototype.getLocalTextPrefix = function () { return Requerimientos.PrioridadRow.localTextPrefix; };
            PrioridadDialog.prototype.getNameProperty = function () { return Requerimientos.PrioridadRow.nameProperty; };
            PrioridadDialog.prototype.getService = function () { return Requerimientos.PrioridadService.baseUrl; };
            PrioridadDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], PrioridadDialog);
            return PrioridadDialog;
        }(Serenity.EntityDialog));
        Requerimientos.PrioridadDialog = PrioridadDialog;
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var PrioridadGrid = /** @class */ (function (_super) {
            __extends(PrioridadGrid, _super);
            function PrioridadGrid(container) {
                return _super.call(this, container) || this;
            }
            PrioridadGrid.prototype.getColumnsKey = function () { return 'Requerimientos.Prioridad'; };
            PrioridadGrid.prototype.getDialogType = function () { return Requerimientos.PrioridadDialog; };
            PrioridadGrid.prototype.getIdProperty = function () { return Requerimientos.PrioridadRow.idProperty; };
            PrioridadGrid.prototype.getLocalTextPrefix = function () { return Requerimientos.PrioridadRow.localTextPrefix; };
            PrioridadGrid.prototype.getService = function () { return Requerimientos.PrioridadService.baseUrl; };
            PrioridadGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], PrioridadGrid);
            return PrioridadGrid;
        }(Serenity.EntityGrid));
        Requerimientos.PrioridadGrid = PrioridadGrid;
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var TicketDialog = /** @class */ (function (_super) {
            __extends(TicketDialog, _super);
            function TicketDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Requerimientos.TicketForm(_this.idPrefix);
                return _this;
            }
            TicketDialog.prototype.getFormKey = function () { return Requerimientos.TicketForm.formKey; };
            TicketDialog.prototype.getIdProperty = function () { return Requerimientos.TicketRow.idProperty; };
            TicketDialog.prototype.getLocalTextPrefix = function () { return Requerimientos.TicketRow.localTextPrefix; };
            TicketDialog.prototype.getNameProperty = function () { return Requerimientos.TicketRow.nameProperty; };
            TicketDialog.prototype.getService = function () { return Requerimientos.TicketService.baseUrl; };
            TicketDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], TicketDialog);
            return TicketDialog;
        }(Serenity.EntityDialog));
        Requerimientos.TicketDialog = TicketDialog;
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Requerimientos;
    (function (Requerimientos) {
        var TicketGrid = /** @class */ (function (_super) {
            __extends(TicketGrid, _super);
            function TicketGrid(container) {
                return _super.call(this, container) || this;
            }
            TicketGrid.prototype.getColumnsKey = function () { return 'Requerimientos.Ticket'; };
            TicketGrid.prototype.getDialogType = function () { return Requerimientos.TicketDialog; };
            TicketGrid.prototype.getIdProperty = function () { return Requerimientos.TicketRow.idProperty; };
            TicketGrid.prototype.getLocalTextPrefix = function () { return Requerimientos.TicketRow.localTextPrefix; };
            TicketGrid.prototype.getService = function () { return Requerimientos.TicketService.baseUrl; };
            TicketGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TicketGrid);
            return TicketGrid;
        }(Serenity.EntityGrid));
        Requerimientos.TicketGrid = TicketGrid;
    })(Requerimientos = CntAppSeguridad.Requerimientos || (CntAppSeguridad.Requerimientos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Riesgos;
    (function (Riesgos) {
        var LogsDialog = /** @class */ (function (_super) {
            __extends(LogsDialog, _super);
            function LogsDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Riesgos.LogsForm(_this.idPrefix);
                return _this;
            }
            LogsDialog.prototype.getFormKey = function () { return Riesgos.LogsForm.formKey; };
            LogsDialog.prototype.getIdProperty = function () { return Riesgos.LogsRow.idProperty; };
            LogsDialog.prototype.getLocalTextPrefix = function () { return Riesgos.LogsRow.localTextPrefix; };
            LogsDialog.prototype.getNameProperty = function () { return Riesgos.LogsRow.nameProperty; };
            LogsDialog.prototype.getService = function () { return Riesgos.LogsService.baseUrl; };
            LogsDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], LogsDialog);
            return LogsDialog;
        }(Serenity.EntityDialog));
        Riesgos.LogsDialog = LogsDialog;
    })(Riesgos = CntAppSeguridad.Riesgos || (CntAppSeguridad.Riesgos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Riesgos;
    (function (Riesgos) {
        var LogsGrid = /** @class */ (function (_super) {
            __extends(LogsGrid, _super);
            function LogsGrid(container) {
                return _super.call(this, container) || this;
            }
            LogsGrid.prototype.getColumnsKey = function () { return 'Riesgos.Logs'; };
            LogsGrid.prototype.getDialogType = function () { return Riesgos.LogsDialog; };
            LogsGrid.prototype.getIdProperty = function () { return Riesgos.LogsRow.idProperty; };
            LogsGrid.prototype.getLocalTextPrefix = function () { return Riesgos.LogsRow.localTextPrefix; };
            LogsGrid.prototype.getService = function () { return Riesgos.LogsService.baseUrl; };
            LogsGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], LogsGrid);
            return LogsGrid;
        }(Serenity.EntityGrid));
        Riesgos.LogsGrid = LogsGrid;
    })(Riesgos = CntAppSeguridad.Riesgos || (CntAppSeguridad.Riesgos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Riesgos;
    (function (Riesgos) {
        var PerfilesDialog = /** @class */ (function (_super) {
            __extends(PerfilesDialog, _super);
            function PerfilesDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new Riesgos.PerfilesForm(_this.idPrefix);
                return _this;
            }
            PerfilesDialog.prototype.getFormKey = function () { return Riesgos.PerfilesForm.formKey; };
            PerfilesDialog.prototype.getIdProperty = function () { return Riesgos.PerfilesRow.idProperty; };
            PerfilesDialog.prototype.getLocalTextPrefix = function () { return Riesgos.PerfilesRow.localTextPrefix; };
            PerfilesDialog.prototype.getNameProperty = function () { return Riesgos.PerfilesRow.nameProperty; };
            PerfilesDialog.prototype.getService = function () { return Riesgos.PerfilesService.baseUrl; };
            PerfilesDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], PerfilesDialog);
            return PerfilesDialog;
        }(Serenity.EntityDialog));
        Riesgos.PerfilesDialog = PerfilesDialog;
    })(Riesgos = CntAppSeguridad.Riesgos || (CntAppSeguridad.Riesgos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Riesgos;
    (function (Riesgos) {
        var PerfilesGrid = /** @class */ (function (_super) {
            __extends(PerfilesGrid, _super);
            function PerfilesGrid(container) {
                return _super.call(this, container) || this;
            }
            PerfilesGrid.prototype.getColumnsKey = function () { return 'Riesgos.Perfiles'; };
            PerfilesGrid.prototype.getDialogType = function () { return Riesgos.PerfilesDialog; };
            PerfilesGrid.prototype.getIdProperty = function () { return Riesgos.PerfilesRow.idProperty; };
            PerfilesGrid.prototype.getLocalTextPrefix = function () { return Riesgos.PerfilesRow.localTextPrefix; };
            PerfilesGrid.prototype.getService = function () { return Riesgos.PerfilesService.baseUrl; };
            PerfilesGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], PerfilesGrid);
            return PerfilesGrid;
        }(Serenity.EntityGrid));
        Riesgos.PerfilesGrid = PerfilesGrid;
    })(Riesgos = CntAppSeguridad.Riesgos || (CntAppSeguridad.Riesgos = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var TareasSI;
    (function (TareasSI) {
        var IndicadorTDialog = /** @class */ (function (_super) {
            __extends(IndicadorTDialog, _super);
            function IndicadorTDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new TareasSI.IndicadorTForm(_this.idPrefix);
                return _this;
            }
            IndicadorTDialog.prototype.getFormKey = function () { return TareasSI.IndicadorTForm.formKey; };
            IndicadorTDialog.prototype.getIdProperty = function () { return TareasSI.IndicadorTRow.idProperty; };
            IndicadorTDialog.prototype.getLocalTextPrefix = function () { return TareasSI.IndicadorTRow.localTextPrefix; };
            IndicadorTDialog.prototype.getNameProperty = function () { return TareasSI.IndicadorTRow.nameProperty; };
            IndicadorTDialog.prototype.getService = function () { return TareasSI.IndicadorTService.baseUrl; };
            IndicadorTDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], IndicadorTDialog);
            return IndicadorTDialog;
        }(Serenity.EntityDialog));
        TareasSI.IndicadorTDialog = IndicadorTDialog;
    })(TareasSI = CntAppSeguridad.TareasSI || (CntAppSeguridad.TareasSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var TareasSI;
    (function (TareasSI) {
        var IndicadorTGrid = /** @class */ (function (_super) {
            __extends(IndicadorTGrid, _super);
            function IndicadorTGrid(container) {
                return _super.call(this, container) || this;
            }
            IndicadorTGrid.prototype.getColumnsKey = function () { return 'TareasSI.IndicadorT'; };
            IndicadorTGrid.prototype.getDialogType = function () { return TareasSI.IndicadorTDialog; };
            IndicadorTGrid.prototype.getIdProperty = function () { return TareasSI.IndicadorTRow.idProperty; };
            IndicadorTGrid.prototype.getLocalTextPrefix = function () { return TareasSI.IndicadorTRow.localTextPrefix; };
            IndicadorTGrid.prototype.getService = function () { return TareasSI.IndicadorTService.baseUrl; };
            IndicadorTGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], IndicadorTGrid);
            return IndicadorTGrid;
        }(Serenity.EntityGrid));
        TareasSI.IndicadorTGrid = IndicadorTGrid;
    })(TareasSI = CntAppSeguridad.TareasSI || (CntAppSeguridad.TareasSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var TareasSI;
    (function (TareasSI) {
        var TareasDialog = /** @class */ (function (_super) {
            __extends(TareasDialog, _super);
            function TareasDialog() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.form = new TareasSI.TareasForm(_this.idPrefix);
                return _this;
            }
            TareasDialog.prototype.getFormKey = function () { return TareasSI.TareasForm.formKey; };
            TareasDialog.prototype.getIdProperty = function () { return TareasSI.TareasRow.idProperty; };
            TareasDialog.prototype.getLocalTextPrefix = function () { return TareasSI.TareasRow.localTextPrefix; };
            TareasDialog.prototype.getNameProperty = function () { return TareasSI.TareasRow.nameProperty; };
            TareasDialog.prototype.getService = function () { return TareasSI.TareasService.baseUrl; };
            TareasDialog = __decorate([
                Serenity.Decorators.registerClass(),
                Serenity.Decorators.responsive()
            ], TareasDialog);
            return TareasDialog;
        }(Serenity.EntityDialog));
        TareasSI.TareasDialog = TareasDialog;
    })(TareasSI = CntAppSeguridad.TareasSI || (CntAppSeguridad.TareasSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var TareasSI;
    (function (TareasSI) {
        var TareasGrid = /** @class */ (function (_super) {
            __extends(TareasGrid, _super);
            function TareasGrid(container) {
                return _super.call(this, container) || this;
            }
            TareasGrid.prototype.getColumnsKey = function () { return 'TareasSI.Tareas'; };
            TareasGrid.prototype.getDialogType = function () { return TareasSI.TareasDialog; };
            TareasGrid.prototype.getIdProperty = function () { return TareasSI.TareasRow.idProperty; };
            TareasGrid.prototype.getLocalTextPrefix = function () { return TareasSI.TareasRow.localTextPrefix; };
            TareasGrid.prototype.getService = function () { return TareasSI.TareasService.baseUrl; };
            TareasGrid = __decorate([
                Serenity.Decorators.registerClass()
            ], TareasGrid);
            return TareasGrid;
        }(Serenity.EntityGrid));
        TareasSI.TareasGrid = TareasGrid;
    })(TareasSI = CntAppSeguridad.TareasSI || (CntAppSeguridad.TareasSI = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Authorization;
    (function (Authorization) {
        Object.defineProperty(Authorization, 'userDefinition', {
            get: function () {
                return Q.getRemoteData('UserData');
            }
        });
        function hasPermission(permissionKey) {
            var ud = Authorization.userDefinition;
            return ud.Username === 'admin' || !!ud.Permissions[permissionKey];
        }
        Authorization.hasPermission = hasPermission;
    })(Authorization = CntAppSeguridad.Authorization || (CntAppSeguridad.Authorization = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Membership;
    (function (Membership) {
        var ChangePasswordPanel = /** @class */ (function (_super) {
            __extends(ChangePasswordPanel, _super);
            function ChangePasswordPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.ChangePasswordForm(_this.idPrefix);
                _this.form.NewPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.w('ConfirmPassword', Serenity.PasswordEditor).value.length < 7) {
                        return Q.format(Q.text('Validation.MinRequiredPasswordLength'), 7);
                    }
                });
                _this.form.ConfirmPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value !== _this.form.NewPassword.value) {
                        return Q.text('Validation.PasswordConfirm');
                    }
                });
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    var request = _this.getSaveEntity();
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/ChangePassword'),
                        request: request,
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.ChangePassword.Success'), function () {
                                window.location.href = Q.resolveUrl('~/');
                            });
                        }
                    });
                });
                return _this;
            }
            ChangePasswordPanel.prototype.getFormKey = function () { return Membership.ChangePasswordForm.formKey; };
            ChangePasswordPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], ChangePasswordPanel);
            return ChangePasswordPanel;
        }(Serenity.PropertyPanel));
        Membership.ChangePasswordPanel = ChangePasswordPanel;
    })(Membership = CntAppSeguridad.Membership || (CntAppSeguridad.Membership = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Membership;
    (function (Membership) {
        var ForgotPasswordPanel = /** @class */ (function (_super) {
            __extends(ForgotPasswordPanel, _super);
            function ForgotPasswordPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.ForgotPasswordForm(_this.idPrefix);
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    var request = _this.getSaveEntity();
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/ForgotPassword'),
                        request: request,
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.ForgotPassword.Success'), function () {
                                window.location.href = Q.resolveUrl('~/');
                            });
                        }
                    });
                });
                return _this;
            }
            ForgotPasswordPanel.prototype.getFormKey = function () { return Membership.ForgotPasswordForm.formKey; };
            ForgotPasswordPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], ForgotPasswordPanel);
            return ForgotPasswordPanel;
        }(Serenity.PropertyPanel));
        Membership.ForgotPasswordPanel = ForgotPasswordPanel;
    })(Membership = CntAppSeguridad.Membership || (CntAppSeguridad.Membership = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Membership;
    (function (Membership) {
        var ResetPasswordPanel = /** @class */ (function (_super) {
            __extends(ResetPasswordPanel, _super);
            function ResetPasswordPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.ResetPasswordForm(_this.idPrefix);
                _this.form.NewPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value.length < 7) {
                        return Q.format(Q.text('Validation.MinRequiredPasswordLength'), 7);
                    }
                });
                _this.form.ConfirmPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value !== _this.form.NewPassword.value) {
                        return Q.text('Validation.PasswordConfirm');
                    }
                });
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    var request = _this.getSaveEntity();
                    request.Token = _this.byId('Token').val();
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/ResetPassword'),
                        request: request,
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.ResetPassword.Success'), function () {
                                window.location.href = Q.resolveUrl('~/Account/Login');
                            });
                        }
                    });
                });
                return _this;
            }
            ResetPasswordPanel.prototype.getFormKey = function () { return Membership.ResetPasswordForm.formKey; };
            ResetPasswordPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], ResetPasswordPanel);
            return ResetPasswordPanel;
        }(Serenity.PropertyPanel));
        Membership.ResetPasswordPanel = ResetPasswordPanel;
    })(Membership = CntAppSeguridad.Membership || (CntAppSeguridad.Membership = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
var CntAppSeguridad;
(function (CntAppSeguridad) {
    var Membership;
    (function (Membership) {
        var SignUpPanel = /** @class */ (function (_super) {
            __extends(SignUpPanel, _super);
            function SignUpPanel(container) {
                var _this = _super.call(this, container) || this;
                _this.form = new Membership.SignUpForm(_this.idPrefix);
                _this.form.ConfirmEmail.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmEmail.value !== _this.form.Email.value) {
                        return Q.text('Validation.EmailConfirm');
                    }
                });
                _this.form.ConfirmPassword.addValidationRule(_this.uniqueName, function (e) {
                    if (_this.form.ConfirmPassword.value !== _this.form.Password.value) {
                        return Q.text('Validation.PasswordConfirm');
                    }
                });
                _this.byId('SubmitButton').click(function (e) {
                    e.preventDefault();
                    if (!_this.validateForm()) {
                        return;
                    }
                    Q.serviceCall({
                        url: Q.resolveUrl('~/Account/SignUp'),
                        request: {
                            DisplayName: _this.form.DisplayName.value,
                            Email: _this.form.Email.value,
                            Password: _this.form.Password.value
                        },
                        onSuccess: function (response) {
                            Q.information(Q.text('Forms.Membership.SignUp.Success'), function () {
                                window.location.href = Q.resolveUrl('~/');
                            });
                        }
                    });
                });
                return _this;
            }
            SignUpPanel.prototype.getFormKey = function () { return Membership.SignUpForm.formKey; };
            SignUpPanel = __decorate([
                Serenity.Decorators.registerClass()
            ], SignUpPanel);
            return SignUpPanel;
        }(Serenity.PropertyPanel));
        Membership.SignUpPanel = SignUpPanel;
    })(Membership = CntAppSeguridad.Membership || (CntAppSeguridad.Membership = {}));
})(CntAppSeguridad || (CntAppSeguridad = {}));
//# sourceMappingURL=CntAppSeguridad.Web.js.map